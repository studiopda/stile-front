FROM node:11

# Install Python3

RUN apt-get update && apt-get install -y python3

# Install JQ

RUN wget "http://stedolan.github.io/jq/download/linux64/jq" && chmod 755 jq

# Install AWS CLI

RUN curl -sO https://bootstrap.pypa.io/get-pip.py
RUN python3 get-pip.py

RUN pip install awscli

CMD ["node"]
