import React from 'react'
import { required, password, date, email, requiredBool, phone } from '../../src/validators'
import { Checkbox } from '../../src/components'
import Flatpickr from 'react-flatpickr'

export const trialFormSchema = {
    fields: [
        {
            type: 'text',
            name: 'firstName',
            label: 'First Name',
            placeholder: 'Fay',
            validator: required,
            errorMessage: 'This field is required',
        },
        {
            type: 'text',
            name: 'lastName',
            label: 'Last Name',
            placeholder: 'James',
            validator: required,
            errorMessage: 'This field is required',
        },
        {
            type: 'text',
            name: 'company',
            label: 'School',
            placeholder: 'Port Niranda State School',
            validator: required,
            errorMessage: 'This field is required',
        },
        {
            type: 'email',
            name: 'email',
            label: 'Email',
            placeholder: 'fay.james@portnirandastateschool.edu',
            validator: email,
            errorMessage: 'This field is required',
        },
        {
            type: 'text',
            name: 'phone',
            label: 'Phone',
            placeholder: '03 9000 0000',
            validator: phone,
            errorMessage: 'This field is required',
        },
        {
            name: 'moreInfo',
            defaultValue: false,
            component: ({value, onChangeValue, validator}) => {
                return (
                    <Checkbox
                        value={value}
                        label={'Email me some more information'}
                        onChange={(val) => onChangeValue(val, validator)}
                    />
                )
            }
        },
        {
            name: 'bookChat',
            defaultValue: false,
            component: ({value, onChangeValue, validator, fieldState}) => {
                return (
                    <Checkbox
                        value={value}
                        label={'Book in a time to chat'}
                        onChange={(val) => onChangeValue(val, validator)}
                    />
                )
            }
        },
        {
            name: 'chatTime',
            defaultValue: false,
            hide: (fieldState) => {
                return fieldState['bookChat'].value ? false : true
            },
            component: ({value, onChangeValue, validator}) => {
                return (
                    <Flatpickr
                        data-enable-time
                        value={date}
                        onChange={(val) => onChangeValue(val, validator)}
                        {...flatpickrProps}
                    />
                )
            }
        },
        {
            name: 'bookSchoolEvaluation',
            defaultValue: false,
            component: ({value, onChangeValue, validator}) => {
                return (
                    <Checkbox
                        value={value}
                        label={'Book in a school evaluation'}
                        onChange={(val) => onChangeValue(val, validator)}
                    />
                )
            }
        },
        {
            name: 'schoolEvaluationTime',
            defaultValue: false,
            hide: (fieldState) => {
                return fieldState['bookSchoolEvaluation'].value ? false : true
            },
            component: ({value, onChangeValue, validator}) => {
                return (
                    <Flatpickr
                        data-enable-time
                        value={date}
                        onChange={(val) => onChangeValue(val, validator)}
                        {...flatpickrProps}
                    />
                )
            }
        },
    ]
}

const flatpickrProps = {
    placeholder: 'Select a date / time',
    options: {
        dateFormat: 'J F Y  —  h:iK'
    }
}