const defaultBlocks = [

    // Paragraph

    {
        type: 'text',
        text: `
            Think of Stile Classroom as the <a href="#">combination of your favourite science</a> textbook, workbook, assessments, videos, and simulations all interwoven and presented as a seamless online teaching and learning experience.
        `
    },

    // Heading

    {
        type: 'heading',
        textAlignment: 'left', // left, center, right
        text: 'We’ve covered every topic for Years 7-10'
    },

  

    // Quote Block

    {
        type: 'quote',
        heading: 'What teachers are saying',
        slides: [
            {
                text: `Stile creates authentic scientific investigations that allows students to take control of their learning.`,
                credit: `
                    Fiona Boneham<br>
                    Aurora College, Ryde
                `,
            },
            {
                text: `Stile gives me a quick and easy way to monitor student understanding that is dynamic and fun for the students too.`,
                credit: `
                    Kylie Wynne<br>
                    Caringbah High School, Caringbah NSW 
                `,
            },
        ]
    },

    // Media Blocks

    {
        type: 'media',
        mediaType: 'image',
        alt: 'Some alt text',
        image: 'https://stileeducation.com/b4f9d4bea0726cb010ad88f91d7b1430.jpg',
        caption: 'text' 
    },    

    {
        type: 'media',
        mediaType: 'image',
        imageScaling: true,
        containImage: false,
        image: require('../../src/assets/images/diagram.svg'),
    },
    
    {
        type: 'media',
        mediaType: 'video',
        alt: null,
        video: 'https://www.youtube.com/watch?v=Ga1MyKTds9g',
        caption: 'text',    

        // Modal

        coverImage: 'https://stileeducation.com/b4f9d4bea0726cb010ad88f91d7b1430.jpg',
        buttonText: 'Watch Stile in action (2 mins)',

        // Behaviour 

        modal: true,
        autoplay: true, // initial behaviour
        loop: true,
    },
    

    // Media Comps

    {
        type: 'heading',
        textAlignment: 'left',
        text: 'A: MONTAGE'
    },

    {
        type: 'media-comp',
        layoutKey: 'a',
        media: [
            {
                type: 'media',
                mediaType: 'video',
                video: 'https://stileeducation.com/f1559a0337d4050ddc0400189688e23f.mp4',
                caption: 'text',    
                inline: true,
                autoplay: true,
            },
            {
                type: 'media',
                mediaType: 'image',
                alt: 'Some alt text',
                image: 'https://stileeducation.com/b4f9d4bea0726cb010ad88f91d7b1430.jpg',
            },
            {
                type: 'media',
                mediaType: 'image',
                alt: 'Some alt text',
                image: 'https://stileeducation.com/b4f9d4bea0726cb010ad88f91d7b1430.jpg',
            },
            {
                type: 'media',
                mediaType: 'image',
                alt: 'Some alt text',
                image: 'https://stileeducation.com/b4f9d4bea0726cb010ad88f91d7b1430.jpg',
            },
      
            {
                type: 'media',
                mediaType: 'image',
                alt: 'Some alt text',
                image: 'https://stileeducation.com/b4f9d4bea0726cb010ad88f91d7b1430.jpg',
            },
        ]
    },


    {
        type: 'heading',
        textAlignment: 'left',
        text: 'B: STEP & REPEAT FOUR LEFT'
    },

    {
        type: 'media-comp',
        layoutKey: 'b',
        media: [
            {
                type: 'media',
                mediaType: 'image',
                alt: 'Some alt text',
                image: 'https://stileeducation.com/b4f9d4bea0726cb010ad88f91d7b1430.jpg'
            },
            {
                type: 'media',
                mediaType: 'image',
                alt: 'Some alt text',
                image: 'https://stileeducation.com/b4f9d4bea0726cb010ad88f91d7b1430.jpg'
            },
            {
                type: 'media',
                mediaType: 'image',
                alt: 'Some alt text',
                image: 'https://stileeducation.com/b4f9d4bea0726cb010ad88f91d7b1430.jpg'
            },
            {
                type: 'media',
                mediaType: 'image',
                alt: 'Some alt text',
                image: 'https://stileeducation.com/b4f9d4bea0726cb010ad88f91d7b1430.jpg'
            }
        ]
    },

    {
        type: 'heading',
        textAlignment: 'left',
        text: 'C: DIAGONAL ASYMMETRICAL FIVE RIGHT'
    },

    {
        type: 'media-comp',
        layoutKey: 'c',
        media: [
            {
                type: 'media',
                mediaType: 'image',
                alt: 'Some alt text',
                image: 'https://stileeducation.com/b4f9d4bea0726cb010ad88f91d7b1430.jpg'
            },
            {
                type: 'media',
                mediaType: 'image',
                alt: 'Some alt text',
                image: 'https://stileeducation.com/b4f9d4bea0726cb010ad88f91d7b1430.jpg'
            },
            {
                type: 'media',
                mediaType: 'image',
                alt: 'Some alt text',
                image: 'https://stileeducation.com/b4f9d4bea0726cb010ad88f91d7b1430.jpg'
            },
            {
                type: 'media',
                mediaType: 'image',
                alt: 'Some alt text',
                image: 'https://stileeducation.com/b4f9d4bea0726cb010ad88f91d7b1430.jpg'
            },
            {
                type: 'media',
                mediaType: 'image',
                alt: 'Some alt text',
                image: 'https://stileeducation.com/b4f9d4bea0726cb010ad88f91d7b1430.jpg'
            }
        ]
    },

    {
        type: 'heading',
        textAlignment: 'left',
        text: 'D: DIAGONAL ASYMMETRICAL FIVE LEFT'
    },
    {
        type: 'media-comp',
        layoutKey: 'd',
        media: [
            {
                type: 'media',
                mediaType: 'image',
                alt: 'Some alt text',
                image: 'https://stileeducation.com/b4f9d4bea0726cb010ad88f91d7b1430.jpg'
            },
            {
                type: 'media',
                mediaType: 'image',
                alt: 'Some alt text',
                image: 'https://stileeducation.com/b4f9d4bea0726cb010ad88f91d7b1430.jpg'
            },
            {
                type: 'media',
                mediaType: 'image',
                alt: 'Some alt text',
                image: 'https://stileeducation.com/b4f9d4bea0726cb010ad88f91d7b1430.jpg'
            },
            {
                type: 'media',
                mediaType: 'image',
                alt: 'Some alt text',
                image: 'https://stileeducation.com/b4f9d4bea0726cb010ad88f91d7b1430.jpg'
            },
            {
                type: 'media',
                mediaType: 'image',
                alt: 'Some alt text',
                image: 'https://stileeducation.com/b4f9d4bea0726cb010ad88f91d7b1430.jpg'
            }
        ]
    },

    {
        type: 'heading',
        textAlignment: 'left',
        text: 'E: ASYMMETRICAL FIVE RIGHT'
    },

    {
        type: 'media-comp',
        layoutKey: 'e',
        media: [
            {
                type: 'media',
                mediaType: 'image',
                alt: 'Some alt text',
                image: 'https://stileeducation.com/b4f9d4bea0726cb010ad88f91d7b1430.jpg',
            },
            {
                type: 'media',
                mediaType: 'image',
                alt: 'Some alt text',
                image: 'https://stileeducation.com/b4f9d4bea0726cb010ad88f91d7b1430.jpg',
            },
            {
                type: 'media',
                mediaType: 'image',
                alt: 'Some alt text',
                image: 'https://stileeducation.com/b4f9d4bea0726cb010ad88f91d7b1430.jpg',
            },
            {
                type: 'media',
                mediaType: 'image',
                alt: 'Some alt text',
                image: 'https://stileeducation.com/b4f9d4bea0726cb010ad88f91d7b1430.jpg',
            },
            {
                type: 'media',
                mediaType: 'image',
                alt: 'Some alt text',
                image: 'https://stileeducation.com/b4f9d4bea0726cb010ad88f91d7b1430.jpg',
            }
        ]
    },

    {
        type: 'heading',
        textAlignment: 'left',
        text: 'F: ASYMMETRICAL FIVE LEFT '
    },

    {
        type: 'media-comp',
        layoutKey: 'f',
        media: [
            {
                type: 'media',
                mediaType: 'image',
                alt: 'Some alt text',
                image: 'https://stileeducation.com/b4f9d4bea0726cb010ad88f91d7b1430.jpg'
            },
            {
                type: 'media',
                mediaType: 'image',
                alt: 'Some alt text',
                image: 'https://stileeducation.com/b4f9d4bea0726cb010ad88f91d7b1430.jpg'
            },
            {
                type: 'media',
                mediaType: 'image',
                alt: 'Some alt text',
                image: 'https://stileeducation.com/b4f9d4bea0726cb010ad88f91d7b1430.jpg'
            },
            {
                type: 'media',
                mediaType: 'image',
                alt: 'Some alt text',
                image: 'https://stileeducation.com/b4f9d4bea0726cb010ad88f91d7b1430.jpg'
            },
            {
                type: 'media',
                mediaType: 'image',
                alt: 'Some alt text',
                image: 'https://stileeducation.com/b4f9d4bea0726cb010ad88f91d7b1430.jpg'
            }
        ]
    },

    {
        type: 'heading',
        textAlignment: 'left',
        text: 'G: STEP & REPEAT FIVE LEFT'
    },
    {
        type: 'media-comp',
        layoutKey: 'g',
        media: [
            {
                type: 'media',
                mediaType: 'image',
                alt: 'Some alt text',
                image: 'https://stileeducation.com/b4f9d4bea0726cb010ad88f91d7b1430.jpg'
            },
            {
                type: 'media',
                mediaType: 'image',
                alt: 'Some alt text',
                image: 'https://stileeducation.com/b4f9d4bea0726cb010ad88f91d7b1430.jpg'
            },
            {
                type: 'media',
                mediaType: 'image',
                alt: 'Some alt text',
                image: 'https://stileeducation.com/b4f9d4bea0726cb010ad88f91d7b1430.jpg'
            },
            {
                type: 'media',
                mediaType: 'image',
                alt: 'Some alt text',
                image: 'https://stileeducation.com/b4f9d4bea0726cb010ad88f91d7b1430.jpg'
            },
            {
                type: 'media',
                mediaType: 'image',
                alt: 'Some alt text',
                image: 'https://stileeducation.com/b4f9d4bea0726cb010ad88f91d7b1430.jpg'
            }
        ]
    },

    {
        type: 'heading',
        textAlignment: 'left',
        text: 'H: STEP & REPEAT FIVE RIGHT'
    },
    {
        type: 'media-comp',
        layoutKey: 'h',
        media: [
            {
                type: 'media',
                mediaType: 'image',
                alt: 'Some alt text',
                image: 'https://stileeducation.com/b4f9d4bea0726cb010ad88f91d7b1430.jpg'
            },
            {
                type: 'media',
                mediaType: 'image',
                alt: 'Some alt text',
                image: 'https://stileeducation.com/b4f9d4bea0726cb010ad88f91d7b1430.jpg'
            },
            {
                type: 'media',
                mediaType: 'image',
                alt: 'Some alt text',
                image: 'https://stileeducation.com/b4f9d4bea0726cb010ad88f91d7b1430.jpg'
            },
            {
                type: 'media',
                mediaType: 'image',
                alt: 'Some alt text',
                image: 'https://stileeducation.com/b4f9d4bea0726cb010ad88f91d7b1430.jpg'
            },
            {
                type: 'media',
                mediaType: 'image',
                alt: 'Some alt text',
                image: 'https://stileeducation.com/b4f9d4bea0726cb010ad88f91d7b1430.jpg'
            }
        ]
    },

    {
        type: 'heading',
        textAlignment: 'left',
        text: 'I: MONTAGE THREE'
    },
    {
        type: 'media-comp',
        layoutKey: 'i',
        media: [
            {
                type: 'media',
                mediaType: 'image',
                alt: 'Some alt text',
                image: 'https://stileeducation.com/b4f9d4bea0726cb010ad88f91d7b1430.jpg'
            },
            {
                type: 'media',
                mediaType: 'image',
                alt: 'Some alt text',
                image: 'https://stileeducation.com/b4f9d4bea0726cb010ad88f91d7b1430.jpg'
            },
            {
                type: 'media',
                mediaType: 'image',
                alt: 'Some alt text',
                image: 'https://stileeducation.com/b4f9d4bea0726cb010ad88f91d7b1430.jpg'
            }
        ]
    },

    {
        type: 'heading',
        textAlignment: 'left',
        text: 'J: CENTRE ALIGN THREE'
    },
    {
        type: 'media-comp',
        layoutKey: 'j',
        media: [
            {
                type: 'media',
                mediaType: 'image',
                alt: 'Some alt text',
                image: 'https://stileeducation.com/b4f9d4bea0726cb010ad88f91d7b1430.jpg'
            },
            {
                type: 'media',
                mediaType: 'image',
                alt: 'Some alt text',
                image: 'https://stileeducation.com/b4f9d4bea0726cb010ad88f91d7b1430.jpg'
            },
            {
                type: 'media',
                mediaType: 'image',
                alt: 'Some alt text',
                image: 'https://stileeducation.com/b4f9d4bea0726cb010ad88f91d7b1430.jpg'
            }
        ]
    },

    // CTA (Uses global data unless overrided)

    {
        type: 'cta',
    },

    // Page Links

    {
        type: 'page-links',
        subHeading: 'What is Stile?',
        heading: 'Stile classroom',
        text: 'Cutting edge resources for direct instruction and self-paced learning',
        button: { // Internal Link
            url: '/what-is-stile/stile-classroom',
            external: false,
            text: 'Learn more →'
        }
    },

    // List Columns

    {
        type: 'list-columns',
        items: [
            {
                heading: 'Biology',
                text: `
                    Classification<br>
                    Kingdoms<br>
                    Food Chains<br>
                    Biodiversity<br>
                    Invasive Species<br>
                    Body Systems<br>
                    Cells<br>
                    Reproduction<br>
                    The Nervous System<br>
                    The Endocrine System<br>
                    The Immune System<br>
                    Vaccination<br>
                    Microbiomes<br>
                `,
            },
            {
                heading: 'Physics',
                text: `
                    Forces<br>
                    Gravity<br>
                    Levers and Gears<br>
                    Inclined Planes<br>
                    Energy Transformation<br>
                    Light and Colour<br>
                    Lenses<br>
                    Sound<br>
                    Radiation<br>
                    Electrical Circuits<br>
                    Energy Conservation<br>
                    Kinematics<br>
                    Newton's Laws of Motion<br>
                `,
            },
            {
                heading: 'Chemistry',
                text: `
                    Classification<br>
                    Kingdoms<br>
                    Food Chains<br>
                    Biodiversity<br>
                    Invasive Species<br>
                    Body Systems<br>
                    Cells<br>
                    Reproduction<br>
                    The Nervous System<br>
                    The Endocrine System<br>
                    The Immune System<br>
                    Vaccination<br>
                    Microbiomes<br>
                `,
            },
            {
                heading: 'Earth & Space',
                text: `
                    Forces<br>
                    Gravity<br>
                    Levers and Gears<br>
                    Inclined Planes<br>
                    Energy Transformation<br>
                    Light and Colour<br>
                    Lenses<br>
                    Sound<br>
                    Radiation<br>
                    Electrical Circuits<br>
                    Energy Conservation<br>
                    Kinematics<br>
                    Newton's Laws of Motion<br>
                `,
            },
        ]
    },

    // Feature List

    {
        type: 'feature-list',
        heading: 'Enable modern pedagogy',
        useTicks: true, // Use Tick for items
        features: [
            {
                icon: false, // override tick icon 
                heading: 'Differentiated instruction',
                text: `
                    Tailor lesson content on the fly to cater to different learning needs.
                `,
            },
            {
                icon: require('../../src/assets/images/feature-tick-override.svg'), // override tick icon 
                heading: 'Drag and drop multime',
                text: `
                    Easily add as much of your own media into your lessons as you like, and in any format.
                `,
            }
        ]
    },

    // Text Grid

    {
        type: 'text-grid',
        alignment: 'left', // left or center
        itemAlignment: 'left', // left or center
        items: [
            {
                icon: require('../../src/assets/icons/text-grid/classroom.svg'), 
                heading: 'More classroom discussions around real-world science',
                text: `Every unit of work in Stile Classroom is set in the context of real-world science discoveries and events, helping spark conversation and debate in the classroom.`,
            },
            {
                icon: require('../../src/assets/icons/text-grid/instructions.svg'), 
                heading: 'Teachers differentiating their instruction more frequently',
                text: `Every lesson can be customised, allowing teachers to quickly tailor content to the needs of individual students.`,
            },
        ]
    },

    // Team Block

    {
        type: 'team',
        heading: 'Teaching & Learning experts',
        text: 'Want to book in some PD at your school? Or simply find out more? Get in touch with your local expert',
        showContactInfo: false,
        showAll: true,
        // category: 'teaching-learning', // will only display members from this category
    },
    
    // Tour Block

    {
        type: 'tour',
        heading: 'Take a tour of Stile',
        sliders: [
            {
                heading: 'Engaging for all students',
                slides: [
                    {
                        image: require('../../src/assets/images/slide-placeholder.svg'),
                        text: 'Topics are taught in the context of recent science news, bringing real-world relevance to every lesson',
                    },
                    {
                        image: require('../../src/assets/images/slide-placeholder.svg'),
                        text: 'Every unit is guided by a relevant and relatable expert, showcasing a wide range of careers',
                    },
                    {
                        image: require('../../src/assets/images/slide-placeholder.svg'),
                        text: 'Engaging science news stories provide context and opportunities for collaboration and scientific discussion',
                    },
                ]
            },
            {
                heading: 'Engaging for all students',
                slides: [
                    {
                        image: require('../../src/assets/images/slide-placeholder.svg'),
                        text: 'Topics are taught in the context of recent science news, bringing real-world relevance to every lesson',
                    },
                    {
                        image: require('../../src/assets/images/slide-placeholder.svg'),
                        text: 'Every unit is guided by a relevant and relatable expert, showcasing a wide range of careers',
                    },
                    {
                        image: require('../../src/assets/images/slide-placeholder.svg'),
                        text: 'Engaging science news stories provide context and opportunities for collaboration and scientific discussion',
                    },
                ]
            },
            {
                heading: 'Engaging for all students',
                slides: [
                    {
                        image: require('../../src/assets/images/slide-placeholder.svg'),
                        text: 'Topics are taught in the context of recent science news, bringing real-world relevance to every lesson',
                    },
                    {
                        image: require('../../src/assets/images/slide-placeholder.svg'),
                        text: 'Every unit is guided by a relevant and relatable expert, showcasing a wide range of careers',
                    },
                    {
                        image: require('../../src/assets/images/slide-placeholder.svg'),
                        text: 'Engaging science news stories provide context and opportunities for collaboration and scientific discussion',
                    },
                ]
            }
        ]
    },
    
    {
        type: 'map'
    },

    // Signature block

    {
        type: 'signature',
        image: require('../../src/assets/images/signature.svg')
    },

    // Eventbrite

    {
        type: 'eventbrite',
        embedCode: 'iframe code here' // could be hardcoded 
    },
]

export default defaultBlocks