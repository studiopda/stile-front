const data = {
    title: 'Page Name',
    menuOrder: 1,
    seo: {
        title: '',
        description: '', 
        opengraph_image: '',
        opengraph_title: '',
        opengraph_description: '',
        twitter_image: '',
        twitter_title: '',
        twitter_description: ''
    },
    pageHero: {
        image: require('../../../src/assets/images/'),
        title: '',
        description: ``,
    },
    blocks: [
        
    ]
}

export default data