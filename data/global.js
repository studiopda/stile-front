export const data = {
    footer: {
        contact: {
            phone: '1300 918 292',
            social: [
                {
                    name: 'Email',
                    link: 'mailto:community@stileeducation.com'
                },
                {
                    name: 'Facebook',
                    link: 'https://www.facebook.com/StileEducation/'
                },
                {
                    name: 'Instagram',
                    link: 'https://www.instagram.com/stileeducation/?hl=en'
                },
                {
                    name: 'Blog',
                    link: 'https://medium.com/stileeducation'
                },
                {
                    name: 'Stile Shop',
                    link: 'https://shop.stileeducation.com/'
                }
            ]
        },
        statement: `Stile is proudly Australian owned by its founders. Stile HQ is located at 128 Exhibition Street, Melbourne, but you’ll find local Stile team members wherever we operate.
                    <br><br>
                    Our office is located on the traditional lands of the Boon Wurrung and Woiwurrung (Wurundjeri) peoples of the Kulin Nation. We acknowledge that sovereignty was never ceded and pay our respects to elders past, present and future`,
        credits: {
            terms:  '#',
            privacy: '#',
        }
    },
    ctaBlock: {
        heading: 'Get Stile at your school',
        text: 'Join a community of schools passionate about great science education.',
        button: {
            url: '/set-up-trial',
            external: false,
            text: 'Set up a trial'
        }
    }
}

export const getGlobalData = (key) => {
    return data[key];
}


export default data