const data = {
    title: 'UX Designer',
    location: ['melbourne'],
    workType: ['full-time'],
    categories: ['product'],
    menuOrder: 1,
    seo: {
        title: '',
        description: '', 
        opengraph_image: '',
        opengraph_title: '',
        opengraph_description: '',
        twitter_image: '',
        twitter_title: '',
        twitter_description: ''
    },
}

export default data