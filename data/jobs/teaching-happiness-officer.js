const data = {
    title: 'Teaching Happiness Officer',
    location: ['melbourne'],
    workType: ['full-time'],
    categories: ['community'],
    menuOrder: 1,
    seo: {
        title: '',
        description: '', 
        opengraph_image: '',
        opengraph_title: '',
        opengraph_description: '',
        twitter_image: '',
        twitter_title: '',
        twitter_description: ''
    },
    blocks: [
        {
            type: 'markdown',
            markdown: `
                Join our team to share your passion for science and technology! 

                **Meaningful:** Shaping the future of science education in Australia

                **High-impact:** Directly support thousands of front-line superheroes - teachers!

                **Flexible:** Work remotely or in the office. Full time with negotiable hours.

                We're living through what will be the most historic global event since the second world war. It's scary, it's confusing - especially for our youngest citizens - and misinformation is rife. It is more important than ever that our schools are armed with the tools, resources and training they need to guide our students - guidance not just to understand the facts of this pandemic (though that is of course important), but how to stop and critically evaluate those facts for themselves.

                We create world-class resources for science teachers (we call them superheroes, because… they are). Stile's resources are used by 1 in 3 Australian secondary schools. All our resources are delivered through our web-based teaching and learning platform, and it's all been built right here in Australia. Every single day, over 80,000 students and 6,000 teachers around the country (and increasingly around the world) use our resources.

                To get a bit of a feel for what we do, check out our latest lesson on the COVID-19 Pandemic. Being a digital resource, Stile just so happens to be perfect for remote teaching - something that is going to be pretty commonplace for the next few months. As you can imagine, we're busier than ever.

                That's why we need your help! We need you on the front lines of our team, assisting teachers when they write or call in needing help. We need you to do whatever it takes to make their day just that little bit easier.

                Let's be clear, this is a customer support role… but a support role like none other. We're obsessive about making every interaction delightful. We pride ourselves on getting back to teachers within minutes, not days. We love the idea that we're making the lives of busy, overworked teachers just that little bit easier. They notice the extra effort we make, and are always hugely appreciative. We also believe in giving our team the flexibility to own and solve problems - there are no scripts here!

                We're looking for someone to join our team who is as obsessed as we are with delighting others. This is a full-time role with an immediate start. This is not a temporary position - we're looking for someone who wants to join the team and be part of something pretty special for years to come. You'll be only our second dedicated teacher happiness team member, but supported by a wider customer team that can jump in and help when it gets busy.

                Does this sound like you? If so, the first thing is to read 'the details' below. Then, send us your CV and a cover letter explaining why you're going to smash this role out of the park. Address your letter to Katrina Don Paul and send both documents to [**jointheteam@stileeducation.com**](mailto:jointheteam@stileeducation.com).

                Can't wait to hear from you!

                # The Details

                #### Hours and location

                This is a full-time, permanent role, but the exact hours worked will be based on negotiation with your team. We need some flexibility on when you work, but we're committed to being flexible too. This role is about helping teachers solve their problems and the best time to do that may turn out to be a few hours on Sunday afternoon, rather than Friday afternoon. However, the hours we want to cover might be different in school holidays, where you could take days off, or public holidays, where we might need you to be on call. Whatever the case, we're big believers in helping our team find the right balance between work and play, knowing this looks pretty different from person to person.

                As part of the social distancing measures being put in place to curb the spread of COVID-19, we're currently working fully remote, so you'll need to be able to work from home. In normal times, our (awesome) office is located on Exhibition Street in the Melbourne CBD, and while we have a flexible remote work policy, we like people to be in the office more than not.

                #### What's in it for you

                You'll join an amazing 40-strong team of teachers, scientists, artists and engineers. Imagine a workplace where everyone genuinely cares about, and is working towards, improving scientific literacy amongst our young citizens. We hold ourselves, and each other, to a high standard, and love what we do

                You'll be working on something that really matters. In partnership with teachers, we're coaching students to become problem solvers and critical thinkers. The internet democratised information. We believe that with a strong command of science, tomorrow's citizens and leaders have a fighting chance of sorting the misinformation from the facts.

                We (at least in normal times) run events (often at the pub!) for our teachers all around the country. From time to time, you'll get the opportunity to actually meet people you're helping online or over the phone. This is pretty rare in this sort of role, and oddly awesome. Real connection.

                Your own professional development - we're lifelong learners here at Stile. Alongside teaching you everything you need to know to succeed in this role, you'll have the opportunity to attend internal and external PD.

                The much-loved annual company snow trip (though 2020 is looking doubtful with COVID-19).

                A Mac laptop, and whatever other bits and pieces you need to be productive remotely.

                #### How you’ll spend your time

                **Answering inbound enquiries**  
                Teachers contact us every day in a multitude of ways: our in-platform chat, by phone, email and carrier pigeon (though the last is rare). Sometimes they've got a simple question about how to achieve something with our online tools. Other times they want to know if we have a lesson on a specific topic available. Sometimes they're just plain stuck. It's your job to get to the bottom of the issue and solve it.

                Sometimes you'll know the answer. Sometimes you'll jump on a quick video or screen sharing session to really see what's going on. Expect to be liaising with our engineering department to identify and report software bugs (though of course, we don't have bugs).

                If we've made a mistake, we give you the flexibility to make it right. Will you send them a hand-written card? A hamper? Up to you, but whatever it takes.

                **Making happiness even better**  
                You'll be on the active lookout for ways we can become even better at serving teachers. One way is to continuously update and add to our self-service help centre, writing new articles that empower teachers to find answers to questions themselves.

                **Special Projects**  
                Variety is the spice of life. Once you're thoroughly settled into your role, you'll get the opportunity to participate in projects in other parts of the business. What this looks like will be guided by your interests.

                #### A bit more about you

                There are no specific experience requirements for this role. We'll teach you everything you need to know to be successful. Here is a bit about you:

                - You delight in helping others, and you often go out of your way to do so. You love to see others succeed.
                - You have a natural affinity for technology and computers - you're completely comfortable teaching yourself how to use new software and you generally pick it up far quicker than most. You're likely the one always getting asked to fix someone's computer. You know more hotkeys than you'd ever actually admit.
                - You generally like to leave things better than you found them (ever rearranged the spices cupboard?). You love making things better. You pick up litter and straighten picture frames as you walk past. Most people think these are small things, but you know how important those small things really are.
                - You're empathetic. You have the ability to put yourself in the shoes of a teacher who is struggling to solve a problem and may be stressed as a result. You know how to communicate effectively to help.
                - You're happy chatting to someone online, and can write a good, well-structured email. You're probably a proficient touch typer (yes kids, that means typing without looking at the keyboard). But equally, you don't hesitate to pick up the phone when that'll be easier.
                - You're eager to learn. Super eager. You soak up feedback, incorporate it, and are constantly driven to be better. You can't help it.
                - You're smart, and you love to get shit done.

                # Working at Stile

                We invest in our people because we believe that great people in a great environment can achive amazing things. People don’t join Stile to have a cushy job; they join to do fantastic things with incredible people. We set high expectations and then help you to exceed them. That said, there are plenty of “the usual” perks:

                - A friendly, fun and informal team environment.
                - A modern office in the heart of the CBD.
                - Fully stocked kitchen, chillout areas, a kickass sound system for after-work events and unlimited coffee.
                - Regular social and team-building events for staff throughout the year.
                - A commitment to the career development & training of all staff members.

                # How to apply

                Send us an email including:

                - 1-page cover letter addressed to Katrina Don Paul telling us why you’re the best candidate for the job.
                - Your resume

                Email applications to [<ins>jointheteam@stileeducation.com</ins>](mailto:jointheteam@stileeducation.com)
            `
        },
        {
            type: 'cta'
        }
    ]
}

export default data