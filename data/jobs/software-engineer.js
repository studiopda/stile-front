const data = {
    title: 'Software Engineer – All levels',
    location: ['melbourne'],
    workType: ['full-time'],
    categories: ['engineering'],
    menuOrder: 1,
    seo: {
        title: '',
        description: '', 
        opengraph_image: '',
        opengraph_title: '',
        opengraph_description: '',
        twitter_image: '',
        twitter_title: '',
        twitter_description: ''
    },
}

export default data