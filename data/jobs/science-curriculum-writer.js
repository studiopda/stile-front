const data = {
    title: 'Science Curriculum Writer',
    location: ['melbourne'],
    workType: ['full-time'],
    categories: ['content'],
    menuOrder: 1,
    seo: {
        title: '',
        description: '', 
        opengraph_image: '',
        opengraph_title: '',
        opengraph_description: '',
        twitter_image: '',
        twitter_title: '',
        twitter_description: ''
    },
}

export default data