const data = {
    title: 'Video Content Producer',
    location: ['sydney'],
    workType: ['full-time'],
    categories: ['content'],
    menuOrder: 2,
    seo: {
        title: '',
        description: '', 
        opengraph_image: '',
        opengraph_title: '',
        opengraph_description: '',
        twitter_image: '',
        twitter_title: '',
        twitter_description: ''
    },
}

export default data