export const data = {

    // Map categories & location

    categories: [
        {
            name: 'All categories',
            slug: 'all',
        },
        {
            name: 'Community',
            slug: 'community',
        },
        {
            name: 'Content',
            slug: 'content',
        },
        {
            name: 'Engineering',
            slug: 'engineering',
        },
        {
            name: 'Operations',
            slug: 'operations',
        },
        {
            name: 'Product',
            slug: 'product',
        },
    ],

    locations: [
        {
            name: 'All locations',
            slug: 'all',
        },
        {
            name: 'Melbourne',
            slug: 'melbourne',
        },
        {
            name: 'Sydney',
            slug: 'sydney',
        },
    ],

    workTypes: [
        {
            name: 'Full-Time',
            slug: 'full-time'
        },
        {
            name: 'Part-Time',
            slug: 'part-time'
        },
    ]
}

export default data