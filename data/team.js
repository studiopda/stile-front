export const data = {

    // Map categories & name

    categories: [
        {
            name: 'Everyone',
            slug: 'all',
        },
        {
            name: 'Community',
            slug: 'community',
        }, 
        {
            name: 'Content',
            slug: 'content',
        },
        {
            name: 'Engineering',
            slug: 'engineering',
        },
        {
            name: 'Leadership',
            slug: 'leadership',
            showInAll: false
        },
        {
            name: 'Operations',
            slug: 'operations',
        },
        {
            name: 'Product',
            slug: 'product',
        },
        {
            name: 'Teaching and Learning Experts',
            slug: 'teaching-learning',
            showInAll: false
        },
    ],


    // Team Members
    
    members: [

        // Content

        {
            image: require('../src/assets/images/team/staffportrait_marta_ivkov.png'),
            name: 'Marta Ivkov', 
            role: 'Head of Content',
            background: 'Science teacher working on her pun game, periodically.',
            countryCode: 'NZ',
            location: 'NZ',
            categories: ['content']
        },              
        {
            image: require('../src/assets/images/team/staffportrait_campbell.png'),
            name: 'Campbell Edgar', 
            role: 'Science Curriculum Editor',
            background: 'Concise',
            countryCode: 'NZ',
            location: 'NZ',
            categories: ['content']
        },
        {
            image: require('../src/assets/images/team/staffportrait_alison_tandy.png'),
            name: 'Alison Tandy', 
            role: 'Science Curriculum Writer',
            background: 'Can use her sternest teacher voice in downward facing dog.',
            countryCode: 'NZ',
            location: 'NZ',
            categories: ['content']
        },
        {
            image: require('../src/assets/images/team/staffportrait_gus_morainslie.png'),
            name: 'Gustavo Morales Ainslie', 
            role: 'Art Director',
            background: 'Background',
            countryCode: 'NZ',
            location: 'NZ',
            categories: ['content']
        },
        
        // Community

        {
            image: require('../src/assets/images/team/staffportrait_alex_russell.png'),
            name: 'Alexandra Russell', 
            role: 'Teaching and Learning Expert, NZ',
            background: 'Middle school teacher and fur baby mama',
            countryCode: 'NZ',
            location: 'NZ',
            contact: {
                text: 'Email Alex',
                link: 'mailto:alexandra.russell@stileeducation.com'
            },
            categories: ['community', 'teaching-learning']
        },
        {
            image: require('../src/assets/images/team/staffportrait_meadhbh_obrien.png'),
            name: 'Meadhbh O’Brien', 
            role: 'Teaching & Learning Expert',
            background: 'Chemistry teacher who likes long walks on the beach',
            countryCode: 'AUS',
            location: 'QLD / NT',
            contact: {
                text: 'Email Meadhbh',
                link: 'mailto:meadhbh.obrien@stileeducation.com'
            },
            categories: ['community', 'teaching-learning']
        },
        {
            image: require('../src/assets/images/team/staffportrait_charlie_kidd.png'),
            name: 'Charlotte Kidd', 
            role: 'Teaching & Learning Expert',
            background: 'Science teacher fuelled by 90% caffeine and 10% water',
            countryCode: 'AUS',
            location: 'VIC / WA / TAS',
            contact: {
                text: 'Email Charlotte',
                link: 'mailto:charlotte.kidd@stileeducation.com'
            },
            categories: ['community', 'teaching-learning']
        },
        {
            image: require('../src/assets/images/team/staffportrait_josh_ravek.png'),
            name: 'Joshua Ravek', 
            role: 'Head of Schools, NSW',
            background: 'Striving for perfectoin',
            countryCode: 'AUS',
            location: 'NSW',
            contact: {
                text: 'Email Josh',
                link: 'mailto:joshua.ravek@stileeducation.com'
            },
            categories: ['community', 'teaching-learning']
        },
        {
            image: require('../src/assets/images/team/staffportrait_meghan.png'),
            name: 'Meghan Fennessy', 
            role: 'Teaching & Learning Expert',
            background: 'I must confess, I am actually an English teacher',
            countryCode: 'AUS',
            location: 'VIC',
            contact: {
                text: 'Email Meghan',
                link: 'mailto:meghan.fennessy@stileeducation.com'
            },
            categories: ['community', 'teaching-learning']
        },
        {
            image: require('../src/assets/images/team/staffportrait_rebecca_wilde.png'),
            name: 'Rebecca Wilde', 
            role: 'Teaching & Learning Expert',
            background: 'Wise (I actually got my wisdom teeth fully through aged 13)',
            countryCode: 'AUS',
            location: 'NSW / SA / ACT',
            contact: {
                text: 'Email Rebecca',
                link: 'mailto:rebecca.wilde@stileeducation.com'
            },
            categories: ['community', 'teaching-learning']
        },
        {
            image: require('../src/assets/images/team/staffportrait_andrew_nicholls.png'),
            name: 'Andrew Nicholls', 
            role: 'Head of Schools, Vic',
            background: 'Cricketer/teacher/dad passionate about science education',
            countryCode: 'AUS',
            location: 'VIC',
            contact: {
                text: 'Email Andrew',
                link: 'mailto:andrew.nicholls@stileeducation.com'
            },
            categories: ['community', 'teaching-learning']
        },
        {
            image: require('../src/assets/images/team/staffportrait_dave.png'),
            name: 'Dave Canavan', 
            role: 'Head of Schools, QLD',
            background: 'Snake catching, science teaching inventor',
            countryCode: 'AUS', 
            location: 'QLD',
            contact: {
                text: 'Email Dave',
                link: 'mailto:dave.canavan@stileeducation.com'
            },
            categories: ['community', 'teaching-learning']
        },
        {
            image: require('../src/assets/images/team/staffportrait_jaclyn_rooney.png'),
            name: 'Jaclyn Rooney', 
            role: 'Head of Teaching & Learning',
            background: 'Not taking heed of the old adage ‘never work with children or animals’ – Jacky worked at Melbourne Zoo and as a teacher before moving across to Stile in 2016. Jacky has worked in multiple roles across Stile; including Head of Content, Head of Professional Development and Head of Community.',
            countryCode: 'AUS',
            location: 'VIC',
            contact: {
                text: 'Email Jacky',
                link: 'mailto:jaclyn.rooney@stileeducation.com'
            },
            categories: ['community', 'leadership', 'teaching-learning']
        },
        {
            image: require('../src/assets/images/team/staffportrait_isobel_telfer.png'),
            name: 'Izzie Telfer', 
            role: 'Head of Schools, SA/ACT',
            background: 'Verbose',
            countryCode: 'AUS',
            location: 'SA',
            contact: {
                text: 'Email Isabel',
                link: 'mailto:isabel.telfer@stileeducation.com'
            },
            categories: ['community', 'teaching-learning']
        },
        {
            image: require('../src/assets/images/team/staffportrait_tony_osullivan.png'),
            name: 'Tony O’Sullivan', 
            role: 'Head of School, NZ',
            background: 'Dad, Kiwi and a bit cuddly.',
            countryCode: 'NZ',
            location: 'NZ',
            contact: {
                text: 'Email Tony',
                link: 'mailto:tony.osullivan@stileeducation.com'
            },
            categories: ['community', 'teaching-learning']
        },
        {
            image: require('../src/assets/images/team/staffportrait_natasha_smith.png'),
            name: 'Natasha Smith', 
            role: 'Teaching and Learning Expert',
            background: 'Student dietitian and dark chocolate afficionado',
            countryCode: 'NZ',
            location: 'NZ',
            categories: ['community', 'teaching-learning']
        },
        {
            image: require('../src/assets/images/team/staffportrait_guido_gautsch.png'),
            name: 'Guido Gautsch', 
            role: 'Happiness & Video',
            background: `'I make videos when I'm not helping people'`,
            countryCode: 'NZ',
            location: 'NZ',
            categories: ['community']
        },
        {
            image: require('../src/assets/images/team/staffportrait_jules_ius.png'),
            name: 'Jules Ius', 
            role: 'Customer Happines Officer',
            background: 'I dish out happiness',
            countryCode: 'NZ',
            location: 'NZ',
            categories: ['community']
        },
        {
            image: require('../src/assets/images/team/staffportrait_katrina.png'),
            name: 'Katrina Don Paul', 
            role: 'Head of Teacher Happiness',
            background: 'Teacher, Scientist, Mum, Netflix (not necessarily in that order...)',
            countryCode: 'NZ',
            location: 'NZ',
            contact: {
                text: 'Email Katrina',
                link: 'mailto:katrina.donpaul@stileeducation.com'
            },
            categories: ['community', 'teaching-learning'],
        },
        {
            image: require('../src/assets/images/team/staffportrait_kirsten_hood.png'),
            name: 'Kirsten Hood', 
            role: 'Communications Coordinator',
            background: 'Science communicator with too many indoor plants',
            countryCode: 'NZ',
            location: 'NZ',
            categories: ['community']
        },
        {
            image: require('../src/assets/images/team/staffportrait_sarah_lindop.png'),
            name: 'Sarah Lindop', 
            role: 'Head of Schools',
            background: 'After working as an eLearning developer, predominantly in the health industry, Sarah made the move to a sector she was more passionate about; education.',
            countryCode: 'NZ',
            location: 'NZ',
            contact: {
                text: 'Email Sarah',
                link: 'mailto:sarah.lindop@stileeducation.com'
            },
            categories: ['community', 'teaching-learning', 'leadership']
        },

        // Engineering

        {
            image: require('../src/assets/images/team/staffportrait_rabia.png'),
            name: 'Rabia Arbar', 
            role: 'Software Architect',
            background: `'I'm actually a poet. Ruby TypeScript, React gulp, brew install yarn'`,
            countryCode: 'NZ',
            location: 'NZ',
            categories: ['engineering']
        },
        {
            image:require('../src/assets/images/team/staffportrai_matthew_borden.png'),
            name: 'Matthew Borden', 
            role: 'Senior Site Reliability Engineer',
            background: 'How does this component handle ẓ̴̜͚̺̭̈̒a̴̞͐͗l̴̬̖̦̣̱̯̋̈́̉͒͋̏g̵̛̞̰̟͐̑̆̓͛o̷̻̪͓͒̃͂̽ ̵̛̬̖̎t̴̢̪̭͙̏̌̔ͅe̷̼͕͓͍̦͐̈̎͘͜x̴̲̲͋̂́̅́t̷̩̲̳͐͌ ?',
            countryCode: 'NZ',
            location: 'NZ',
            categories: ['engineering']
        },
        {
            image:require('../src/assets/images/team/staffportrait_nepa.png'),
            name: 'Narthana Epa', 
            role: 'Software Engineer',
            background: 'Resident burrito programmer',
            countryCode: 'NZ',
            location: 'NZ',
            categories: ['engineering']
        },
        {
            image: require('../src/assets/images/team/staffportrait_alex_finkel.png'),
            name: 'Alexander Finkel', 
            role: 'Head of Product Engineering',
            background: 'Fantasy/sci-fi nerd, ballroom/latin dancer, sometimes abuses apostrophes.',
            countryCode: 'NZ',
            location: 'NZ',
            categories: ['engineering']
        },
        {
            image: require('../src/assets/images/team/staffportrait_daniel_flynn.png'),
            name: 'Daniel Flynn', 
            role: 'Software Engineer',
            background: 'I once stole a cheese plate from a physicist, then blamed their rival',
            countryCode: 'NZ',
            location: 'NZ',
            categories: ['engineering']
        },
        {
            image: require('../src/assets/images/team/staffportrait_peter_gates.png'),
            name: 'Peter Gates', 
            role: 'Software Engineer',
            background: 'Part-time photographer and cat-herder, full-time storyteller', // no relation to Bill
            countryCode: 'NZ',
            location: 'NZ',
            categories: ['engineering']
        },
        {
            image: require('../src/assets/images/team/staffportrait_huw_llewellyn.png'),
            name: 'Huw Llewellyn', 
            role: 'Software Engineer',
            background: 'How does this component handle ẓ̴̜͚̺̭̈̒a̴̞͐͗l̴̬̖̦̣̱̯̋̈́̉͒͋̏g̵̛̞̰̟͐̑̆̓͛o̷̻̪͓͒̃͂̽ ̵̛̬̖̎t̴̢̪̭͙̏̌̔ͅe̷̼͕͓͍̦͐̈̎͘͜x̴̲̲͋̂́̅́t̷̩̲̳͐͌ ?',
            countryCode: 'NZ',
            location: 'NZ',
            categories: ['engineering']
        },
        {
            image: require('../src/assets/images/team/staffportrait_matt_needham.png'),
            name: 'Matthew Needham', 
            role: 'Business Systems Engineer',
            background: `'When one Matt just isn't enough'`,
            countryCode: 'NZ',
            location: 'NZ',
            categories: ['engineering']
        },
        {
            image: require('../src/assets/images/team/staffportrait_quynh-chi_Nguyen.png'),
            name: 'Quynh-Chi Nguyen',
            role: 'Senior Software Engineer',
            background: 'Professional life guru, unless you needed life advice',
            countryCode: 'NZ',
            location: 'NZ',
            categories: ['engineering']
        },
        {
            image: require('../src/assets/images/team/staffportrait_shaun_okeefe.png'),
            name: `Shaun O’Keefe`,
            role: 'Head of Infrastructure and SRE',
            background: 'Breaking hearts and builds',
            countryCode: 'NZ',
            location: 'NZ',
            categories: ['engineering']
        },
        {
            image: require('../src/assets/images/team/staffportrait_jeff_parsons.png'),
            name: 'Jeffrey Parsons', 
            role: 'Head of Engineering Systems and Business Systems',
            background: 'Prefers his graphs directed and acyclic',
            countryCode: 'NZ',
            location: 'NZ',
            categories: ['engineering']
        },

        // Leadership

        {
            image: require('../src/assets/images/team/staffportrait_daniel_pikler.png'),
            name: 'Daniel Pikler', 
            role: 'Head of Education',
            background: 'After working as a musician, then geneticist at the CSIRO in Canberra, Danny studied secondary teaching in Melbourne and saw first hand the need for better science teaching resources. Danny joined Byron and Alan to found Stile in 2012.',
            countryCode: 'NZ',
            location: 'NZ',
            categories: ['leadership']
        },
        {
            image: require('../src/assets/images/team/staffportrait_daniel_rodgers-pryor.png'),
            name: 'Daniel Rodgers-Pryor', 
            role: 'CTO',
            background: 'Daniel’s academic background in Condensed Matter Physics and Computer Science gave him a passion for promoting scientific literacy, and the skills to manage complex computing systems. Daniel joined Stile in 2014, and stepped into the role of CTO in 2017.',
            countryCode: 'NZ',
            location: 'NZ',
            categories: ['leadership', 'engineering']
        },
        {
            image: require('../src/assets/images/team/staffportrait_byron_scaf.png'),
            name: 'Byron Scaf', 
            role: 'CEO',
            background: 'Byron got a few months into a PhD in Neuroscience at Melbourne University before building Australia’s first electric vehicle charging network with Better Place. Finally understanding what STEM meant in the real world, he co-founded Stile in 2012 and set about working with schools.',
            countryCode: 'NZ',
            location: 'NZ',
            categories: ['leadership', 'product']
        },

        // Operations

        {
            image: require('../src/assets/images/team/staffportrait_aleesha_vandenheuvel.png'),
            name: 'Aleesha Van Den Heuvel', 
            role: 'Reception & Office Assistant',
            background: `'I'm a pisces, so that's about all you need to know'`,
            countryCode: 'NZ',
            location: 'NZ',
            categories: ['operations']
        },
        {
            image: require('../src/assets/images/team/staffportrait_emily.png'),
            name: 'Emily Cheng', 
            role: 'Finance Manager',
            background: 'My happiness is debit = credit.',
            countryCode: 'NZ',
            location: 'NZ',
            categories: ['operations']
        },
        {
            image: require('../src/assets/images/team/staffportrait_makeila_reyes.png'),
            name: 'Makeila Reyes', 
            role: 'People & Culture Manager',
            background: 'Puts completed things on her to-do list just to check them off',
            countryCode: 'NZ',
            location: 'NZ',
            categories: ['operations']
        },
        
        // Product

        {
            image: require('../src/assets/images/team/staffportrait_clare.png'),
            name: 'Clare Feeney', 
            role: 'Head of Product',
            background: 'After working as a medical scientist in the biochemistry laboratory at the Royal Brisbane Hospital, Clare became a science teacher and worked for a few years as a teacher in Brisbane. She has worked in the Ed-Tech industry building science education products since 2015. Clare joined the Stile team in 2019.',
            countryCode: 'NZ',
            location: 'NZ',
            categories: ['product', 'leadership']
        },
        {
            image: require('../src/assets/images/team/staffportrait_sean_hurley.png'),
            name: 'Sean Hurley', 
            role: 'Head of Design',
            background: 'Can cook eggs at least 9 different ways',
            countryCode: 'NZ',
            location: 'NZ',
            categories: ['product']
        },
    ]
}

export default data