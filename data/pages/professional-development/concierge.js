const data = {
    title: 'Concierge service',
    menuOrder: 2,
    seo: {
        title: 'Concierge service',
        description: 'We are here, at your service', 
        opengraph_image: require('../../../src/assets/images/coverimage_concierge_service.jpg'),
    },
    pageHero: {
        image: require('../../../src/assets/images/coverimage_concierge_service.jpg'),
        title: 'Concierge service',
        description: `We are here, at your service`,
    },
    blocks: [
        {
            type: 'text',
            text: `
                Stile's concierge service provides you with a dedicated team of teachers to assist with your planning and administrative tasks. Our Teaching and Learning experts are always on hand to help your teachers do brilliant work: from curriculum planning, right through to helping set up engaging lessons in Stile.
            `
        },
        {
            type: 'text-grid',
            alignment: 'center', 
            items: [
                {
                    icon: require('../../../src/assets/icons/text-grid/planning.svg'), 
                    heading: 'Planning support',
                    text: 
                        `Send us your scope and sequence or unit plans. We will analyse them, identify Stile lessons and activities that match your learning goals and curriculum, and return your plan to you with suggestions for where you can best incorporate Stile lessons to enhance student learning.
                    `,
                },
                {
                    icon: require('../../../src/assets/icons/text-grid/converting.svg'), 
                    heading: 'Converting materials into Stile',
                    text: 
                        `Want to convert existing worksheets, tests or pracs into a Stile lesson? Send your existing documents to us and we'll convert them into Stile lessons, allowing you to take advantage of all the interactive Stile tools, auto-marking features and ability to track student progress.
                    `,
                },
                {
                    icon: require('../../../src/assets/icons/text-grid/coaching.svg'), 
                    heading: 'One-on-one coaching',
                    text:
                         `Want a hand with something and want it now? No worries, give us a call and we'll coach you through anything you need; from the technical to the pedagogical.
                    `,
                },
            ]
        },
        {
            type: 'page-links',
            subHeading: 'Professional Development',
            heading: 'Public events and webinars',
            text: 'Connect and collaborate with other passionate science teachers from your area',
            button: { 
                url: '/professional-development/public-events-webinars',
                external: false,
                text: 'Learn more →'
            }
        },
        {
            type: 'cta',
        },
    ]
}

export default data