const data = {
    title: 'Public events and webinars',
    menuOrder: 3,
    seo: {
        title: 'Public events and webinars',
        description: 'Connect and collaborate with other passionate science teachers from your area', 
        opengraph_image: require('../../../src/assets/images/coverimage_public_events.jpg'),
    },
    pageHero: {
        image: require('../../../src/assets/images/coverimage_public_events.jpg'),
        title: 'Public events and webinars',
        description: `Connect and collaborate with other passionate science teachers from your area`,
    },
    blocks: [
        {
            type: 'text',
            text: `
                Stile hosts a range of public professional development events and webinars throughout Australia and New Zealand. These are a fantastic opportunity to connect and share ideas with other teachers from your area, as well as learn some new and interesting ways to use Stile in your classroom.
            `
        },
        {
            type: 'heading',
            textAlignment: 'left', 
            text: 'Upcoming events'
        },
        {
            type: 'page-links',
            subHeading: 'Who we are',
            heading: 'The Stile team',
            text: 'We share a passion for great science education',
            button: { 
                url: '/who-we-are/team',
                external: false,
                text: 'Learn more →'
            }
        },
        {
            type: 'cta',
        },
    ]
}

export default data