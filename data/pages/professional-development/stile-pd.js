const data = {
    title: 'Stile PD',
    menuOrder: 1,
    seo: {
        title: 'Stile PD',
        description: 'Stile can customise professional development workshops for your school', 
        opengraph_image: require('../../../src/assets/images/coverimage_professional_developement.png'),
    },
    pageHero: {
        image: require('../../../src/assets/images/coverimage_professional_developement.png'),
        title: 'Stile PD',
        description: `Stile can customise professional development workshops for your school`,
    },
    blocks: [
        {
            type: 'text',
            text: `
                Developed in conjunction with leading STEM educators and institutions, Stile’s professional development workshops set the standard for STEM PD in Australia and New Zealand. Stile PD is designed to help teachers extend their skills and enhance their practice, focusing on research-based pedagogies and high-impact teaching strategies.
            `
        },
    {
        type: 'media-comp',
        layoutKey: 'b',
        media: [
            {
                type: 'media',
                mediaType: 'image',
                alt: 'Some alt text',
                image: require('../../../src/assets/images/professional_development_01.png'),
            },
            {
                type: 'media',
                mediaType: 'image',
                alt: 'Some alt text',
                image: require('../../../src/assets/images/professional_development_02.png'),
            },
        ]
    },
    {
        type: 'heading',
        text: 'We cover a wide range of topics'
    },
    {
        type: 'text',
        text: `
            <h3>Demonstration lessons</h3>
            Explore the best features of Stile first-hand, as one of our educators models best-practice use of Stile for your teaching faculty, or with a class of students.
            <br><br>

            <h3>Getting started with Stile</h3>
            Learn how to quickly set up subjects in Stile and start using it effectively in your classroom. Explore the range of features and tools in Stile that will help you engage students and enhance their learning.
            <br><br>

            <h3>Blended learning</h3>
            Stile is designed to be used as a blended learning tool. Explore the instructional practices that ensure success integrating Stile into your classroom, and that have been shown to enhance learning outcomes.
            <br><br>

            <h3>Collaborative learning</h3>
            Explore the range of features in Stile that have been designed to enhance collaboration in your classroom.
            <br><br>

            <h3>Differentiation</h3>
            Explore different methods for differentiating in Stile to take into account the significant differences among students in terms of their ability, rate of learning, language proficiency, and literacy skills.
            <br><br>

            <h3>Providing feedback</h3>
            Used well, teacher feedback is one of the most effective ways that we can help improve learning outcomes. Explore the features and tools in Stile that will help you to track student progress and provide regular, timely and effective feedback to your students.
            <br><br>

            <h3>Using augmented reality and virtual reality</h3>
            Using augmented and virtual reality in the classroom allows you to provide immersive learning experiences for your students that would otherwise be impossible. Discover the wealth of resources available in Stile to transform learning in your classroom. Be prepared to erupt volcanoes, explore the inner-workings of a plant cell and use your superpowers to view the particle structure of everyday items.
            <br><br>

            <h3>Using Stile to teach remotely</h3>
            Stile has been designed for use as a blended learning tool within the classroom, but is also perfectly designed to facilitate and support remote learning. It has been used successfully by a number of distance-education schools across Australia and New Zealand for many years. Learn the top tricks and tips to support your students learning, foster collaboration and provide timely and effective feedback in a remote setting.
        `
    },
    {
        type: 'team',
        heading: 'Teaching & Learning experts',
        showContactInfo: true,
        text: 'Want to book in some PD at your school? Or simply find out more? Get in touch with your local expert.',
        showAll: false,
        category: 'teaching-learning',
    },
    {
        type: 'page-links',
        subHeading: 'Professional Development',
        heading: 'Concierge service',
        text: 'We are here, at your service, every day of the week',
        button: { 
            url: '/professional-development/concierge',
            external: false,
            text: 'Learn more →'
        }
    },
    {
        type: 'cta',
    },
    ]
}

export default data