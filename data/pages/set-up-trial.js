const data = {
    title: 'Set up a trial',
    seo: {
        title: '',
        description: '', 
        opengraph_image: '',
        opengraph_title: '',
        opengraph_description: '',
        twitter_image: '',
        twitter_title: '',
        twitter_description: ''
    },

    salesforceEndpoint: 'https://webto.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8',
    heading: `Set up a trial`,
    text: `<p>We really want to make it as easy as possible for science departments to get started with Stile. 
            You’ll get enormous value out of Stile almost immediately, but as your team becomes more familiar, 
            we’re there to help demonstrate how to use Stile to really augment their existing teaching practice.</p>

            <p>Our team is there every step of the way - our teaching and learning team have years of experience 
            in helping science teachers build on their existing practices and refine their craft.</p>
    `,
    formSuccessMessage: `Thanks! One of our team members will be in touch shortly.`
}

export default data