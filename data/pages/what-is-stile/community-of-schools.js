const data = {
    title: 'Our community of schools',
    menuOrder: 5,
    seo: {
        title: 'Our community of schools',
        description: 'Join a community of schools passionate about great science education', 
        opengraph_image: '',
    },
    pageHero: {
        image: require('../../../src/assets/images/coverimage_our_community_of_schools.png'),
        title: 'Our community of schools',
        description: `Join a community of schools passionate about great science education`,
    },
    blocks: [
        {
            type: 'text',
            text: `
            Stile is used by over 1 in 3 Australian secondary schools, making it the most widely used science resource in Australia. More science departments are joining us every year, largely from word of mouth.
            <br><br>
            Before a school adopts Stile, we ask that they first have a chat with a nearby school that has already been using it for some time. We’re always happy to provide introductions.
            `
        },
        {
            type: 'map'
        },
        {
            type: 'quote',
            heading: 'What teachers are saying',
            slides: [
                {
                    text: `Stile creates authentic scientific investigations that allows students to take control of their learning.`,
                    credit: `
                        Fiona Boneham<br>
                        Aurora College, Ryde
                    `,
                },
                {
                    text: `Stile gives me a quick and easy way to monitor student understanding that is dynamic and fun for the students too.`,
                    credit: `
                        Kylie Wynne<br>
                        Caringbah High School, Caringbah NSW 
                    `,
                },
            ]
        },
        {
            type: 'page-links',
            subHeading: 'Professional Development',
            heading: 'Stile PD',
            text: 'Stile can customise professional development workshops for your school',
            button: { 
                url: '/professional-development/stile-pd',
                external: false,
                text: 'Learn more →'
            }
        },
        {
            type: 'cta',
        },
    ]
}

export default data