const data = {
    title: 'Our lessons and pedagogy',
    menuOrder: 3,
    seo: {
        title: 'Our lessons and pedagogy',
        description: 'Stile lessons are engaging and use thoroughly tested, evidence-based pedagogies', 
        opengraph_image: require('../../../src/assets/images/coverimage_our_lessons_and_pedagogy.png'),
    },
    pageHero: {
        image: require('../../../src/assets/images/coverimage_our_lessons_and_pedagogy.png'),
        title: 'Our lessons and pedagogy',
        description: `Stile lessons are engaging and use thoroughly tested, evidence-based pedagogies`,
    },
    blocks: [
        {
            type: 'text',
            text: `
                Stile incorporates a range of pedagogical approaches in the creation of its resources, drawing on the research of John Hattie, Robert Marzano, Dylan Wiliam and Jean Piaget, to name a few. 
            `
        },
        {
            type: 'feature-list',
            heading: 'Pedagogies found in Stile lessons',
            useTicks: true, 
            features: [
                {
                    heading: 'Teaching through real-world connections',
                },
                {
                    heading: 'Explicit teaching',
                },
                {
                    heading: 'Lower-to-higher order thinking',
                },
                {
                    heading: 'Collaborative learning',
                },
                {
                    heading: 'Learning goals and success criteria',
                },
                {
                    heading: 'Phenomena-based learning',
                },
                {
                    heading: 'Visible thinking routines',
                },
                {
                    heading: 'Self-paced learning',
                },
                {
                    heading:'Timely and specific feedback for students',
                },
                {
                    heading: 'Spaced repetition',
                },
                {
                    heading: 'Multi-modal learning',
                },
                {
                    heading: 'Inquiry-based learning',
                },
                {
                    heading: 'PISA scientific literacy framework',
                },
                {
                    heading: 'High impact teaching strategies',
                },
                {
                    heading: 'The five Es',
                },
            ]
        },
        {
            type: 'heading',
            textAlignment: 'left', 
            text: 'How we make a Stile lesson'
        },
        {
            type: 'text',
            text: `
                We are a group of passionate science teachers, artists, designers and engineers. We care deeply about science education.<br><br>
                At the beginning of a unit we sit down and think about the curriculum, what we want students to know, the skills we think are important for the future and how we can create engaging, interesting and relevant science lessons.
            `
        },
        {
            type: 'media',
            mediaType: 'image',
            imageScaling: false,
            image: require('../../../src/assets/images/diagram_pedagogy.svg'),
        },
        {
            type: 'media-comp',
            layoutKey: 'b',
            media: [
                {
                    type: 'media',
                    mediaType: 'image',
                    alt: 'Some alt text',
                    image: require('../../../src/assets/images/lessons_01.png'),
                },
                {
                    type: 'media',
                    mediaType: 'image',
                    alt: 'Some alt text',
                    image: require('../../../src/assets/images/lessons_02.png'),
                },
            ]
        },
        {
            type: 'heading',
            textAlignment: 'left', 
            text: 'Blended learning approach'
        },
        {
            type: 'text',
            text: `
                During our creation process we constantly consider how teachers will use our lessons in their classrooms to create meaningful learning experiences for all of their students. We believe that the teacher–student relationship has one of the highest impacts on student learning and we aim to create a collaborative learning environment. In science a key aspect of teaching is unveiling misconceptions. Our content is designed to include collaborative features such as polls and live brainstorms to encourage class discussions.
            `
        },
        {
            type: 'media-comp',
            layoutKey: 'b',
            media: [
                {
                    type: 'media',
                    mediaType: 'image',
                    alt: 'Some alt text',
                    image: require('../../../src/assets/images/lessons_03.png'),
                },
                {
                    type: 'media',
                    mediaType: 'image',
                    alt: 'Some alt text',
                    image: require('../../../src/assets/images/lessons_04.png'),
                },
            ]
        },
        {
            type: 'heading',
            textAlignment: 'left', 
            text: 'Test with real students'
        },
        {
            type: 'text',
            text: `
                We regularly visit schools to observe Stile being used in real classrooms and often teach lessons (as we are trained science teachers) with the classroom teacher. This provides us with really valuable insights.
                <br><br>
                Toward the end of creating our units we invite teachers and students to test our lessons at Stile HQ. Doing this gives us feedback from both perspectives and allows us to then validate or improve upon what we have built. 
            `
        },
        {
            type: 'media-comp',
            layoutKey: 'i',
            media: [
                {
                    type: 'media',
                    mediaType: 'image',
                    alt: 'Some alt text',
                    image: require('../../../src/assets/images/lessons_05.png'),
                },
                {
                    type: 'media',
                    mediaType: 'image',
                    alt: 'Some alt text',
                    image: require('../../../src/assets/images/lessons_06.png'),
                },
                {
                    type: 'media',
                    mediaType: 'image',
                    alt: 'Some alt text',
                    image: require('../../../src/assets/images/lessons_07.png'),
                    caption: 'text'
                }
            ]
        },
        {
            type: 'team',
            heading: 'Meet the content team',
            showContactInfo: false,
            showAll: false,
            category: 'content',
        },
        {
            type: 'page-links',
            subHeading: 'What is Stile?',
            heading: 'Stile at home',
            text: 'Your students’ companion to learn, revise and master science concepts',
            button: { 
                url: '/what-is-stile/at-home',
                external: false,
                text: 'Learn more →'
            }
        },
        {
            type: 'cta',
        },
    ]
}

export default data