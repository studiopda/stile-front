import defaultBlocks from '../../templates/default-blocks.js'

const data = {
    title: 'Tour of Stile',
    menuOrder: 2,
    seo: {
        title: "Tour of Stile",
        description: "Topics are taught in the context of recent science news, bringing real-world relevance to every lesson",
        opengraph_image: '',
    },
    blocks: [
        {
            type: 'tour',
            heading: 'Take a tour of Stile',
            sliders: [
                {
                    heading: 'Relevant and contextualised',
                    slides: [
                        {
                            image: require('../../../src/assets/images/tour_01.png'),
                            text: 'Topics are taught in the context of recent science news, bringing real-world relevance to every lesson',
                        },
                        {
                            image: require('../../../src/assets/images/tour_02.png'),
                            text: 'Every unit is guided by a relevant and relatable expert, showcasing a wide range of careers',
                        },
                        {
                            image: require('../../../src/assets/images/tour_03.png'),
                            text: 'Engaging science news stories provide context and opportunities for collaboration and scientific discussion',
                        },
                    ]
                },
                {
                    heading: 'Engaging for all students',
                    slides: [
                        {
                            image: require('../../../src/assets/images/tour_04.png'),
                            text: 'Each lesson starts with learning goals, helping students take responsibility for their learning',
                        },
                        {
                            image: require('../../../src/assets/images/tour_05.png'),
                            text: 'Lessons are scaffolded from lower- to higher-order thinking, ensuring all students are challenged',
                        },
                        {
                            image: require('../../../src/assets/images/tour_06.png'),
                            text: 'Interactive simulations allow students to model and run experiments',
                        },
                    ]
                },
                {
                    heading: 'Key insights into learning',
                    slides: [
                        {
                            image: require('../../../src/assets/images/tour_07.png'),
                            text: 'Provide real-time feedback to your students as you teach in Stile',
                        },
                        {
                            image: require('../../../src/assets/images/tour_08.png'),
                            text: 'As they work, students’ responses are automatically saved in Stile and instantly visible to the teacher',
                        },
                        {
                            image: require('../../../src/assets/images/tour_09.png'),
                            text: 'Students receive more feedback, more often',
                        },
                    ]
                }
            ]
        },
        {
            type: 'heading',
            textAlignment: 'center',
            text: 'Features built for the classroom'
        },
        {
            type: 'feature-list',
            heading: 'Enable modern pedagogy',
            useTicks: true, 
            features: [
                {
                    heading: 'Differentiated instruction',
                    text: `
                        Tailor lesson content on the fly to cater to different learning needs.
                    `,
                },
                {
                    heading: 'Data informed teaching',
                    text: `
                        Enhance your teaching practice with the insights provided by data analytics.
                    `,
                },
                {
                    heading: 'Visible thinking',
                    text: `
                        Gain insight into your students’ thought patterns by incorporating visible thinking routines into your lessons.
                    `,
                },
                {
                    heading: 'Flipped learning',
                    text: `
                        Fast and easy video embedding designed with flipped learning in mind.
                    `,
                },
                {
                    heading: 'Blended learning',
                    text: `
                        Easily combine online activities with offline tasks within your existing classroom workflow.
                    `,
                },
            ]
        },   
        {
            type: 'feature-list',
            heading: 'Build interactive lessons',
            useTicks: true, 
            features: [
                {
                    heading: 'Incorporate existing materials',
                    text: `
                        Don’t reinvent the wheel; include your existing lesson materials in Stile.
                    `,
                },
                {
                    heading: 'Drag and drop multimedia',
                    text: `
                        Easily add as much of your own media into your lessons as you like, and in any format.
                    `,
                },
                {
                    heading: 'Live polls',
                    text: `
                        Gauge student understanding, survey opinions, or facilitate peer-to-peer learning.
                    `,
                },
                {
                    heading: 'Annotation, sequencing and drawing questions',
                    text: `
                        The flexible 'interactive canvas' question type enables a whole host of innovative visual questions.
                    `,
                },
                {
                    heading: 'Save all work in one place',
                    text: `
                        Any student work done offline or in other applications can be uploaded right in the lessons — presentations, videos, photos and more.
                    `,
                },
                {
                    heading: 'Paperless classroom',
                    text: `
                    Save time, money and the environment by reducing printing and photocopying.
                    `,
                },
                {
                    heading: 'Adapt and reuse',
                    text: `
                        Teachers can build a library of customised student resources that can be used year after year, and shared with colleagues.
                    `,
                },
                {
                    heading: 'Mind mapping',
                    text: `
                        Have students brainstorm what they already know about a new topic or reinforce the grouping of ideas in a mind map.
                    `,
                },
                {
                    heading: 'Real-time class discussion',
                    text: `
                        Students can collaborate, share ideas and discuss work in a safe, moderated environment.
                    `,
                },
                {
                    heading: 'Open-ended tasks',
                    text: `
                        Empower students to respond with a mixture of text, multimedia, graphs and sketches.
                    `,
                },
                {
                    heading: 'Live brainstorms',
                    text: `
                    Allow students to collaborate in real-time. The Live Brainstorm widget allows students to quickly contribute their ideas to a class discussion.
                    `,
                },
            ]
        },    
        {
            type: 'feature-list',
            heading: 'Classroom workflow',
            useTicks: true,
            features: [
                {
                    heading: 'Real-time feedback',
                    text: `
                        Keep students on track with real-time written or audio feedback.
                    `,
                },
                {
                    heading: 'Everything in one place',
                    text: `
                        Lesson content, student responses, class discussions and feedback, all together.
                    `,
                },
                {
                    heading: 'Due dates',
                    text: `
                        It’s simple to set, change and check upcoming due work.
                    `,
                },
                {
                    heading: 'Automated instant feedback',
                    text: `
                        Pre-set responses give students even faster feedback.
                    `,
                },
                {
                    heading: 'Real-time class insights',
                    text: `
                        Live analytics and performance insights help identify student misconceptions earlier than ever.
                    `,
                },
                {
                    heading: 'Fully-worked model answers',
                    text: `
                        Model answers are embedded into each open-ended question in Stile, and teachers can modify these or create their own.
                    `,
                },
            ]
        },  
        {
            type: 'feature-list',
            heading: 'Customised to your science classroom',
            useTicks: true, // Use Tick for items
            features: [
                {
                    heading: 'Flexible assessment tools',
                    text: `
                        Diagnostic, formative and summative assessment tasks are all included, or you can design your own.
                    `,
                },
                {
                    heading: 'Digital portfolio',
                    text: `
                        Each student’s work, results and progress are saved, and can be referred to year after year.
                    `,
                },
                {
                    heading: 'Digital workbook',
                    text: `
                        Worksheet creation, organisation and assessment is built into the lesson, and seamlessly incorporated into the experience.
                    `,
                },
                {
                    heading: 'Streamlined reporting',
                    text: `
                        Keep parents informed when it matters most, with easy to use reporting tools.
                    `,
                },
                {
                    heading: 'Quizzes',
                    text: `
                        Quickly test your students’ understanding of a topic with pre-made quizzes or make your own.
                    `,
                },
                {
                    heading: 'Learning diary',
                    text: `
                        Students can reflect on their learning process over the course of a unit.
                    `,
                },
                {
                    heading: 'Digital lab-book',
                    text: `
                        Students record experiment results straight into Stile. Placing their data alongside the lesson content makes it easier to revise, and easier for teachers to assess learning and progress.
                    `,
                },
            ]
        },
        {
            type: 'feature-list',
            heading: 'Interoperable',
            useTicks: true, 
            features: [
                {
                    heading: 'Any device, anywhere',
                    text: `
                        Seamless switching between a tablet at school and a laptop at home, for students and teachers.
                    `,
                },
                {
                    heading: 'iOS app',
                    text: `
                        Students can bring work from any iPad app into Stile for feedback and marking.
                    `,
                },
                {
                    heading: 'ntegration API',
                    text: `
                        Incorporates seamlessly into your school’s software ecosystem.
                    `,
                },
                {
                    heading: 'Any file format',
                    text: `
                        Just upload whatever you have and Stile takes care of the rest.
                    `,
                },
            ]
        },       
        {
            type: 'page-links',
            subHeading: 'What is Stile?',
            heading: 'Our lessons and pedagogy',
            text: 'Stile lessons are engaging and use thoroughly tested, evidence-based pedagogies',
            button: { // Internal Link
                url: '/what-is-stile/lessons-and-pedagogy',
                external: false,
                text: 'Learn more →'
            },
        },    
        {
            type: 'cta',
        },  
    ]
}

export default data