import defaultBlocks from '../../templates/default-blocks.js'

const data = {
    title: 'Benefits',
    menuOrder: 4,
    seo: {
        title: '',
        description: '', 
        opengraph_image: '',
    },
    pageHero: {
        image: require('../../../src/assets/images/coverimage_benefits.png'),
        title: 'Benefits',
        description: `With Stile, your school can enhance its science program`,
    },
    blocks: [
        {
            type: 'text',
            text: `Stile’s science resources aren’t like the others. Our relentless commitment to creating resources of the highest quality, our focus on using proven pedagogies, and our philosophy of getting all students interested in science, will elevate your school’s science program to new heights.`,
            localisation: {
                NZ: {
                    text: 'Hello from NZ! Stile’s science resources aren’t like the others. Our relentless commitment to creating resources of the highest quality, our focus on using proven pedagogies, and our philosophy of getting all students interested in science, will elevate your school’s science program to new heights.'
                }
            }
        },
        {
            type: 'text-grid',
            items: [
                {
                    icon: require('../../../src/assets/icons/text-grid/classroom.svg'), 
                    heading: 'More classroom discussions around real-world science',
                    text: `Every unit of work in Stile Classroom is set in the context of real-world science discoveries and events, helping spark conversation and debate in the classroom.`,
                },
                {
                    icon: require('../../../src/assets/icons/text-grid/instructions.svg'), 
                    heading: 'Teachers differentiating their instruction more frequently',
                    text: `Every lesson can be customised, allowing teachers to quickly tailor content to the needs of individual students.`,
                },
                {
                    icon: require('../../../src/assets/icons/text-grid/thinking.svg'), 
                    heading: 'Students pondering hard problems',
                    text: `Every lesson can be customised, allowing teachers to quickly tailor content to the needs of individual students.`,
                },
                {
                    icon: require('../../../src/assets/icons/text-grid/feedback.svg'), 
                    heading: 'Students receiving better feedback, more often',
                    text: `Stile provides teachers with the ability to see their students’ work at any time, allowing them to provide more feedback, and in a more timely manner. `,
                },
                {
                    icon: require('../../../src/assets/icons/text-grid/book.svg'), 
                    heading: 'Improve student literacy',
                    text: `Embedded within every Stile unit are science news stories from Cosmos Magazine. These stories are written with age- appropriate language and are accompanied with audio narration to help improve student reading and comprehension.`,
                },
                {
                    icon: require('../../../src/assets/icons/text-grid/voice.svg'), 
                    heading: 'Quieter students find their voice',
                    text: `Quieter students often “fly under the radar”, slowly slipping behind the rest of the class. Stile’s collaboration tools create a safe, comfortable environment for all students to have their voices heard in class conversations and debates.`,
                },
                {
                    icon: require('../../../src/assets/icons/text-grid/computer.svg'), 
                    heading: 'Classroom technology enhancing learning',
                    text: `Stile Classroom has been designed to make excellent use of smartboards and student devices. Rather than being sidelined, you’ll notice teachers at the heart of their classrooms, using technology peripherally in a blended learning environment.`,
                },
                {
                    icon: require('../../../src/assets/icons/text-grid/timer.svg'), 
                    heading: 'Improved uptake of STEM subjects in senior years',
                    text: `Schools have told us that Stile has been a key factor in boosting the uptake of STEM subjects in senior years, both for boys and girls.`,
                },
                {
                    icon: require('../../../src/assets/icons/text-grid/tree.svg'), 
                    heading: 'Less printing and photocopying',
                    text: `We’ve had schools measure how much money they spent on printing and photocopying before and after adopting Stile. Some reported savings of up to $33 per student.`,
                },
                {
                    icon: require('../../../src/assets/icons/text-grid/grade.svg'), 
                    heading: 'Students achieving higher marks in assessments',
                    text: `A number of schools have told us that Stile has boosted their results, both in internal summative assessments, and in external science examinations (such as VALID in New South Wales).`,
                },
            ]
        },
        {
            type: 'page-links',
            subHeading: 'What is Stile?',
            heading: 'Our community of schools',
            text: 'Join a community of schools passionate about great science education',
            button: {
                url: '/what-is-stile/community-of-schools',
                external: false,
                text: 'Learn more →'
            },
        },    
        {
            type: 'cta',
        },    
    ]
}

export default data