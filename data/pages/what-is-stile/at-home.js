const data = {
    title: 'Stile at home',
    menuOrder: 6,
    seo: {
        title: 'Stile at home',
        description: 'Your students’ companion to learn, revise and master science concepts', 
        opengraph_image: '',
        opengraph_title: '',
        opengraph_description: '',
        twitter_image: '',
        twitter_title: '',
        twitter_description: ''
    },
    pageHero: {
        image: require('../../../src/assets/images/coverimage_stile_homework.png'),
        title: 'Stile at home',
        description: `Your students’ companion to learn, revise and master science concepts`,
    },
    blocks: [
        {
            type: 'text',
            text: `
                Designed to complement Stile Classroom, Stile Homework is the perfect way for students to quickly learn, consolidate and master the fundamentals of science outside of the classroom. The mobile phone app can help students learn every concept and scientific term in the Years 7–10 curriculum. This allows teachers to optimise classroom time: applying their students’ knowledge through higher-order questions and hands-on investigations, rather than simply learning the facts.
            `
        },
        {
            type: 'media',
            mediaType: 'image',
            imageScaling: false,
            image: require('../../../src/assets/images/diagram.svg'),
        },
        {
            type: 'heading',
            textAlignment: 'left', 
            text: 'A perfect education companion'
        },
        {
            type: 'text',
            text: `
                By learning the basics outside of class, your school time can be dedicated to applying science to real-world problems.
            `
        },
        {
            type: 'media',
            mediaType: 'image',
            alt: 'Some alt text',
            containImage: true,
            image: require('../../../src/assets/images/homework_05.gif'),
        },   
        {
            type: 'heading',
            textAlignment: 'left', 
            text: 'Stile Homework learns with you'
        },
        {
            type: 'text',
            text: `
                The true magic of Stile Homework is that it personalises learning for every student. As students answer questions, Stile Homework learns what they do and don’t know, and teaches them what they don’t know until they’ve mastered a topic.
            `
        },
        {
            type: 'media-comp',
            layoutKey: 'b',
            media: [
                {
                    type: 'media',
                    mediaType: 'image',
                    alt: 'Some alt text',
                    image: require('../../../src/assets/images/homework_01.png'),
                },
                {
                    type: 'media',
                    mediaType: 'image',
                    alt: 'Some alt text',
                    image: require('../../../src/assets/images/homework_02.png'),
                },
                {
                    type: 'media',
                    mediaType: 'image',
                    alt: 'Some alt text',
                    image: require('../../../src/assets/images/homework_03.png'),
                },
                {
                    type: 'media',
                    mediaType: 'image',
                    alt: 'Some alt text',
                    image: require('../../../src/assets/images/homework_04.png'),
                }
            ]
        },
        {
            type: 'page-links',
            subHeading: 'What is Stile?',
            heading: 'Benefits',
            text: 'With Stile, your school can enhance its science program',
            button: { 
                url: '/what-is-stile/benefits',
                external: false,
                text: 'Learn more →'
            }
        },
        {
            type: 'cta',
        },
    ]
}

export default data 