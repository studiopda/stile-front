import defaultBlocks from '../../templates/default-blocks.js'

const data = {
    title: 'All Blocks',
    menuOrder: 10,
    seo: {
        title: '',
        description: '', 
        opengraph_image: '',
    },
    pageHero: {
        image: 'https://stileeducation.com/0937f88fcf3b3953d7ae15d168fe572f.jpg',
        title: 'All Blocks',
        description: `Displaying all content blocks`,
    },
    blocks: [
        ...defaultBlocks,
        
    ]
}

export default data