const data = {
    title: 'Stile in the classroom',
    menuOrder: 1,
    seo: {
        title: 'Stile in the classroom',
        description: 'Cutting-edge resources for direct instruction and self-paced learning', 
        opengraph_image: require('../../../src/assets/images/coverimage_stile_classroom.png'),
    },
    pageHero: {
        image: require('../../../src/assets/images/coverimage_stile_classroom.png'),
        title: 'Stile in the classroom',
        description: `Cutting-edge resources for direct instruction and self-paced learning`,
    },
    blocks: [
        {
            type: 'text',
            text: `
            Think of Stile as the combination of your favourite science textbook, workbook, assessments, videos, and simulations all interwoven and presented as a seamless online teaching and learning experience.
            <br><br>
            It’s everything you need to teach Years 7-10 science.
            `
        },
        {
            type: 'media',
            mediaType: 'video',
            alt: null,
            video: 'https://youtu.be/jcLFR8p8nDw',
            caption: 'text',    
    
            coverImage: require('../../../src/assets/images/stile-in-action.jpg'),
            buttonText: 'Watch Stile in action (2 mins)',
    
            modal: true,
            autoplay: true,
            loop: true,
        },
        {
            type: 'heading',
            textAlignment: 'left', 
            text: 'We’ve covered every topic for Years 7-10'
        },
        {
            type: 'list-columns',
            items: [
                {
                    heading: 'Biology',
                    text: `
                        Classification<br>
                        Kingdoms<br>
                        Food Chains<br>
                        Biodiversity<br>
                        Invasive Species<br>
                        Body Systems<br>
                        Cells<br>
                        Reproduction<br>
                        The Nervous System<br>
                        The Endocrine System<br>
                        The Immune System<br>
                        Vaccination<br>
                        Microbiomes<br>
                    `,
                },
                {
                    heading: 'Physics',
                    text: `
                        Forces<br>
                        Gravity<br>
                        Levers and Gears<br>
                        Inclined Planes<br>
                        Energy Transformation<br>
                        Light and Colour<br>
                        Lenses<br>
                        Sound<br>
                        Radiation<br>
                        Electrical Circuits<br>
                        Energy Conservation<br>
                        Kinematics<br>
                        Newton's Laws of Motion<br>
                    `,
                },
                {
                    heading: 'Chemistry',
                    text: `
                        Classification<br>
                        Kingdoms<br>
                        Food Chains<br>
                        Biodiversity<br>
                        Invasive Species<br>
                        Body Systems<br>
                        Cells<br>
                        Reproduction<br>
                        The Nervous System<br>
                        The Endocrine System<br>
                        The Immune System<br>
                        Vaccination<br>
                        Microbiomes<br>
                    `,
                },
                {
                    heading: 'Earth & Space',
                    text: `
                        Natural Disasters<br>
                        The Solar System<br>
                        Seasons<br>
                        Eclipses<br>
                        Tides<br>
                        Resources<br>
                        The Water Cycle<br>
                        Active Earth<br>
                        Minerals<br>
                        Fungi<br>
                        Mass Extinctions<br>
                        Comets<br>
                        Earth Systems<br>
                    `,
                },
                
            ]
        },
        {
            type: 'media-comp',
            layoutKey: 'j',
            media: [
                {
                    type: 'media',
                    mediaType: 'image',
                    alt: 'Some alt text',
                    image: require('../../../src/assets/images/stileclassroom_01.png'),
                },
                {
                    type: 'media',
                    mediaType: 'image',
                    alt: 'Some alt text',
                    image: require('../../../src/assets/images/stileclassroom_02.png'),
                },
                {
                    type: 'media',
                    mediaType: 'image',
                    alt: 'Some alt text',
                    image: require('../../../src/assets/images/stileclassroom_03.png'),
                    caption: 'text'
                }
            ]
        },
        {
            type: 'heading',
            textAlignment: 'center', 
            text: 'For each topic, you’ll find a combination of'
        },
        {
            type: 'text-grid',
            alignment: 'center',
            itemAlignment: 'center',
            items: [
                {
                    icon: require('../../../src/assets/icons/text-grid/assessments.svg'),
                    heading: 'Formative and summative assessments',
                },
                {
                    icon: require('../../../src/assets/icons/text-grid/experiments.svg'),
                    heading: 'Experiments and projects',
                },
                {
                    icon: require('../../../src/assets/icons/text-grid/skills.svg'),
                    heading: 'Skill builders',
                },
                {
                    icon: require('../../../src/assets/icons/text-grid/careers.svg'),
                    heading: 'STEM career profiles',
                },
                {
                    icon: require('../../../src/assets/icons/text-grid/engineering.svg'),
                    heading: 'Engineering challenges',
                },
                {
                    icon: require('../../../src/assets/icons/text-grid/socratic.svg'),
                    heading: 'Socratic seminars',
                },
                {
                    icon: require('../../../src/assets/icons/text-grid/science_news.svg'),
                    heading: 'Science news',
                },
                {
                    icon: require('../../../src/assets/icons/text-grid/escape.svg'),
                    heading: 'Escape room challenges',
                },
                {
                    icon: require('../../../src/assets/icons/text-grid/videos.svg'),
                    heading: 'Videos',
                },
                {
                    icon: require('../../../src/assets/icons/text-grid/augvr.svg'),
                    heading: 'Augmented and virtual reality',
                },
                {
                    icon: require('../../../src/assets/icons/text-grid/simulations.svg'),
                    heading: 'Simulations and interactives',
                },
                {
                    icon: require('../../../src/assets/icons/text-grid/glossary.svg'),
                    heading: 'Glossary of terms',
                },
            ]
        },
        {
            type: 'media-comp',
            layoutKey: 'b',
            media: [
                {
                    type: 'media',
                    mediaType: 'image',
                    alt: 'Some alt text',
                    image: require('../../../src/assets/images/stileclassroom_04.png'),
                },
                {
                    type: 'media',
                    mediaType: 'image',
                    alt: 'Some alt text',
                    image: require('../../../src/assets/images/stileclassroom_05.png'),
                },
                {
                    type: 'media',
                    mediaType: 'image',
                    alt: 'Some alt text',
                    image: require('../../../src/assets/images/stileclassroom_06.png'),
                },
                {
                    type: 'media',
                    mediaType: 'image',
                    alt: 'Some alt text',
                    image: require('../../../src/assets/images/stileclassroom_07.png'),
                }
            ]
        },
        {
            type: 'heading',
            textAlignment: 'left', 
            text: 'We go beyond simply covering the curriculum'
        },
        {
            type: 'text',
            text: `
                <br>

                <h3>Build the skills needed for tomorrow’s jobs</h3>
                We’ve analysed the research on which skills will be the most important for tomorrow’s jobs. These skills, such as critical thinking, creative problem-solving and evidence-based reasoning, are carefully interwoven through all of our resources.
                <br><br>

                <h3>Keep it modern, relevant and always up-to-date</h3>
                Every month, Stile makes brand-new science resources available to your teachers. This means that your teachers always have access to the very best resources, and your students are learning in the context of the news and events happening around them.
                <br><br>

                <h3>Nurture informed, reflective young citizens</h3>
                We specifically craft our resources around relevant, real-world issues for students to discuss, analyse and debate. Learning about issues such as climate change, vaccination and genetically modified foods, help build students’ scientific literacy in a context that’s relevant to them.
                <br><br>
            `
        },
        {
            type: 'page-links',
            subHeading: 'What is Stile?',
            heading: 'Tour of Stile',
            text: 'Check out our app that powers a Stile classroom',
            button: { 
                url: '/what-is-stile/tour-of-stile',
                external: false,
                text: 'Learn more →'
            }
        },
        {
            type: 'cta',
        },
        
    ]
}

export default data