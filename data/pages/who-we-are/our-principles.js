const data = {
    title: 'Our principles',
    menuOrder: 2,
    seo: {
        title: 'Our principles',
        description: 'The core beliefs that guide everything we do', 
        opengraph_image: require('../../../src/assets/images/coverimage_our_principles.jpg'),
    },
    pageHero: {
        image: require('../../../src/assets/images/coverimage_our_principles.jpg'),
        title: 'Our principles',
        description: `The core beliefs that guide everything we do`,
    },
    blocks: [
        {
            type: 'text',
            text: `
                Countless questions arise when creating science resources; questions such as “How can we truly enhance learning in the classroom?” and “How do we most clearly explain this concept?”. To help us best answer these questions, and more, we are constantly guided by our principles.
            `
        },
        {
            type: 'heading',
            textAlignment: 'left',
            text: 'Teachers are of the utmost importance, we need to support them'
        },
        {
            type: 'text',
            text: `
                We believe that teachers are the single most important part of a student’s education. We want to help teachers perform at their best, and believe that a great learning resource should both save teachers time and provide them with powerful learning tools.
            `
        },
        {
            type: 'heading',
            textAlignment: 'left',
            text: 'Prepare students for life, not just exams'
        },
        {
            type: 'text',
            text: `
                A great education extends beyond the curriculum. We believe that it is our duty to provide students with the skills and knowledge they need to prosper in life, not just to pass their exams.
            `
        },
        {
            type: 'heading',
            textAlignment: 'left',
            text: 'One size doesn’t fit all'
        },
        {
            type: 'text',
            text: `
                Every school is different, every teacher is different, and every student is different. We believe that learning resources should cater to a wide range of students and should be easy for teachers to customise for their students, as needed.
            `
        },
        {
            type: 'heading',
            textAlignment: 'left',
            text: 'Technology should empower teachers, not replace them'
        },
        {
            type: 'text',
            text: `
                Many online resources sideline the teacher, leaving students in isolation. We believe the teacher should be in the driver's seat, at the centre of the classroom. Classroom technology should empower teachers, helping to enhance their practice, not change it.
            `
        },
        {
            type: 'heading',
            textAlignment: 'left',
            text: 'Quality is paramount'
        },
        {
            type: 'text',
            text: `
                Our team has a relentless commitment to quality and are constantly striving to make Stile better. This commitment is what sets us apart, whether it's our lessons, platform, Professional Development or even our customer support. Take our curriculum design process: all of our lessons are created by our in-house team of teachers, writers, illustrators and videographers. Every lesson is thoroughly tested in the classroom prior to release, lessons are reviewed for accuracy and quality by teachers and practicing scientists, and instead of cutting corners and using freely-available (yet often poor quality) resources from the internet, Stile’s team creates custom-made images, videos and simulations to ensure that students are consistently met with high quality, age-appropriate content in every lesson.
            `
        },
        {
            type: 'page-links',
            subHeading: 'Who we are',
            heading: 'Our team',
            text: 'We share a passion for great science education.',
            button: { 
                url: '/who-we-are/our-team',
                external: false,
                text: 'Learn more →'
            }
        },
        {
            type: 'cta',
        },
    ]
}

export default data