const data = {
    title: 'Join the team',
    menuOrder: 3,
    seo: {
        title: 'Join the team',
        description: '', 
        opengraph_image: require('../../../src/assets/images/coverimage_stile_team.png'),
    },
    pageHero: {
        image: require('../../../src/assets/images/coverimage_stile_team.png'),
        title: 'Join the team',
    },
    blocks: [
        {
            type: 'text',
            text: `
            Stile is run by the people who started and own the company. We’re here to build something real and enduring. A company that in years to come we look back on and are proud of what we achieved, and how we achieved it.
            <br><br>

            We want Stile to be an awesome workplace; a workplace for people who love their job. We aspire to build a team of exceptional individuals who are a genuine delight to work with every day. We firmly believe that working with outstanding people is what makes getting up and going to work delightful on an ongoing basis. That and having a strong purpose to your work.
            <br><br>
            
            Stile is a place for people who want to have a bigger impact. For people who believe there is room to improve our education system. It’s for people who care deeply about quality. It’s for people who prefer the outsized impact you can have, and the quick decisions that can be made, in a smaller company. It’s for people who value the team over the player. It’s a place for people who love working with great people while having fun doing it.
            `
        },
        {
            type: 'media-comp',
            layoutKey: 'g',
            media: [
                {
                    type: 'media',
                    mediaType: 'image',
                    alt: 'Some alt text',
                    image: require('../../../src/assets/images/join_team_01.jpg'),
                },
                {
                    type: 'media',
                    mediaType: 'image',
                    alt: 'Some alt text',
                    image: require('../../../src/assets/images/join_team_02.jpg'),
                },
                {
                    type: 'media',
                    mediaType: 'image',
                    alt: 'Some alt text',
                    image: require('../../../src/assets/images/join_team_03.jpg'),
                },
                {
                    type: 'media',
                    mediaType: 'image',
                    alt: 'Some alt text',
                    image: require('../../../src/assets/images/join_team_04.jpg'),
                },
                {
                    type: 'media',
                    mediaType: 'image',
                    alt: 'Some alt text',
                    image: require('../../../src/assets/images/join_team_05.jpg'),
                },
            ]
        },
        {
            type: 'heading',
            text: `We need your help`
        },
        {
            type: 'text',
            text: `
                If you’re interested in joining the team, we’d love to hear from you. Before you apply, make sure you’ve read our values and Behaviours of highly effective Stilists. You may also want to take a look at the process for joining the team.
                <br><br>
                
                If you don’t see yourself on that list, consider sending us an email anyway. Maybe you’ll blow our socks off and we’ll make a position for you. What’s the worst that could happen?
            `
        },
        {
            type: 'jobs',
            heading: 'Open positions',
            showAll: true,
        },
        {
            type: 'page-links',
            subHeading: 'Who we are',
            heading: 'The Stile team',
            text: 'We share a passion for great science education',
            button: { 
                url: '/who-we-are/our-team',
                external: false,
                text: 'Learn more →'
            }
        },
        {
            type: 'cta'
        }
    ]
}

export default data