const data = {
    title: 'Our team',
    menuOrder: 3,
    seo: {
        title: 'Our team',
        description: 'We share a passion for great science education', 
        opengraph_image: require('../../../src/assets/images/coverimage_our_team.png'),
    },
    pageHero: {
        image: require('../../../src/assets/images/coverimage_our_team.png'),
        title: 'Our team',
        description: `We share a passion for great science education`,
    },
    blocks: [
        {
            type: 'text',
            text: `
                Stile was founded by Alan Finkel, Byron Scaf and Danny Pikler, who together own and operate the company. Alan has temporarily stepped away from the company while he serves as Australia's Chief Scientist.
                <br><br>

                Every aspect of Stile is crafted in-house by our passionate team of teachers, scientists, artists and engineers - a multidisciplinary group of the best and brightest. You’ll find Stilists scattered all around Australia and New Zealand, but the majority of the team is based in Melbourne, Australia.
            `
        },
        {
            type: 'team',
            heading: 'Leadership',
            showBackground: true,
            people: [
                'Byron Scaf',
                'Clare Feeney',
                'Daniel Rodgers-Pryor',
                'Jaclyn Rooney',
                'Daniel Pikler',
                'Sarah Lindop',
            ]
        },
        {
            type: 'team',
            heading: 'The dream team',
            showContactInfo: false,
            showAll: true,
        },
        {
            type: 'page-links',
            subHeading: 'Who we are',
            heading: 'Join the team',
            text: 'We share a passion for great science education',
            button: { 
                url: '/who-we-are/join-the-team',
                external: false,
                text: 'Learn more →'
            }
        },
        {
            type: 'cta',
        }
    ]
}

export default data