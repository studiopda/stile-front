const data = {
    title: 'Our why',
    menuOrder: 1,
    seo: {
        title: 'Our why',
        description: `It’s more important than ever that our young citizens graduate from school scientifically literate, and ready to tackle tomorrow’s problems`,
        opengraph_image: require('../../../src/assets/images/coverimage_why_we_started_stile.jpg'),
    },
    pageHero: {
        image: require('../../../src/assets/images/coverimage_why_we_started_stile.jpg'),
        title: 'Our why',
        description: `It’s more important than ever that our young citizens graduate from school scientifically literate, and ready to tackle tomorrow’s problems`,
    },
    blocks: [
        {
            type: 'text',
            text: `
                A fundamental understanding of science is an integral part of being an informed, reflective citizen. As citizens, we’re being asked to make choices on science-related issues such as climate change, renewable energy, genetically modified foods and vaccinations. And we’re being asked to do so in a world of “fake news” and “alternative facts”, where it’s up to us as individuals to critically evaluate the quality of the evidence.
                <br><br>
                
                Scientifically literate students know that science is a powerful tool for understanding the natural world and is the foundation of the technologies that can shape it. But they also understand the limitations of science as it is practised today, and can clearly separate the science from political and social considerations.
                <br><br>
                
                These students can confidently explain phenomena scientifically, and evaluate the rigour of scientific inquiry. They’re comfortable participating in intelligent, reasoned discourse about social issues because they have a solid working knowledge of the science and technologies behind them.
                <br><br>
                
                These young citizens are sceptical – they’re less likely to take an idea or opinion on face value. They’re able to critically evaluate an argument or point of view, make sense of data, evaluate the evidence and form their own conclusions.
                <br><br>
                
                We are passionate about helping all of our students – now and in the future – develop these skills and reach their full capabilities so they can graduate as confident young citizens of the future.
                <br><br>            
            `
        },
        {
            type: 'text',
            text: `
                <em>“The solutions to political and ethical dilemmas involving science and technology cannot be the subject of informed debate unless young people possess certain scientific awareness”.</em>
                <br><br>
                <strong>— PISA 2015 Assessment and Analytical Framework</strong>
            `
        },
        
        {
            type: 'page-links',
            subHeading: 'Who we are',
            heading: 'Our principles',
            text: 'The core beliefs that guide everything we do',
            button: { 
                url: '/who-we-are/our-principles',
                external: false,
                text: 'Learn more →'
            }
        },
        {
            type: 'cta',
        },
    ]
}

export default data