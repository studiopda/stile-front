const data = {
    title: 'Home',
    seo: {
        title: '',
        description: '', 
        opengraph_image: '',
    },
    watchStileVideo: {
        mediaType: 'video',
        video: 'https://youtu.be/jcLFR8p8nDw',
        modal: true,
        autoplay: true, // initial behaviour
        loop: true,
    },
    blocks: [
        {
            type: 'heading',
            text: `Australia’s #1 Science Teaching Resource`,
            collapsedText: `Australia’s #1 Science Teaching Resource`  
        },
        {
            type: 'text',
            text: 'Think of Stile as the combination of your favourite science textbook, workbook, assessments, videos, and simulations. All interwoven and presented as a seamless online teaching and learning experience.'
        },
        {
            type: 'media',
            mediaType: 'video',
            alt: null,
            video: 'https://youtu.be/jcLFR8p8nDw',
            caption: 'text',    
    
            // Modal
    
            coverImage: require('../../src/assets/images/stile-in-action.jpg'),
            buttonText: 'Watch Stile in action (2 mins)',
    
            // Behaviour 
    
            modal: true,
            autoplay: true, // initial behaviour
            loop: true,
        },
        {
            type: 'heading',
            text: 'It’s used in real classrooms',
            collapsedText: 'It’s used in real classrooms to'
        },
        {
            type: 'text',
            text: 'Rich, interactive activities for your classroom, covering every outcome of the years 7–10 science    curriculum.'
        },
        {
            type: 'media-comp',
            layoutKey: 'b',
            media: [
                {
                    type: 'media',
                    mediaType: 'image',
                    alt: 'Some alt text',
                    image: require('../../src/assets/images/home_01.png'),
                },
                {
                    type: 'media',
                    mediaType: 'image',
                    alt: 'Some alt text',
                    image: require('../../src/assets/images/home_02.png'),
                },
            ]
        },
        {
            type: 'heading',
            text: 'Engage students in real-world science',
            collapsedText: 'Engage students in real-world science'
        },
        {
            type: 'text',
            text: 'Stile is built around the latest science news and the global issues that are important to your students.'
        },
        {
            type: 'media-comp',
            layoutKey: 'j',
            caption: `
                From our monthly Art of Science series: (from left) <i>Celebrating 50 Years of Space Travel, Global collaboration on minimising the impact of Coronavirus, Plastics and our oceans ecosystems</i>
            `,
            media: [
                {
                    type: 'media',
                    mediaType: 'image',
                    alt: 'Some alt text',
                    image: require('../../src/assets/images/home_03.png'),
                },
                {
                    type: 'media',
                    mediaType: 'image',
                    alt: 'Medical Learning',
                    image: require('../../src/assets/images/home_04.png'),
                },
                {
                    type: 'media',
                    mediaType: 'image',
                    alt: 'Illustrated Whale',
                    image: require('../../src/assets/images/home_05.png'),
                },
            ]
        },
        {
            type: 'heading',
            text: 'And help them build the skills they’ll need to thrive',
            collapsedText: 'and help them build the skills they’ll need to thrive.'
        },
        {
            type: 'text',
            text: 'Get students observing, thinking, writing and arguing like real scientists and engineers. Stile supports the development of critical thinking, science skills and general capabilities through the prism of the science classroom.'
        },
        {
            type: 'media-comp',
            layoutKey: 'i',
            media: [
                {
                    type: 'media',
                    mediaType: 'image',
                    alt: 'Some alt text',
                    image: require('../../src/assets/images/home_07.png'),
                },
                {
                    type: 'media',
                    mediaType: 'image',
                    alt: 'Some alt text',
                    image: require('../../src/assets/images/home_06.png'),
                },
                {
                    type: 'media',
                    mediaType: 'image',
                    alt: 'Some alt text',
                    image: require('../../src/assets/images/home_08.png'),
                },
            ]
        },
        {
            type: 'heading',
            text: 'You’ll see the difference'
        },
        {
            type: 'text',
            text: `
                <h3>More classroom discussions around real-world science issues</h3>
                Every unit of work is set in the context of real-world science discoveries and events, helping spark conversation and debate in the classroom.
                <br><br>

                <h3>Teachers differentiating their instruction more frequently</h3>
                Every lesson can be customised, allowing teachers to quickly tailor content to the needs of individual students.
                <br><br>

                <h3>Students pondering hard problems</h3>
                Every lesson is scaffolded from lower- to higher-order thinking, helping to ensure that every student is challenged at their level.
                <br><br>
            `
        },
        {
            type: 'button',
            button: { 
                url: '/what-is-stile/benefits',
                external: false,
                text: 'More benefits →'
            }
        },
        {
            type: 'page-links',
            subHeading: 'What is Stile?',
            heading: 'Stile classroom',
            text: 'Cutting edge resources for direct instruction and self-paced learning',
            button: { 
                url: '/what-is-stile/classroom',
                external: false,
                text: 'Learn more →'
            }
        },
        {
            type: 'cta',
        },
    ]
}

export default data