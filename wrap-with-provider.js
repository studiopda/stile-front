import React from 'react'
import Transition from "./src/transition"
import { GlobalStyles } from './src/styles/global';
import LocaleProvider from './src/locale'

export default ({ element, props  }) => {
    return (
        <>
            <GlobalStyles/> 
            <LocaleProvider>
                {element}
            </LocaleProvider>
        </>
    )
}