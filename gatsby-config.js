require("dotenv").config();

module.exports = {
    siteMetadata: {
        title: ``,
        description: ``,
        author: `@studiopda`,
    },
    plugins: [
        `gatsby-plugin-react-helmet`,
        `gatsby-plugin-styled-components`,
        {
            resolve: `gatsby-source-filesystem`,
            options: {
                name: `pages`,
                path: `${__dirname}/data/pages`,
                ignore: [
                    `**/page-template.js`
                ]
            },
        },
        {
            resolve: `gatsby-source-filesystem`,
            options: {
                name: `jobs`,
                path: `${__dirname}/data/jobs`,
                ignore: [
                    `**/page-template.js`
                ]
            },
        },
        {
            resolve: `gatsby-plugin-s3`,
            options: {
                bucketName: "stile-marketing-2020-stag ing",
            },
        },
        {
            resolve: 'gatsby-plugin-remove-console',
            options: {
                exclude: ['error', 'warn'],
            }
        },
        // {
        //     resolve: `gatsby-plugin-google-analytics`,
        //     options: {
        //       trackingId: "UA-*******"
        //     },
        // },
    ],
}
