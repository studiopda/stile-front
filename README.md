## Stile Marketing 2020

React / Gatsby powered marketing site for Stile Education.  


**Data** 

All editable data is located in the /data folder in the root of the repo.

The folder / file structure is as follows: 


```
– data 
	– forms 
	– pages 
		– top-level-page.js (ie: home)
		– what-is-stile (section folder)
			– page.js 
	– templates (data templates for creating new pages)
		– default-blocks.js (overview of all blocks with test data)
		- page-template.js (base file for creating new pages)
	- global.js (non-page specific data)
	– team.js (all team data)
```

All editable pages can be found under the `/pages` directory.

Page slugs are generated from the file names.

Pages that belong to a top level section (ie: what-is-stile) are nested within a directory with a section slug. This generates the structure shown in the navigation on the frontend and the nested url structure (ie: what-is-stile/stile-classroom/). 

To create a new page copy `page-template.js` and paste within the relevant section under `/pages`.  Rename this file, populate the keys and begin add to blocks to the `blocks` array key. You can reference `/templates/default-blocks.js` to see which keys are required per block and how to use them correctly. 

Global data is handled in the `global.js`. This relates to all non-page specific editable data such as the footer and CTA Block defaults. 

Image assets will need to be added under `/src/assets` and then included via a require function (ie: `require('../../../src/assets/images/image.png')`, ). This path will be relative to where the page file is located so ensure it's correct if you find an image isn't displaying. Modern code editors such as Visual Studio Code will help autocomplate these paths as you type. 


**Getting Started**

Ensure you have node installed along with the Gatsby CLI per these instructions: https://www.gatsbyjs.org/tutorial/part-zero/

Once you've done that, clone the repo and install all the dependancies via `yarn`  

Now we're ready to start the repo. To do so simply run `yarn start` and visit the URL show in the console output. 



