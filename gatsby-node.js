const path = require('path')
const fs = require('fs')
const createPages = require('./gatsby/createPages')
const createJobsPages = require('./gatsby/createJobsPages')
const { createFilePath } = require('gatsby-source-filesystem')

exports.createPages = ({ graphql, actions }) => {
    const { createPage } = actions
    createPages(createPage, graphql)
    createJobsPages(createPage, graphql)
}

exports.onCreateWebpackConfig = ({ stage, loaders, actions }) => {
    if (stage === 'build-html') {
        actions.setWebpackConfig({ 
            module: {
                rules: [
                    {
                    test: /react-mapbox-gl/,
                    use: loaders.null(),
                    },
                    {
                    test: /mapbox-gl/,
                    use: loaders.null(),
                    },
                ],
			}
        })
    }
}