import axios from 'axios'
import { forEach } from 'lodash'

function postData(endpoint, data) {

    // console.log(JSON_URL, endpoint, data)

    let config = {
        headers: {
            "Cache-Control": "no-cache",
            "Content-Type": "application/x-www-form-urlencoded"
        }
    }     

    return new Promise((resolve, reject) => {
        axios.post(endpoint, data, config)
            .then((response) => {
                console.log('response',response);
                if (response.data) {
                    resolve(response.data);
                }
            })
            .catch(error =>  {
                console.log('error',error);
                reject(error)
            })   
    })
}


export const handleSignup = (fields, resetForm, updateForm) => {       
    const { formComplete } = fields;
    
	const data = {   
        oid: '00D7F000001tu7R',
        retURL: 'https://stileeducation.com',
        lead_source: 'Inbound Lead - Website Form',
		email: fields.email.value,
		first_name: fields.firstName.value,
        last_name: fields.lastName.value,
        company: fields.company.value,
        phone: fields.phone.value,
        // moreInfo: fields.moreInfo.value,
        // bookChat: fields.bookChat.value,
        // bookSchoolEvaluation: fields.bookSchoolEvaluation.value
    }
    
    // URL Encode

    const params = new URLSearchParams();

    forEach(data, (value, key) => {
        params.append(key, value);
    })
    
    postData('https://webto.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8', params)
        .then(res => {
			console.log(res)
            if (res.success) {
                updateForm({
                    formComplete: true
                })	
            } else {
                // updateForm({
                // 	error: res.errors[0],
                // 	submitting: false
                // })
            }
            
            resetForm()
        })
        .catch(error =>  {
            updateForm({
                error: 'Server Error',
                submitting: false
            })
            resetForm()
        })  
    
}
