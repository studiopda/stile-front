import React, {Component} from 'react'
import styled, { css } from 'styled-components'
import { rgba } from 'polished'

import { media } from '../../styles/utils'
import { bgIcon } from '../../styles/global'
import { subheading } from '../../styles/type'
import { black, red } from '../../styles/colors'

class Input extends Component {

    state = {
        focused: false
    }

    onBlur = () => {
        const { toggleFocus } = this.props;

        this.setState({ focused: false })

        if (toggleFocus) {
            toggleFocus(false)
        }
    }

    onFocus = () => {
        const { toggleFocus } = this.props;

        this.setState({ focused: true })

        if (toggleFocus) {
            toggleFocus(true)
        }
    }

    onChangeValue = (e) => {
        const { validator, onChangeValue } = this.props;
        const value = e.target.value;
        
        if (onChangeValue) {
            onChangeValue(value, validator)
        }
    }

    shouldShowError = () => {
        const { focused } = this.state;
        const { shouldValidate, isValid } = this.props;

        if (shouldValidate === true && !isValid) {
            return true
        }

        if (shouldValidate && !isValid && !focused) {
            return true
        }
    }

    getInputProps = () => {
        const { value, label, type, placeholder, className, disabled, name, locale, id, icon} = this.props;
        const showError = this.shouldShowError();

        return {
            className: className,
            id: name,
            type: type,
            placeholder: placeholder,
            value: value,
            disabled: disabled,
            hasIcon: icon,
            onChange: !disabled && this.onChangeValue,
            error: showError,
            onFocus: this.onFocus,
            onBlur: this.onBlur,
        }
    }
    
    render() {
        const { focused } = this.state;
        const { errorMessage, helperText, id, icon, type, label, validator, component } = this.props;
        const showError = this.shouldShowError();
        const CustomComponent = component || false;
        // console.log(this.props)
        
        return (
            <Wrapper
                id={id}
                className={[id, 'dynamic-field']}
            >
                <FieldWrapper
                     className={'field-wrapper'}
                >

                    {label && (
                        <Label>
                            {label}
                        </Label>
                    )}

                    <Field
                        className={'field'}
                        error={showError}
                    >
                        {!CustomComponent && (
                            <>
                                {type == 'textarea' ? 
                                    <TextField 
                                        {...this.getInputProps()}
                                    />
                                :
                                    <InputField 
                                        {...this.getInputProps()}
                                    />
                                }   
                            </>
                        )}

                        {CustomComponent && (
                            <CustomComponent
                                {...this.props}
                                {...this.props.stripeOptions}
                            />
                        )}
                        
                    </Field>

                    
                </FieldWrapper>

                {showError && (
                    <Error
                        className={'error'}
                    >
                        {errorMessage}
                    </Error>
                )}

            </Wrapper>
        )
    }
}


export const Wrapper = styled.div`
    position: relative;
    width: 100%;
`

const FieldWrapper = styled.div`

`

// Field


export const inputStyle = css`
    appearance: none;
    box-shadow: none;
    display: flex;
    flex: 1;
    height: 48px;
    width: 100%;
    box-sizing: border-box;
    border: 1px solid #CED0D6;
    background: transparent;
    padding: 0 12px;
    border-radius: 4px;
    font-size: 16px;

    &::placeholder {
        color: ${black};
        opacity: 0.5;
    }

    &:hover {

    }

    transition: all 0.25s ease;

    &.active, &:focus {
        outline: none;
        border: 1px solid #00CB50;
        box-shadow: 0px 0px 0px 1px #00CB50;
    }

    ${props => {
        if (props.disabled) return css`
            pointer-events: none;
            
            &, &::placeholder {
                color: rgba(0, 0, 0, 0.2);
            }
        `
    }}

    ${props => {
        if (props.hasIcon) return css`
            padding-left: 43px;
            padding-bottom: 2px;
        `
    }}
`

const Field = styled.div`
    display: flex;
    width: 100%;
    position: relative;

    select {
        ${inputStyle}
        border-radius: 0;
    }
`

export const InputField = styled.input`
    ${inputStyle}
    min-width: 110px;
`

export const TextField = styled.textarea`
    ${inputStyle}
    height: 270px;
    resize: vertical;
    padding-top: 12px;
    box-sizing: border-box;
`

// Label

const Info = styled.div`
    display: flex;
    flex-direction: column;
    align-items: flex-end;
    margin-right: 20px;

    ${media.phone`
        display: none;
    `}   
`

const Label = styled.div`
    font-size: 14px;
    margin-bottom: 10px;

    ${props => {
        if (props.error) return css`
            color: ${red}
        `
    }}
`


const message = css`
    font-size: 14px;
`

// Helper Text

export const HelperText = styled.div`
    ${message}
    margin-bottom: 10px;
`

// Error

export const Error = styled.div`
    ${message}
    color: ${red} !important; 
    margin-top: 8px;
	margin-left: auto;
    margin-bottom: 10px;
`

export default Input;