import React from 'react'
import styled, { css } from 'styled-components'

import { media } from '../../styles/utils'
import { container, bgIcon, padding, hoverState } from '../../styles/global'
import { resolveBlock, resolveContainer, resolveLocalisation } from './utils';
import { withLocale } from '../../locale';

const Block = (props) => {
	const { layout, lastBlock, block_type, nextBlock, noContainer, locale} = props;
	const BlockLayout = resolveBlock(layout);
	const hasContainer = resolveContainer(layout)
	const content = resolveLocalisation(props);

	if (!BlockLayout) return <></>

	return (
		<Wrapper
			className={`block_${layout}`}
			layout={layout}
			blockType={block_type}
			blockProps={props}
			styles={BlockLayout && BlockLayout.wrapper}
			lastBlock={lastBlock}
			nextBlock={nextBlock}
		>
			{BlockLayout && (
				hasContainer ?
					<Container
						className={'block-container'}
					>
						<BlockLayout {...content}/>
					</Container>	
				:
				<BlockLayout {...content}/>
			)}
		</Wrapper>
	)
}

const Wrapper = styled.div`
	display: flex;	
	flex-direction: column;
	align-items: center;
    width: 100%;
	margin-bottom: 88px;

	${media.phone`
		margin-bottom: 40px;
	`}
		
	/* Block specific container styles */

	${props => {
		if (props.styles) return props.styles
	}}

	/* Remove padding on last block */
	
	${props => {
		if (props.lastBlock) return css`
			margin-bottom: 0 !important;
		`
	}}

	/* Reduce padding */

	${props => {
		if (props.nextBlock && props.nextBlock.type == 'text') return css`
			margin-bottom: 42px;
		`
	}}

`

const Container = styled.div`
	${container}
	${padding}

	display: flex;
	flex-direction: column;
	align-items: center;
`


export default withLocale(Block)