import React from 'react'
import styled, { css } from 'styled-components'
import ProgressiveImage from 'react-progressive-image'

import { media } from '../../../styles/utils'
import { container, bgIcon, padding, hoverState, bgImage, innerContainer } from '../../../styles/global'
import { grey } from '../../../styles/colors'
import { body } from '../../../styles/type'

const BlockText = (props) => {
	const { text } = props;
        
	return (
		<Wrapper>
			<Description
				dangerouslySetInnerHTML={{__html: text}}  
			/>
		</Wrapper>
	)
}

const Description = styled.div`
	&, p, h3 {
		${body}
		color: ${grey}
	}

	h3 {
		font-weight: 400;
		margin-bottom: 8px;
	}
`

const Wrapper = styled.div`
	${innerContainer}
`

BlockText.wrapper = css`

`

export default BlockText
