import React, { useState } from 'react'
import styled, { css } from 'styled-components'

import { media, useBreakpoint } from '../../../styles/utils'
import { container, bgIcon, padding, hoverState, bgImage } from '../../../styles/global'
import { green2 } from '../../../styles/colors'

const BlockListColumns = ({items, ...props}) => {
	const isPhone = useBreakpoint('phone');

	const renderColumns = () => {
		if (!items) return

		const columns = items.map((item, i) => {
			const [collapsed, setCollapsed] = useState(isPhone ? true : false)

			return (
				<Column
					key={i}
					collapsed={collapsed}
				>
					<ColumnHeader
						onClick={() => setCollapsed(!collapsed)}
					>
						<Heading
							dangerouslySetInnerHTML={{__html: item.heading}}
						/>
						<Toggle
							collapsed={collapsed}
						/>
					</ColumnHeader>

					<Description
						dangerouslySetInnerHTML={{__html: item.text}}
					/>
				</Column>
			)
		})

		return <Columns>{columns}</Columns>
	}

	return (
		<Wrapper>
			{renderColumns()}
		</Wrapper>
	)
}

// Shared

const Heading = styled.div``
const Subheading = styled.div``
const Description = styled.div``

// Layout

const Wrapper = styled.div`
	width: 100%;
`

// Columns

const Columns = styled.div`
	display: flex;
	flex-flow: row wrap;
	width: 100%;
`

const Toggle = styled.div`
	display: none;
	width: 14px;
	height: 14px;
	flex: 0 1 14px;
	${bgIcon};
	background-image: url(${require('../../../assets/icons/accordion-close.svg')});
	transform: translateY(-3px);

	${media.phone`
		display: flex;
	`}

	${props => {
		if (props.collapsed) return css`
			background-image: url(${require('../../../assets/icons/accordion-open.svg')})
		`
	}}
`

const ColumnHeader = styled.div`
	${Heading} {
		font-family: 'Boing';
		font-weight: 500;
		font-size: 32px;
		line-height: 40px;
		color: ${green2};
		margin-bottom: 12px;
	}

	${media.phone`
		display: flex;
		justify-content: space-between;
		align-items: center;
		min-height: 64px;
		border-top: 1px solid #E5E5E5;

		${Heading} {
			font-size: 24px;
			line-height: 32px;
			margin-bottom: 0;
		}
	`}
`

// Column

const Column = styled.div`
	flex: 1;
	padding-right: 32px;

	${Description} {
		font-size: 16px;
		line-height: 34px;
		color: #4E4C49;
	}

	${media.tablet`
		flex: 0 1 50%;
		margin-bottom: 40px;
	`}

	${media.phone`
		flex: 1 0 100%;
		padding-right: 0;
		margin-bottom: 0;

		${Description} {
			margin-bottom: 40px;
		}

		${props => {
			if (props.collapsed) return css`
				${Description} {
					display: none;
				}
			`
		}}
	`}

`


BlockListColumns.wrapper = css`

`

export default BlockListColumns
