import React, { useState } from 'react'
import { navigate, Link, useStaticQuery } from 'gatsby'
import styled, { css } from 'styled-components'
import { filter, orderBy, forEach, find, countBy, compact } from 'lodash'
import ProgressiveImage from 'react-progressive-image'

import { media } from '../../../styles/utils'
import { container, bgIcon, padding, hoverState, bgImage } from '../../../styles/global'
import { heading1, body } from '../../../styles/type'

import categoryData from '../../../../data/jobs'
import { getJobData } from '../../../utils'
import { useMount } from 'react-use'
import { black, green } from '../../../styles/colors'
import selectArrow from '../../../assets/icons/select-arrow.svg'

const BlockJobs = (props) => {
	const { showAll, category: jobCategory } = props;
	const [category, setCategory] = useState(jobCategory || 'all')
	const [location, setLocation] = useState('all')
	const jobPages = useStaticQuery(query).allSitePage.nodes
	const jobsData = []
	const categories = categoryData.categories
	const locations = categoryData.locations

    forEach(jobPages, (page, i) => {
        const data = getJobData(page.context.relativePath);
        jobsData.push({
            ...page,
            ...data
        })
	})

	const filterJobs = () => {
		let sections = []

		// filter by category
		forEach(categories, (item) => {
			if (item.slug === 'all') {
				sections.push({
					...item,
					jobs: jobsData,
					count: jobsData.length,
				})
			} else {
				let jobs = filter(jobsData, {categories: [item.slug]})
				sections.push({
					...item,
					jobs: jobs,
					count: jobs.length,
					active: category == item.slug || category === 'all'
				})
			}
		})

		// filter by location
		if (location !== 'all') {
			forEach(sections, (item) => {
				let jobs = filter(item.jobs, {location: [location]})
				item.jobs = jobs
				item.count = jobs.length
			})
		}

		return sections
	}

	const renderInfo = () => {
		const { heading, text } = props;

		return (
			<Info>
				<Heading>{heading}</Heading>
				{showAll && renderLocationSelect()}
				{showAll && renderNav()}
			</Info>
		)
	}

	const onLocationSelect = (e) => {
		setLocation(e.target.value)
	}

	const renderLocationSelect = () => {
		const items = locations.map((item, i) => {
			return (
				<option
					key={i}
					value={item.slug}
				>
					{item.name}
				</option>
			)
		})
		return (
			<LocationSelect>
				<select
					onChange={onLocationSelect}
				>
					{items}
				</select>
				<Icon src={selectArrow}/>
			</LocationSelect>
		)
	}

	const renderNav = () => {
		const sections = filterJobs()

		const items = sections.map((item, i) => {
			if (item.count === 0 && item.slug !== 'all') return

			return (
				<NavItem 
					key={i}
					active={category == item.slug}
					onClick={() => setCategory(item.slug)}
				>
					{`${item.name} (${item.count})`}
				</NavItem>
			)
		})
		
		return (
			<Nav>
				{items}
			</Nav>
		)
	}

	const renderJob = (job, sectionName, i) => {
		return (
			<Job
				key={i}
			>
				<JobLink to={job.path}>
					{job.title}
				</JobLink>

				<Items>
					{job.location && (
						<Item>{job.location}</Item>
					)}
					
					{sectionName && (
						<Item>{sectionName}</Item>
					)}
					
					{job.workType && (
						<Item>{job.workType}</Item>
					)}
				</Items>
			</Job>
		)
	}

	const renderSection = (section, i) => {
		const jobs = section?.jobs.map((job, i) => {
			if (job) return renderJob(job, section.name, i)
		})

		return (
			<Cat
				key={i}
			>
				<Heading>
					{`${section.name} (${jobs.length})`}
				</Heading>
				<Jobs>
					{jobs}
				</Jobs>
			</Cat>
		)
	}

	const renderGrid = () => {
		const sections = filterJobs()
		
		let items = sections.map((section, i) => {
			if (section.active && section.count > 0) {
				return renderSection(section, i)
			} else {
				return false
			}
		})

		// remove empty items
		items = compact(items)

		return (
			<>
				{items.length > 0 && (
					<Grid>
						{items}
					</Grid>
				)}
				{items.length === 0 && (
					<Grid>
						<NoResults>No results found</NoResults>
					</Grid>
				)}
			</>
		)
	}

	return (
		<Wrapper>
			{renderInfo()}
			{renderGrid()}
		</Wrapper>
	)
}

// Shared

const Heading = styled.div``
const Subheading = styled.div``
const Description = styled.div``
const Items = styled.div``
const Item = styled.div``

// Layout

const Wrapper = styled.div`
	display: flex;
	width: 100%;

	${media.tablet`
		flex-direction: column;
	`}
`

// Info

const Info = styled.div`
	display: flex;
	flex-direction: column;
	flex: 0 1 433px;
	margin-right: 24px;
	
	${media.tablet`
		flex: 0 1 auto;
		margin-bottom: 40px;
	`}

	${Heading} {
		${heading1};
		margin-bottom: 24px;
	}
`

// Location Select

const Icon = styled.img``
const LocationSelect = styled.div`
	position: relative;
    display: flex;
    width: 320px;
	max-width: 100%;

	select {
		appearance: none;
		box-shadow: none;
		display: flex;
		flex: 1;
		height: 48px;
		width: 100%;
		box-sizing: border-box;
		border: 1px solid #CED0D6;
		background: transparent;
		padding: 0 12px;
		border-radius: 4px;
		font-size: 16px;
		font-family: 'Graphik';
		font-weight: 300;

		&::placeholder {
			color: ${black};
			opacity: 0.5;
		}

		&:hover {

		}

		transition: all 0.25s ease;

		&.active, &:focus {
			outline: none;
			border: 1px solid #00CB50;
			box-shadow: 0px 0px 0px 1px #00CB50;
		}
	}

	${Icon} {
		z-index: 5;
		position: absolute;
		top: 50%;
		right: 21px;
		transform: translateY(-50%);
	}
`

// Nav

const NavItem = styled.div`
	${props => {
		if (props.active) return css`
			color: ${green};
			text-decoration: underline;
		`
	}}
`

const Nav = styled.div`
	display: flex;
	flex-direction: column;
	align-items: flex-start;
	margin-top: 24px;

	${NavItem} {
		${body};
		cursor: pointer;

		&:not(:last-child) {
			margin-bottom: 32px;

			${media.tablet`
				margin-bottom: 12px;
			`}
		}
	} 
`

// No Result

const NoResults = styled.div`
	${body}
`

// Grid

const Grid = styled.div`
	display: flex;
	flex-direction: column;
	flex: 1;
`

// Job Section (Category)

const Cat = styled.div`
	&:not(:first-child) {
		padding-top: 64px;
	}

	> ${Heading} {
		margin-bottom: 16px;
		${body}
		font-weight: 500;
	}
`

const Jobs = styled.div`
	display: flex;
	flex-direction: column;
`

// Job

const JobLink = styled(Link)``
const Job = styled.div`
	display: flex;
	flex-direction: column;
	padding-top: 24px;
	padding-bottom: 24px;
	border-bottom: 1px solid #CED0D6;

	${JobLink} {
		display: inline-block;
		${body};
		color: ${green};
		text-decoration: underline;
		${hoverState};
		cursor: pointer;
	}

	${Items} {
		display: flex;
		margin-top: 16px;

		${Item} {
			font-weight: 400;
			font-size: 14px;
			line-height: 24px;
			text-transform: uppercase;
			white-space: pre;

			&:not(:last-child)::after {
				content: ' · ';
			}
		}
	}
`

BlockJobs.wrapper = css`
	.block-container {
		max-width: 1380px;
	}
`

const query = graphql`
	query jobPages {
		allSitePage(filter: {path: {regex: "/jobs/"}}) {
			nodes {
				path
			  	context {
					section
					name
					relativePath
				}
			}
		}
	}
`

export default BlockJobs
