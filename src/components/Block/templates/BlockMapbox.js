import React from 'react'
import styled, { css, createGlobalStyle } from 'styled-components'
import ReactMapboxGl, { ZoomControl } from "react-mapbox-gl";
import mapboxgl from 'mapbox-gl'
import { MAPBOX_KEY } from '../../../constants';

import { media, isClient, useBreakpoint } from '../../../styles/utils'
import { container, bgIcon, padding, hoverState, bgImage } from '../../../styles/global'
import { body } from '../../../styles/type'

let MapBox = !isClient() ? () => <div/> : ReactMapboxGl({
    accessToken: MAPBOX_KEY,
});

const BlockMapbox = (props) => {
	const isPhone = useBreakpoint('phone')

	const createMapPopup = (e, map, popup) => {
        if (!e.features.length) return
		map.getCanvas().style.cursor = 'pointer';
		
        const school = e.features[0];
        const data = school.properties;

        if (data) {
            var coordinates = school.geometry.coordinates.slice();

            while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
                coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
            }

            popup.setLngLat(coordinates)
                .setHTML(`
                    <h3>${data.title}</h3>
                `)
                .addTo(map);
        }
    }

	const onMapLoad = (map) => {
        if (!isClient()) return;
        
        // School label hover

        var popup = new mapboxgl.Popup({
            closeButton: false,
            closeOnClick: false
        });

        map.on('mouseenter', 'stile-schools', (e) => {
            createMapPopup(e, map, popup)
        })

        map.on('mouseleave', 'stile-schools', (e) =>  {
            map.getCanvas().style.cursor = '';
            popup.remove();
        })
    }
		
	const renderMap = () => {
		return (
			<MapBox
				style="mapbox://styles/seanhurley01/ck6zn48bi1xum1iryclh8d0it"
				onStyleLoad={onMapLoad}
				center={[140.541578, -27.446216]}
				zoom={isPhone ? [1.5] : [3]}
			>
				<ZoomControl/>
			</MapBox>
		)
	}
	
	return (
		<Wrapper>
			{renderMap()}
		</Wrapper>
	)
}

// Shared

const Heading = styled.div``
const Subheading = styled.div``
const Description = styled.div``

// Layout

const Wrapper = styled.div`
	width: 100%;

	.mapboxgl-map {
        width: 100%;
		padding-bottom: 56.25%;

		${media.phone`
			height: 300px;
		`}
    }

	 /* Mapbox Popup */

	 .mapboxgl-popup {
        max-width: none !important;
        user-select: none;

        .mapboxgl-popup-content {
            max-width: 240px;
            box-shadow: 0 0 30px 0 rgba(0,0,0,0.2);
            padding: 12px 24px 10px;
			text-align: center;

            h3 {
				font-family: 'Graphik';
				font-size: 16px;
            }	
        }
    }


`

BlockMapbox.wrapper = css`
	.block-container {
		max-width: 1380px;
	}
`

export default BlockMapbox
