import React from 'react'
import styled, { css } from 'styled-components'

import { media, useBreakpoint, isClient } from '../../../styles/utils'
import { container, bgIcon, padding, hoverState, bgImage } from '../../../styles/global'
import BlockMedia from './BlockMedia'

const BlockMediaComp = (props) => {
	const { media, layoutKey } = props;
	const layout = layoutKey && layouts[layoutKey];
	const isPhone = useBreakpoint('phone');

	const wrapScroll = (items) => {
		return (
			<ScrollWrapper>
				{items}
				<Spacer/>
			</ScrollWrapper>
		)
	}
		
	const renderMedia = () => {
		if (!media || !layout) return;
		
		const items = media.map((item, i) => {
			const compStyles = layout[i];
			
			return (
				<BlockMedia
					compStyles={compStyles}
					imageScaling={!isPhone}
					{...item}
				/>
			)
		})

		return isPhone 
			? wrapScroll(items)
			: items
		// return items
	}	

	const renderCaption = (params) => {
		const { caption } = props;
		
		return (
			<Caption
				dangerouslySetInnerHTML={{__html: caption}}
			/>
		)
	}

	return (
		<Wrapper>
			{renderMedia()}
			{renderCaption()}
		</Wrapper>
	)
}

// Layout

const Wrapper = styled.div`
	width: 100%;
	display: flex;
	flex-flow: row wrap;
	justify-content: space-between;

	/* Layout styles override */

	${props => props.wrapper}

	/* Mobile Override */

	${media.phone`
		flex-direction: column;
	
		overflow-y: scroll;
		width: 100vw;
	`}
`

// Caption

const Caption = styled.div`
	font-size: 16px;
	line-height: 24px;
	margin-top: 40px;
`

// Scroll Wrapper

const Spacer = styled.div`
	width: 17px;
`

const ScrollWrapper = styled.div`
	height: 240px;
	display: flex;
	padding-left: 17px;
	overflow-y: hidden;

	> * {
		margin: 0;
		padding: 0;

		&:not(:last-child) {
			padding-right: 8px;
		}

		.media {
			padding-bottom: 0;
			height: 240px;
			width: auto;	
		}
	}
`

const layouts = {

	// Montage
	
	a: [
		css`
			flex: 0 1 53.93%;
			margin-right: 86px;

			.media {
				padding-bottom: 41.5%;
			}
		`,
		css`
			flex: 0 1 27%;
			margin-right: auto;
		
			.media {
				padding-bottom: 100%;
			}
		`,
		css`
			flex: 0 1 36%;
			margin-left: 72px;
			margin-top: 2.2%;

			.media {
				padding-bottom: 125%;
			}
		`,
		css`
			flex: 0 1 22.4%;
			margin-top: 13.5%;

			.media {
				padding-bottom: 84%; 
			}
		`,
		css`
			flex: 0 1 22.4%;
			margin-top: 7%;

			.media {
				padding-bottom: 140%;
			}
		`,
	],

	// Step & repeat four left
	
	b: [
		css`
			flex: 0 1 50%;
			margin-right: 10.71%;

			.media {
				padding-bottom: 71.4%;
			}
		`,
		css`
			flex: 0 1 28.6%;
			margin-right: auto;
			margin-top: 17.85%;
		
			.media {
				padding-bottom: 125%;
			}
		`,
		css`
			flex: 0 1 28.6%;
			margin-left: 10.17%;
			margin-top: -7.5%;

			.media {
				padding-bottom: 125%;
			}
		`,
		css`
			flex: 0 1 50%;
			margin-top: 10.71%;

			.media {
				padding-bottom: 71.4%;
			}
		`
	],

	// Diagonal asymmetrical five right

	c: [
		css`
			flex: 0 1 22.26%;
			margin-left: 41.5%;

			.media {
				padding-bottom: 125%;
			}
		`,
		css`
			flex: 0 1 27.81%;
			margin-left: auto;
			margin-top: 5.54%;
		
			.media {
				padding-bottom: 80%;
			}
		`,
		css`
			flex: 0 1 22.26%;
			margin-left: 5.54%;

			.media {
				padding-bottom: 125%;
			}
		`,
		css`
			flex: 0 1 27.81%;
			margin-top: 10.7%;
			margin-right: 36.8%;

			.media {
				padding-bottom: 80%;
			}
		`,
		css`
			flex: 0 1 27.81%;
			margin-top: 5.54%;

			.media {
				padding-bottom: 80%;
			}
		`
	],

	// Diagonal asymmetrical five left

	d: [
			
		css`
			flex: 0 1 27.81%;			
			margin-top: 5.56%;
		
			.media {
				padding-bottom: 80%;
			}
		`,
		css`
			flex: 0 1 22.26%;
			margin-right: 41.48%;
			margin-left: 8.36%;

			.media {
				padding-bottom: 125%;
			}
		`,
		css`
			flex: 0 1 27.81%;
			margin-left: 36.12%;
			margin-top: 9.68%;
		
			.media {
				padding-bottom: 80%;
			}
		`,
		css`
			flex: 0 1 22.26%;
			margin-right: 5.57%;

			.media {
				padding-bottom: 125%;
			}
		`,
		css`
			flex: 0 1 27.81%;
			margin-left: auto;
			margin-top: 5.7%;
		
			.media {
				padding-bottom: 80%;
			}
		`,
	],
	
	// Asymmetrical five right

	e: [
			
		css`
			flex: 0 1 27.78%;
			margin-left: 36.11%;
		
			.media {
				padding-bottom: 80%;
			}
		`,
		css`
			flex: 0 1 27.78%;
			margin-left: auto;

			.media {
				padding-bottom: 140%;
			}
		`,
		css`
			flex: 0 1 27.78%;
			margin-top: -8.52%;
		
			.media {
				padding-bottom: 80%;
			}
		`,
		css`
			flex: 0 1 27.78%;
			margin-left: 8.33%;
			margin-top: -8.52%;
			margin-right: 36.11%;

			.media {
				padding-bottom: 140%;
			}
		`,
		css`
			flex: 0 1 27.78%;
			margin-top: -8.52%;
		
			.media {
				padding-bottom: 140%;
			}
		`,
	],

	// Asymmetrical five left

	f: [
		css`
			flex: 0 1 27.78%;

			.media {
				padding-bottom: 140%;
			}
		`,
		css`
			flex: 0 1 27.78%;
			margin-left: 8.33%;
			margin-right: 36.11%;
		
			.media {
				padding-bottom: 80%;
			}
		`,
		css`
			flex: 0 1 27.78%;
			margin-left: 36.11%;
			margin-top: -8.52%;

			.media {
				padding-bottom: 140%;
			}
		`,
		css`
			flex: 0 1 27.78%;
			margin-left: auto;
			margin-top: -8.52%;
		
			.media {
				padding-bottom: 80%;
			}
		`,	
		css`
			flex: 0 1 27.78%;
			margin-left: auto;
			margin-top: -8.52%;
		
			.media {
				padding-bottom: 140%;
			}
		`,
	],

	// Step & repeat five left

	g: [
		css`
			flex: 0 1 28%;
			margin-right: 72%;

			.media {
				padding-bottom: 71.43%;
			}
		`,
		css`
			flex: 0 1 16%;
			margin-left: 12%;
			margin-top: 6.3%;
		
			.media {
				padding-bottom: 125%;
			}
		`,
		css`
			flex: 0 1 32%;
			margin-left: 6%;
			margin-top: -4.2%;

			.media {
				padding-bottom: 125%;
			}
		`,
		css`
			flex: 0 1 16%;
			margin-left: 6%;
			margin-top: 6.3%;
			margin-right: 12%;
		
			.media {
				padding-bottom: 125%;
			}
		`,		
		css`
			flex: 0 1 27.78%;
			margin-left: auto;
			margin-top: -4.2%;
		
			.media {
				padding-bottom: 71.43%;
			}
		`,
	],

	// Step & repeat five right

	h: [
		css`
			flex: 0 1 28%;
			margin-left: 72%;

			.media {
				padding-bottom: 71.43%;
			}
		`,
		css`
			flex: 0 1 16%;
			margin-left: 12%;
			margin-top: 6.3%;
		
			.media {
				padding-bottom: 125%;
			}
		`,
		css`
			flex: 0 1 32%;
			margin-left: 6%;
			margin-top: -4.2%;

			.media {
				padding-bottom: 125%;
			}
		`,
		css`
			flex: 0 1 16%;
			margin-left: 6%;
			margin-top: 6.3%;
			margin-right: 12%;
		
			.media {
				padding-bottom: 125%;
			}
		`,		
		css`
			flex: 0 1 27.78%;
			margin-top: -4.2%;
		
			.media {
				padding-bottom: 71.43%;
			}
		`,
	],

	// Montage thhree

	i: [
		css`
			flex: 0 1 48.48%;
			margin-top: 8.8%;

			.media {
				padding-bottom: 125%;
			}
		`,
		css`
			flex: 0 1 24.24%;
			margin-left: 9.09%;
			margin-right: 18.18%;
		
			.media {
				padding-bottom: 125%;
			}
		`,
		css`
			flex: 0 1 42.42%;
			margin-left: auto;
			margin-top: -30%;

			.media {
				padding-bottom: 100%;
			}
		`
	],

	// Centre align three

	j: [
		css`
			flex: 0 1 18.18%;
			margin-top: 11.3%;

			.media {
				padding-bottom: 125%;
			}
		`,
		css`
			flex: 0 1 36.36%;
			margin-top: 0%;
			margin-left: 6.82%;
			margin-right: 6.82%;
		
			.media {
				padding-bottom: 125%;
			}
		`,
		css`
			flex: 0 1 31.82%;
			margin-top: 6.9%;

			.media {
				padding-bottom: 100%;
			}
		`
	]
}

BlockMediaComp.wrapper = css`
	.block-container {
		max-width: 1380px;
	}
`

export default BlockMediaComp
