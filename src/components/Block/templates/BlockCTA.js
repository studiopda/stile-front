import React from 'react'
import styled, { css } from 'styled-components'

import { media } from '../../../styles/utils'
import { button } from '../../../styles/global'
import { heading1, body } from '../../../styles/type'
import { darkGrey,  green } from '../../../styles/colors'

import { getLinkProps } from '../../../utils'
import { getGlobalData } from '../../../../data/global'

const BlockCTA = (props) => {
    const defaultData = getGlobalData('ctaBlock');

    const data = {
        ...defaultData,
        ...props,
    }
    
	return (
		<Wrapper>
            <Heading>{data.heading}</Heading>

			<Description
				dangerouslySetInnerHTML={{__html: data.text }}  
			/>

            <Button
                {...getLinkProps(data.button)}
            >
                {data.button.text}
            </Button>
		</Wrapper>
	)
}

// Shared

const Heading = styled.div``
const Description = styled.div``
const Button = styled.div``

// Layout

const Wrapper = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    text-align: center;
    width: 100%;
    background: ${green};
    padding: 86px 0 80px;

    ${media.phone`
        padding: 40px 0;
    `}

    ${Heading} {
        ${heading1}
    }

    ${Description} {
        ${body}
        color: ${darkGrey};
        max-width: 671px;
        text-align: center;
        margin: 17px 0 24px;
    }

    ${Button} {
        ${button}
        background: ${darkGrey};
        color: white;
    }
`

BlockCTA.wrapper = css`

    /* Max-width override */

    .block-container {
        max-width: 1380px;
    }
`

export default BlockCTA
