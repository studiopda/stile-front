import React from 'react'
import styled, { css } from 'styled-components'

import { media } from '../../../styles/utils'
import { container, bgIcon, padding, hoverState, bgImage } from '../../../styles/global'

const BlockEventbrite = (props) => {
	const { paragraph } = props;
        
	return (
		<Wrapper>
			<Description
				dangerouslySetInnerHTML={{__html: paragraph}}  
			/>
		</Wrapper>
	)
}

// Shared

const Heading = styled.div``
const Subheading = styled.div``
const Description = styled.div``

// Layout

const Wrapper = styled.div``

BlockEventbrite.wrapper = css`

`

export default BlockEventbrite
