import React from 'react'
import styled, { css } from 'styled-components'
import { motion } from 'framer-motion'

import { media } from '../../../styles/utils'
import { container, bgIcon, padding, hoverState, bgImage, innerContainer } from '../../../styles/global'
import { heading1 } from '../../../styles/type'

const BlockHeading = (props) => {
	const { text, collapsed, collapsedText, textAlignment } = props;
        
	return (
		<Wrapper
			align={textAlignment}
		>
			{/* Collapsed animation for home */}

			{collapsedText && (
				<Heading
					isCollapsed={true}
					dangerouslySetInnerHTML={{__html: collapsedText}}
					variants={animatedHeading(true)}		 
				/>
			)}

			<Heading
				key={'heading'}
				dangerouslySetInnerHTML={{__html: text}}
				variants={collapsedText && animatedHeading(false)}		 
			/>
		</Wrapper>
	)
}

// Layout

const Wrapper = styled.div`
	${innerContainer}
	position: relative;

	${props => {
		if (props.align) return css`
			text-align: ${props.align};
		`
	}}

	${media.phone`
		text-align: left;
	`}
`

// Heading

const animatedHeading = (collapsed) => {
	return {
		hidden: {
			opacity: collapsed ? 1 : 0,
			transition: {
				duration: 1,
				easing: 'easeOut'
			}
		},
		visible: {
			opacity: collapsed ? 0 : 1,
			transition: {
				duration: 1,
				easing: 'easeOut'
			}
		},
	}
}

const Heading = styled(motion.div)`
    ${heading1}
	transform: translate3d(0,0,0);

	${props => {
		if (props.isCollapsed) return css`
			position: absolute;
		`
	}}
`

BlockHeading.wrapper = css`

`

export default BlockHeading
