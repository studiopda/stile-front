import React, { useState, useEffect } from 'react'
import styled, { css } from 'styled-components'
import ProgressiveImage from 'react-progressive-image'
import Player from 'react-player'

import { VideoModal } from '../../'

import { media } from '../../../styles/utils'
import { container, bgIcon, padding, hoverState, bgImage, innerContainer } from '../../../styles/global'

const BlockMedia = (props) => {
	const { mediaType, caption, alt, compStyles, styles, modal, containImage, imageScaling = true } = props;
	const [modalActive, setModalActive] = useState(props.modalActive)

	const toggleModal = () => {
		setModalActive(!modalActive)
		props.onModalChange && props.onModalChange(!props.modalActive)
	}

	useEffect(() => {
		setModalActive(props.modalActive)
	}, [props.modalActive])
	
	const renderImage = () => {
		const { image } = props; 
		if (!image) return

		return (
			<Image
				key={image}
				src={image}
			>
				{(src, loading) => {
					return (
						<BGImage 
							className={'media'}
							as={imageScaling ? 'div' : 'img'}
							src={src} 
							style={{opacity: loading ? 0 : 1}}  
							title={alt}
						/>
					)
				}}
			</Image> 
		)
	}
	
	const renderVideo = () => {
		const { video, autoplay, loop } = props;
		if (!video) return;

		return (
			<Video
				className={'media video'}
				url={video}
				playing={autoplay}
				playsinline={autoplay}
				width={'100%'}
				height={'100%'}
				inComp={compStyles}
				title={alt}
				loop={loop}
			/>
		)
	}
	
	const renderVideoModal = () => {
		const { coverImage, buttonText } = props;

		return (
			<>
				{coverImage && (
					<Cover
						className={'video-cover'}
						src={coverImage}
						onClick={toggleModal}
					>
						<Button>
							<PlayIcon/>
							
							{buttonText && (
								<Subheading>
									{buttonText}
								</Subheading>
							)}
						</Button>
					</Cover>
				)}

				<VideoModal
					renderVideo={renderVideo}
					active={modalActive}
					toggle={toggleModal}
				/>
			</>
		)
	}

	return (
		<Wrapper
			mediaType={mediaType}
			styles={compStyles || styles}
			imageScaling={imageScaling}
			contain={containImage}
		>
			{mediaType == 'image' && renderImage()}
			{mediaType == 'video' && !modal && renderVideo()}
			{mediaType == 'video' && modal && renderVideoModal()}
		</Wrapper>
	)
}

// Shared

const Heading = styled.div``
const Subheading = styled.div``

const Image = styled(ProgressiveImage)`
	overflow: hidden;
`

const BGImage = styled.div`
	transition: opacity 0.45s ease;
	background-image: url(${props => props.src});  
	${bgImage};

	${props => {
		if (props.contain) return css`
		
		`
	}}
`

// Video 

const Video = styled(Player)`
	position: relative;
			
	video {
		position: absolute;
		object-fit: cover;
	}
`

// Layout

const Wrapper = styled.div`
	width: 100%;

	.media {
		width: 100%;
		overflow: hidden;
	}

	/* Composition Styles */

	${props => props.styles}

	/* Single Block Styles */

	${props => {
		if (!props.styles) {
			if (props.mediaType == 'video') return css`
				height: 392px;
				${innerContainer}

				.media {
					height: 100% !important;
				}
			`

			if (props.mediaType == 'image' && props.imageScaling) return css`
				.media {
					height: 600px;
					${bgImage};
				}
			`
		}
	}}

	/* Disable scaling */

	${props => {
		if (props.mediaType == 'image' && props.imageScaling == false) return css`
			.media {
				background-image: none;
			}
		`
	}}

	/* Contain image */

	${props => {
		if (props.mediaType == 'image' && props.contain == true) return css`
			.media {
				${bgIcon};
			}
		`
	}}


	${media.phone`
		&, .media {
			height: 240px;
		}

		${props => {
			if (props.mediaType == 'video') return css`
				video {
					width: auto !important;
					position: relative;
				}
			`
		}}
	`}
`

// Video Modal

const PlayIcon = styled.div`
	background-image: url(${require('../../../assets/images/play-button.svg')});  
	${bgIcon};
	height: 20px;
	width: 17px;
`

const Button = styled.div`
	display: flex;
	align-items: center;
	background: white;
	opacity: 0.9;
	border-radius: 193px;
	height: 48px;
	padding: 0 24px;
	width: auto;

	position: absolute;
	top: 50%;
	left: 50%;
	transform: translate(-50%, -50%);

	${media.phone`
		width: fit-content;
	`}

	${Subheading} {
		margin-left: 12px;
		font-weight: 500;
	}
`

const Cover = styled(BGImage)`
	height: 100%;
	width: 100%;
	background-color: rgba(0,0,0, 0.2);
	box-shadow: 1px -1px 5px rgba(0, 0, 0, 0.15);
	border-radius: 4px;
	overflow: hidden;
	position: relative;
	cursor: pointer;
	
	&:hover {
		opacity: 0.9;
	}
`

BlockMedia.wrapper = css`

`

export default BlockMedia
