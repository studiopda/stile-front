import React from 'react'
import styled, { css } from 'styled-components'

import { media } from '../../../styles/utils'
import { button } from '../../../styles/global'
import { heading1, heading4, body } from '../../../styles/type'
import { darkGrey, green, beige } from '../../../styles/colors'
import { getLinkProps } from '../../../utils'

const BlockPageLinks = (props) => {
	const { subHeading, heading, text, button } = props
        
	return (
		<Wrapper>
            <Subheading>{subHeading}</Subheading>
            <Heading>{heading}</Heading>

            <Description
                dangerouslySetInnerHTML={{__html: text }}  
            />

            <Button
                {...getLinkProps(button)}
            >
                {button.text}
            </Button>
		</Wrapper>
	)
}

// Shared

const Heading = styled.div``
const Subheading = styled.div``
const Description = styled.div``
const Button = styled.div``

// Layout

const Wrapper = styled.div`
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    background: ${beige};
    width: 100%;
    padding: 80px;
    border-radius: 16px;

    ${media.phone`
        padding: 32px 24px;
    `}

    ${Subheading} {
        ${heading4}
        margin-bottom: 5px;
    }

    ${Heading} {
        ${heading1}

        ${media.phone`
            margin-bottom: 12px;
        `}
    }

    ${Description} {
        ${body}
        color: ${darkGrey};
        max-width: 525px;
        margin-bottom: 16px;
    }

    ${Button} {
        margin-top: 32px;
        ${button}
    }

`

BlockPageLinks.wrapper = css`
    /* Max-width override */

    .block-container {
        max-width: 1380px;
    }
`

export default BlockPageLinks
