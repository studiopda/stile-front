import React from 'react'
import styled, { css } from 'styled-components'

import { media } from '../../../styles/utils'
import { container, bgIcon, padding, hoverState, bgImage } from '../../../styles/global'
import { grey, greyText } from '../../../styles/colors'
import { smallBody } from '../../../styles/type'

const BlockTextGrid = (props) => {
	const { items, alignment, itemAlignment } = props;

	const renderItems = () => {
		if (!items) return

		return items.map((item, i) => {
			return (
				<Item
					alignment={itemAlignment}
				>
					{item.icon && (
						<Icon
							icon={item.icon}
						/>
					)}
					
					<Heading
						dangerouslySetInnerHTML={{__html: item.heading}}
					/>	
					<Description
						dangerouslySetInnerHTML={{__html: item.text}}
					/>	
				</Item>
			)
		})
	}
	
        
	return (
		<Wrapper
			alignment={alignment}
		>
			{renderItems()}
		</Wrapper>
	)
}

// Shared

const Heading = styled.div``
const Subheading = styled.div``
const Description = styled.div``

// Layout

const Wrapper = styled.div`
	display: flex;
	flex-flow: row wrap;
	width: 100%;

	${props => {
		if (props.alignment == 'center') return css`
			justify-content: center;
		`
	}}
`

// Item

const Icon = styled.div`
	width: 48px;
	height: 48px;
	${bgIcon}
	margin-right: 6px;
	background-image: url(${props => props.icon});
	margin-bottom: 16px;
`

const Item = styled.div`
	flex: 0 1 25%;
	padding-right: 24px;
	margin-bottom: 64px;

	${media.tablet`
		flex: 0 1 50%;
	`}

	${media.phone`
		flex: 1 0 100%;
		padding-right: 0;
		margin-bottom: 40px;
	`}

	${Heading} {
		display: flex;
		font-weight: 500;
		line-height: 24px;
		color: ${grey};
	}

	${Description} {
		margin-top: 16px;
		${smallBody};
		color: ${greyText};
	}

	${props => {
		if (props.alignment == 'center') return css`
			display: flex;
			flex-direction: column;
			text-align: center;
			align-items: center;
		`
	}}
`

BlockTextGrid.wrapper = css`

`

export default BlockTextGrid
