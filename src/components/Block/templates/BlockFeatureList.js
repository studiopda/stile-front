import React from 'react'
import styled, { css } from 'styled-components'

import { media } from '../../../styles/utils'
import { container, bgIcon, padding, hoverState, bgImage } from '../../../styles/global'
import { green2, greyText } from '../../../styles/colors'
import { heading3, smallBody } from '../../../styles/type'

const BlockFeatureList = (props) => {
	const { heading, features, useTicks } = props;

	const renderItems = () => {
		if (!features) return

		const items = features.map((item, i) => {
			const useIcon = item.icon || useTicks;
			
			return (
				<Feature 
					key={i}
				>
					<Subheading>
						{useIcon && (
							<Icon
								icon={item.icon}
							/>
						)}

						{item.heading}
					</Subheading>

					{item.text && (
						<Description
							dangerouslySetInnerHTML={{__html: item.text}}
						/>
					)}
				</Feature>
			)
		})

		return <Features>{items}</Features>
	}
	
	return (
		<Wrapper>
			<Heading
				as={'h3'}
				dangerouslySetInnerHTML={{__html: heading}}
			/>
			{renderItems()}
		</Wrapper>
	)
}

// Shared

const Heading = styled.div``
const Subheading = styled.div``
const Description = styled.div``

// Layout

const Wrapper = styled.div`
	width: 100%;
	
	${Heading} {
		${heading3};
		color: ${green2};
	}
`

// Features

const Features = styled.div`
	display: flex;
	flex-flow: row wrap;
	margin-top: 32px;
`

// Feature

const Icon = styled.div`
	width: 24px;
	height: 24px;
	${bgIcon}
	background-image: url(${require('../../../assets/images/feature-tick.svg')});
	margin-right: 6px;

	${props => {
		if (props.icon) return css`
			background-image: url(${props.icon});
		`
	}}
`

const Feature = styled.div`
	flex: 0 1 25%;
	margin-bottom: 40px;
	padding-right: 25px;

	${media.tablet`
		flex: 1 0 50%;
	`}

	${media.phone`
		flex: 1 0 100%;
		padding-right: 0;
		margin-bottom: 40px;
	`}

	${Subheading} {
		display: inline;
		align-items: center;
		font-weight: 500;
		line-height: 24px;

		${Icon} {
			display: inline-block;
			min-width: 24px;
			transform: translateY(6px);
		}

		> * {
			display: inline;
		}
	}

	${Description} {
		margin-top: 16px;
		${smallBody};
		color: ${greyText};
	}
`

BlockFeatureList.wrapper = css`
	margin-bottom: 60px;
`

export default BlockFeatureList
