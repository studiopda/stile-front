import React from 'react'
import styled, { css } from 'styled-components'
import ReactMarkdown from 'react-markdown/with-html'
import stripIndent from 'common-tags/lib/stripIndent'

import { media } from '../../../styles/utils'
import { container, bgIcon, padding, hoverState, bgImage, innerContainer } from '../../../styles/global'
import { grey } from '../../../styles/colors'
import { body, heading1, heading2, heading3, heading4 } from '../../../styles/type'

const BlockMarkdown = (props) => {
    const { markdown } = props
        
	return (
		<Wrapper>
            <Content>
                <ReactMarkdown
                    source={stripIndent(markdown)}
                    escapeHtml={false}
                />
            </Content>
		</Wrapper>
	)
}

const Wrapper = styled.div`
	${innerContainer}
`

const Content = styled.div`
    display: flex;
    flex-direction: column;

    &, * {
        font-size: 16px;
        line-height: 24px;
        margin-bottom: 0;
	}

    h1 {
        ${heading1};
        margin-top: 48px;
    }

    h2 {
        ${heading2};
    }

    h3 {
        ${heading3};
    }

    h4 {
        ${heading4};
        margin-top: 40px;
        margin-bottom: 16px;
    }

    p {
        margin-top: 24px;
    }
    
    > *:first-child {
        margin-top: 0;
    }
`

BlockMarkdown.wrapper = css`

`

export default BlockMarkdown
