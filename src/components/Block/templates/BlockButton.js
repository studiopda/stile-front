import React from 'react'
import styled, { css } from 'styled-components'

import { media } from '../../../styles/utils'
import { container, padding, button } from '../../../styles/global'
import { getLinkProps } from '../../../utils'

const BlockButton = (props) => {
	const { button } = props;
        
	return (
		<Wrapper>
			{button && (
				<Button
					{...getLinkProps(button)}
				>
					{button.text}
				</Button>
			)}
		</Wrapper>
	)
}

// Layout

const Button = styled.div`
	${button}
	cursor: pointer;
`

const Wrapper = styled.div`
	display: flex;
	justify-content: flex-start;
	width: 100%;
`

BlockButton.wrapper = css``

export default BlockButton
