import React, { useState, useEffect } from 'react'
import styled, { css } from 'styled-components'
import { useInterval } from 'react-use';

import { Slider } from '../../'

import { media } from '../../../styles/utils'
import { container, bgIcon, padding, hoverState, bgImage } from '../../../styles/global'
import { heading1, heading4} from '../../../styles/type'
import { darkGrey, black } from '../../../styles/colors'

const BlockQuote = (props) => {
    const { slides, heading } = props
    const [activeSlide, setActiveSlide] = useState(0)
    const [resetProgress, setResetProgress] = useState(false)

    const updateSlide = (index) => {
        if (index == slides.length) {
            setActiveSlide(0)
            setResetProgress(true)
        } else {
            if (index > 0) setResetProgress(false)
            setActiveSlide(index)
        }
    }
        
    const renderSlides = (slides) => {
        return slides.map((item, i) => {
            const { text, credit } = item

            return (
                <Slide>
                    <Description
                        dangerouslySetInnerHTML={{__html: `“${text}”`}}  
                    />

                    <Credit
                        dangerouslySetInnerHTML={{__html: credit}}  
                    /> 
                </Slide>
            )
        })
    }

    const renderProgress = () => {
        const items = slides.map((item, i) => {
            const [progress, setProgress] = React.useState(0);
            const isActive = activeSlide == i;
            
            useInterval(
				() => { 
					progress == 110 ? updateSlide(i + 1) : setProgress(progress + 1)
				}, 
				isActive ? 50 : null
            )
            
            useEffect(() => {
				if (resetProgress) setProgress(0)
			}, [resetProgress])

            return (
                <Progress
                    progress={progress}
                />
            )
        })

        return (
            <ProgressBars>{items}</ProgressBars>
        )
    }
    
    return (
        <Wrapper>
            <Heading>{heading}</Heading>

            <Slider
                activeSlide={(activeSlide + 1)}
                renderSlides={renderSlides}
                slides={slides}
                sliderStyles={sliderStyles}
                sliderParams={{
                    effect: 'fade',
                    speed: 1000,
                    autoHeight: true,
                    autoplay: {
                        delay: 5000,
                    },
                }}
            />

            {renderProgress()}
        </Wrapper>       
    )
}


// Shared

const Heading = styled.div``
const Description = styled.div``
const Credit = styled.div``

// Layout


const Wrapper = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    ${padding}
    text-align: center;

    ${Heading} {
        ${heading1}
        max-width: 700px;
    }
`

const Slide = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    width: 100%;
    text-align: center;
    background: white;

    ${Description},
    ${Credit} {
        max-width: 640px;
        color: ${darkGrey};
    }

    ${Description} {
        font-size: 32px;
        line-height: 48px;
        text-align: center;
        margin: 48px 0 40px;

        ${media.phone`
            font-size: 24px;
            line-height: 32px;
            margin: 32px 0 24px;
        `}
    }

    ${Credit} {
        font-size: 16px;
        line-height: 24px;
        font-weight: 500;
        text-align: center;
    }
`

// Progress Bar

const ProgressBars = styled.div`
    display: flex;
    margin-top: 40px;
`

const Progress = styled.div`
	height: 4px;
	min-width: 58px;
	background-color: #CDCDCD;
	margin-right: 8px;
	position: relative;
	overflow: hidden;

	${props => {
		if (props.progress) return css`
			&::before {
				content: '';
				position: absolute;
				background-color: #393939;
				top: 0;
				bottom: 0;
				right: 0;
				left: 0;
				transition: all 0.5s linear;
				transform: scaleX(${props => props.progress / 100});
				transform-origin: left center;
			}
		`
	}}
`

// Slider

const sliderStyles = css`
    width: calc(100vw - 100px);

    ${media.phone`
        width: calc(100vw - 34px);
    `}
`

BlockQuote.wrapper = css`

`

export default BlockQuote
