import React from 'react'
import styled, { css } from 'styled-components'

import { media } from '../../../styles/utils'
import { innerContainer, padding } from '../../../styles/global'

const BlockSignature = (props) => {
	const { image } = props;
        
	return (
		<Wrapper>
			<Container>
				<Image
					src={image}
				/>
			</Container>
		</Wrapper>
	)
}

// Layout

const Image = styled.img`
	background-image: url(${props => props.src});  
	max-height: 120px;
	width: auto;
	height: 100%;
`

const Container = styled.div`
	${innerContainer}
`

const Wrapper = styled.div`
	display: flex;
	justify-content: flex-start;
	width: 100%;
	${padding}
	justify-content: center;
`

BlockSignature.wrapper = css`
	margin-bottom: 48px !important;
`

export default BlockSignature
