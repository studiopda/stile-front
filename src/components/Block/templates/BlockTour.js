import React, { useState, useEffect } from 'react'
import styled, { css } from 'styled-components'
import { useInterval } from 'react-use';

import { Slider } from '../../'

import { media } from '../../../styles/utils'
import { container, bgIcon, padding, hoverState, bgImage } from '../../../styles/global'
import { darkGrey } from '../../../styles/colors'
import { heading1, body } from '../../../styles/type'

const BlockTour = (props) => {
	const { heading, sliders } = props;
	
	const renderSlides = (slides) => {
		return slides.map((item, i) => {
			if (!item.image) return;
			
			return (
				<Slide>
					<AddressBar
						src={require('../../../assets/images/tour-address-bar.svg')}
					/>
					<BGImage 
						image={item.image} 
					/>
				</Slide>
			)
		})
	}

	const renderSlider = (item) => {
		const [activeSlide, setActiveSlide] = useState(0)
		const [resetProgress, setResetProgress] = useState(false)

		const updateSlide = (index) => {
			if (index == item.slides.length) {
				setActiveSlide(0)
				setResetProgress(true)
			} else {
				if (index > 0) setResetProgress(false)
				setActiveSlide(index)
			}
		}
		
		const sliderText = item.slides.map((slide, i) => {
			const [progress, setProgress] = React.useState(0);
			const isActive = activeSlide == i;
			
			useInterval(
				() => { 
					progress == 110 ? updateSlide(i + 1) : setProgress(progress + 1)
				}, 
				isActive ? 50 : null
			);

			useEffect(() => {
				if (resetProgress) setProgress(0)
			}, [resetProgress])
			
			return (
				<SlideText 
					key={i}
				>
					<Progress
						progress={progress}
					/>
					<Description
						dangerouslySetInnerHTML={{__html: slide.text}}
					/>	
				</SlideText>
			)
		})

		return (
			<Tour>
				<SliderWrapper>
					{/* <AddressBar/> */}
					<Slider
						activeSlide={(activeSlide + 1)}
						slides={item.slides}
						renderSlides={renderSlides}
						sliderParams={{
							effect: 'fade',
							speed: 750,
							noSwiping: true
						}}
					/>
				</SliderWrapper>
				
				<Info>
					<Heading>{item.heading}</Heading>
					{sliderText}
				</Info>
			</Tour>
		)
	}

	const renderTours = () => {
		if (!sliders) return
		 
		return sliders.map((item, i) => {
			if (item.slides) return renderSlider(item)
		})
	}

	return (
		<Wrapper>
			<Container>
				<Heading>
					{heading}
				</Heading>

				{renderTours()}
			</Container>
		</Wrapper>
	)
}

// Shared

const Heading = styled.div``
const Subheading = styled.div``
const Description = styled.div``
const Info = styled.div``

const BGImage = styled.div`
	background-image: url(${props => props.image});  
	${bgImage};
`


// Layout

const Wrapper = styled.div`
	display: flex;
	justify-content: center;
	width: 100%;
	background: ${darkGrey};
`

const Container = styled.div`
	display: flex;
	flex-direction: column;
	padding: 0 92px;

	${media.tablet`
		${padding}
		width: 100%;
	`}

	> ${Heading} {
		margin: 128px 0;
		${heading1};
		font-size: 64px;
		color: white;
		text-align: center;

		${media.tablet`
			margin: 40px 0;
		`}
	}

	${Heading},
	${Description} {
		color: white;
	}
`

// Tour

const Tour = styled.div`
	display: flex;
	margin-bottom: 160px;

	${media.tablet`
		flex-direction: column;
		margin-bottom: 80px;
	`}

	${Info} {
		margin-left: 115px;
		max-width: 435px;

		${media.tablet`
			margin-top: 40px;
			margin-left: 0;
			max-width: auto;
		`}

		${Heading} {
			${heading1};
			margin-bottom: 40px;
		}
	}

	&:nth-child(odd) {
		${Info} {
			order: 0;
			margin-left: 0;
			margin-right: 115px;

			${media.tablet`
				margin-right: 0;
			`}
		}
		
		> *:first-child {
			order: 2;

			${media.tablet`
				order: 0;
			`}
		}
	}
`

const Progress = styled.div`
	width: 4px;
	min-width: 4px;
	background-color: rgba(255,255,255, 0.5);
	margin-right: 12px;
	position: relative;
	overflow: hidden;

	${props => {
		if (props.progress) return css`
			&::before {
				content: '';
				position: absolute;
				background-color: white;
				top: 0;
				bottom: 0;
				right: 0;
				left: 0;
				transition: all 0.5s linear;
				transform: scaleY(${props => props.progress / 100});
				transform-origin: top center;
			}
		`
	}}
`

const SlideText = styled.div`
	position: relative;
	display: flex;

	${Description} {
		${body}
	}

	&:not(:last-child) {
		margin-bottom: 32px;

		${media.tablet`
			margin-bottom: 24px;	
		`}
	}
`

// Slider

const SliderWrapper = styled.div`
	position: relative;
`

const AddressBar = styled.img`
	width: 100%;
	height: auto;
`
// Slide

const Slide = styled.div`
	display: flex;
	flex-direction: column;

	${BGImage} {
		height: 100%;
		padding-bottom: 80%;
		flex: 1;
	}
`



BlockTour.wrapper = css`

`

export default BlockTour
