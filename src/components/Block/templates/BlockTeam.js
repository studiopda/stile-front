import React, { useState } from 'react'
import styled, { css } from 'styled-components'
import { filter, orderBy, forEach, find } from 'lodash'
import ProgressiveImage from 'react-progressive-image'

import { media } from '../../../styles/utils'
import { container, bgIcon, padding, hoverState, bgImage } from '../../../styles/global'
import { heading1, body } from '../../../styles/type'

import teamData from '../../../../data/team'
import { useMount } from 'react-use'
import { green } from '../../../styles/colors'

const BlockTeam = (props) => {
	const { showAll, category: teamCategory } = props;
	const [category, setCategory] = useState(teamCategory || 'all')

	const filterPeople = () => {
		let people = []

		if (category == 'all') {
			people = teamData.members
		} 
		
		if (category !== 'all') {
			people = filter(teamData.members, {categories: [category]});
		}

		if (props.people) {
			return people = props.people.map(name => {
				return find(teamData.members, {name: name})
			})
		}

		return orderBy(people, 'name')
	}
	
	const renderInfo = (params) => {
		const { heading, text } = props;

		return (
			<Info>
				<Heading>{heading}</Heading>
				
				{text && (
					<Description
						dangerouslySetInnerHTML={{__html: text}}
					/>
				)}

				{showAll && renderNav()}
			</Info>
		)
	}

	const renderNav = () => {
		const items = teamData.categories.map((team, i) => {
			if (team.showInAll == false) return;

			return (
				<NavItem 
					key={i}
					active={category == team.slug}
					onClick={() => setCategory(team.slug)}
				>
					{team.name}
				</NavItem>
			)
		})
		
		return (
			<Nav>
				{items}
			</Nav>
		)
	}

	const renderPerson = (item, i) => {
		const { showContactInfo, showBackground } = props;
		const { contact } = item;

		return (
			<Person
				key={i}
			>
				{item.image && (
					<Image
						key={item.image}
						src={item.image}
					>
						{(src, loading) => {
							return (
								<BGImage 
									image={src} 
									style={{opacity: loading ? 0 : 1}}  
								/>
							)
						}}
					</Image> 
				)}

				<Heading>
					{item.name}
				</Heading>

				<Subheading>
					{showContactInfo ? item.location : item.role}
				</Subheading>

				<Description>
					{showBackground && item.background}
				</Description>

				{showContactInfo && contact && (
					<ContactLink
						href={contact.link}
						target={'_blank'}
					>
						{contact.text}
					</ContactLink>
				)}
			</Person>
		)
	}

	const renderGrid = () => {
		const people = filterPeople()
		
		const items = people.map((item, i) => {
			if (item) return renderPerson(item, i)
		})

		return <Grid>{items}</Grid>
	}
	        
	return (
		<Wrapper>
			{renderInfo()}
			{renderGrid()}
		</Wrapper>
	)
}

// Shared

const Heading = styled.div``
const Subheading = styled.div``
const Description = styled.div``

const Image = styled(ProgressiveImage)`
	overflow: hidden;
`

const BGImage = styled.div`
	transition: opacity 0.45s ease;
	background-image: url(${props => props.image});  
	${bgImage};
`

// Layout

const Wrapper = styled.div`
	display: flex;
	width: 100%;

	${media.tablet`
		flex-direction: column;
	`}
`

// Info

const Info = styled.div`
	display: flex;
	flex-direction: column;
	flex: 0 1 433px;
	margin-right: 24px;
	
	${media.tablet`
		flex: 0 1 auto;
		margin-bottom: 40px;
	`}

	${Heading} {
		${heading1};
		margin-bottom: 24px;
	}

	${Description} {
		${body}
		margin-bottom: 24px;
		margin-right: 20px;
	}
`

// Nav

const NavItem = styled.div`
	${props => {
		if (props.active) return css`
			color: ${green};
			text-decoration: underline;
		`
	}}
`

const Nav = styled.div`
	${NavItem} {
		${body};
		cursor: pointer;

		&:not(:last-child) {
			margin-bottom: 32px;

			${media.tablet`
				margin-bottom: 12px;
			`}
		}
	} 
`

// Grid

const Grid = styled.div`
	display: flex;
	flex-flow: row wrap;
	flex: 1;
`

// Person	

const ContactLink = styled.a``
const Background = styled.div``

const Person = styled.div`
	display: flex;
	flex-direction: column;
	flex-basis: calc(33.3% - 24px);
	margin-bottom: 80px;

	&:not(:nth-child(3n)) {
		margin-right: 24px;

		${media.tablet`
			margin-right: 0;
		`}
	}

	${media.tablet`
		flex-basis: calc(50% - 12px);
		margin-bottom: 40px;

		&:not(:nth-child(even)) {
			margin-right: 24px;
		}
	`}

	${BGImage} {
		width: 100%;
		padding-bottom: 119.5%;
		margin-bottom: 24px;
	}

	${Heading},
	${Subheading} {
		${body}
	}

	${Heading} {
		font-weight: 500;
		margin-bottom: 8px;
	}

	${Description} {
		margin-top: 8px;
		font-size: 16px;
		line-height: 24px;
	}

	${ContactLink} {
		margin-top: 12px;
		color: ${green};
		text-decoration: underline;
		${hoverState};
		cursor: pointer;
	}
`

BlockTeam.wrapper = css`
	.block-container {
		max-width: 1380px;
	}
`

export default BlockTeam
