import { 
    BlockCTA,
    BlockEventbrite,
    BlockFeatureList,
    BlockHeading,
    BlockListColumns,
    BlockMedia,
    BlockMediaComp,
    BlockPageLinks,
    BlockSignature,
    BlockTeam,
    BlockText,
    BlockTextGrid,
    BlockTour,
    BlockQuote,
    BlockMapbox,
    BlockButton,
    BlockJobs,
    BlockMarkdown
} from './templates'

export const resolveBlock = (layout) => {
    let block;

    // Map layout strings to blocks

    const blocks = {
        'text': BlockText,
        'heading': BlockHeading,
        'quote': BlockQuote,
        'media': BlockMedia,
        'media-comp': BlockMediaComp,
        'page-links': BlockPageLinks,
        'list-columns': BlockListColumns,
        'feature-list': BlockFeatureList,
        'text-grid': BlockTextGrid,
        'cta': BlockCTA,
        'team': BlockTeam,
        'tour': BlockTour,
        'signature': BlockSignature,
        'eventbrite': BlockEventbrite,
        'map': BlockMapbox,
        'button': BlockButton,
        'jobs': BlockJobs,
        'markdown': BlockMarkdown,
    }
    
    return blocks[layout]
}

export const resolveContainer = (layout) => {
    const noContainer = ['cta', 'tour'];
    return !noContainer.includes(layout)
}


export const resolveLocalisation = ({locale, localisation, ...props}) => {    
    let newProps = props;

    if (locale !== 'AU' && localisation && localisation[locale]) {
        newProps = {
            ...props,
            ...localisation[locale]
        }
    }

    return newProps
}
