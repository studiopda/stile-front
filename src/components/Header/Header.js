import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import { navigate, Link, useStaticQuery } from 'gatsby'
import { useMount } from 'react-use';
import styled, { css } from 'styled-components'

import { TrialModal } from '../'

import { media } from '../../styles/utils'
import { container, bgImage, padding, hoverState, button } from '../../styles/global'
import { green } from '../../styles/colors';
import { getPagesByFolder } from './utils'

export const navSections = [
	{
		name: 'What is Stile?',
		folder: 'what-is-stile'
	},
	{
		name: 'Professional Development',
		folder: 'professional-development'
	},
	{
		name: 'Who we are',
		folder: 'who-we-are'
	},
]

const Header = (props) => {
	const pages = useStaticQuery(query).allSitePage.nodes
	const sections = getPagesByFolder(navSections, pages);
	const [mobileMenuActive, setMobileMenuActive] = useState(false)
	const [trialModalActive, setTrialModalActive] = useState(false)
	const [blockoutActive, setBlockoutActive] = useState(false)

	const toggleMobileMenu = () => {
		setMobileMenuActive(!mobileMenuActive)
		setBlockoutActive(!blockoutActive)
	}

	const toggleTrialModal = () => {
		setTrialModalActive(!trialModalActive)
	}

	useMount(() => {
		console.log('mount')
	})

	const renderMobileMenu = () => {
		return (
			<MobileMenu
				active={mobileMenuActive}
			>
				{renderFunctions()}
				{renderNavigation()}
			</MobileMenu>
		)		
	}
	
	const renderSubPages = (item, active) => {
		if (!item.pages || !item.pages.length) return;
		
		const items = item.pages.map((item, i) => {
			return (
				<Item 
					key={i}
					as={Link}
					to={item.path}
				>
					{item.title}	
				</Item>
			)
		})

		return (
			<MenuDropdown
				active={active}
			>
				{items}
			</MenuDropdown>
		)
	}
	
	const renderNavigation = () => {
		if (!sections) return;

		const items = sections.map((item, i) => {
			const [active, setActive] = useState(false)

			useEffect(() => {
				setBlockoutActive(active)
			}, [active])

			return (
				<Section 
					key={i}
					as={Link}
					to={item.pages && item.pages[0].path}
					active={active}
					onMouseEnter={() => setActive(true)}
					onMouseLeave={() => setActive(false)}
					onClick={() => mobileMenuActive && toggleMobileMenu()}
				>
					{item.name}
					{renderSubPages(item, active)}
				</Section>
			)
		})
		
		return (
			<Navigation>
				{items}
			</Navigation>
		)
	}

	const renderFunctions = (params) => {
		return (
			<Functions>
				<Login
					href={'https://stileapp.com/login'}
					target={'_blank'}
				>
					Log in
				</Login>

				<Trial
					as={'div'}
					onClick={toggleTrialModal}
				>
					Set up a trial
				</Trial>

				<TrialModal
					active={trialModalActive}
					toggle={toggleTrialModal}
				/>
			</Functions>
		)
	}
	
	return (
		<>
			<Wrapper
				mobileMenuActive={mobileMenuActive}
			>
				<Container>
					<Logo
						onClick={props.onLogoClick}
						as={Link}
						to={'/'}
					/>

					{renderNavigation()}
					{renderFunctions()}

					<MenuToggle
						onClick={toggleMobileMenu}
					>
						{mobileMenuActive ? 'Close' : 'Menu'}
					</MenuToggle>
				</Container>	
			</Wrapper>

			<Blockout
				active={blockoutActive}
			/>

			{renderMobileMenu()}
		</>
	)
}

// Shared

const Heading = styled.div``
const Subheading = styled.div``
const Description = styled.div``
const Item = styled.div``
const Items = styled.div``

const BGImage = styled.div`
	background-image: url(${props => props.image});  
	${bgImage};
`

// Layout

const Wrapper = styled.div`
	display: flex;
	justify-content: center;
	min-height: 72px;	
	background: white;

	position: fixed;
	top: 0;
	right: 0;
	left: 0;
	z-index: 10;


	${media.tablet`
		min-height: 62px;
	`}
`

const Container = styled.div`
	max-width: 1380px;
	width: 100%;
	${padding}

	display: flex;
	align-items: flex-start;

	${media.tablet`
		align-items: center;
	`}
`

// Logo

const Logo = styled(BGImage)`
	background-image: url(${require('../../assets/images/header-logo.svg')});
	width: 62.5px;
	height: 24px;
	margin-top: 24px;

	${media.tablet`
		margin-top: 0;
	`}
`

// Navigation

const Section = styled.div`
	transition: color 0.25s ease;
	cursor: pointer;

	${props => {
		if (props.active) return css`
			color: ${green}
		`
	}}
`

const Navigation = styled.div`
	display: flex;
	margin-left: 28px;
	margin-top: 29px;

	${Section} {
		font-weight: 400;
		height: 100%;
		position: relative;
		user-select: none;

		&:not(:last-child) {
			margin-right: 24px;
		}
	}

	${media.tablet`
		display: none;
	`}
`

// Dropdown Menu

const MenuDropdown = styled.div`
	display: flex;
	flex-direction: column;
	padding-top: 18px;
	padding-bottom: 10px;

	position: absolute;
	background: white;
	width: 600px;

	&::before {
		content: '';
		position: absolute;
		left: -100vw;
		bottom: 0;
		top: 24px;
		width: 200vw;
		background: white;
	}

	${Item} {
		font-size: 16px;
		line-height: 24px;
		${hoverState}
		z-index: 2;
		margin-bottom: 14px;
		user-select: none;
	}

	/* Animation */
	
	opacity: 0;
	pointer-events: none;
	transition: opacity 0.25s ease;

	${props => {
		if (props.active) return css`
			pointer-events: all;
			opacity: 1;
		`
	}}
`

const Blockout = styled.div`
	position: fixed;
	top: 0;
	left: 0;
	right: 0;
	bottom: 0;
	background: rgba(0,0,0, 0.25);
	z-index: 9;

	/* Animation */
	
	opacity: 0;
	pointer-events: none;
	transition: opacity 0.25s ease;

	${props => {
		if (props.active) return css`
			pointer-events: all;
			opacity: 1;
		`
	}}
`

// Functions

const Login = styled.a`
	font-weight: 500;
	margin-right: 32px;
`

const Trial = styled(Link)`
	${button}
	cursor: pointer;
`

const Functions = styled.div`
	margin-left: auto;
	display: flex;
	align-items: center;
	margin-top: 12px;

	${media.tablet`
		display: none;
	`}
`

// Mobile Menu Toggle

const MenuToggle = styled.div`
	${button}
	display: none;
	margin-left: auto;
	min-width: 0;

	${media.tablet`
		display: flex;
	`}
`

// Mobile Menu

const MobileMenu = styled.div`
    position: fixed;
	top: 0;
	left: 0;
	right: 0;
	bottom: 0;
	background: white;
	z-index: 9;
	max-width: 100vw;
	overflow-y: scroll;
	overflow-x: hidden;

	display: flex;
	flex-direction: column;
	${padding}
	padding-top: 68px;

	${Functions} {
		display: flex;
		margin-left: 0;

		${Login} {
			${button}
		}

		${Login},
		${Trial} {
			flex: 1;

			&:not(:last-child) {
				margin-right: 15px;
			}
		}
	}

	${Navigation} {
		display: flex;
		flex-direction: column;
		margin: 0;
		margin-top: 36px;

		${Section} {
			&:not(:last-child) {
				margin-bottom: 16px;
			}

			&:active {
				outline: none;
			}
		}

		${MenuDropdown} {
			opacity: 1;
			position: relative;
			margin-top: 4px;

			&::before {
				box-shadow: none;
			}

			${Item} {
				margin-bottom: 24px;
				-webkit-text-size-adjust: none;
			}
		}
	}


	/* Active */

	transition: opacity 0.5s ease;
	opacity: 1;
	will-change: opacity;

	${props => {
		if (!props.active) return css`
			opacity: 0;
			pointer-events: none;
		`
	}}
`

const query = graphql`
	query headerPages {
		allSitePage {
			nodes {
				path
			  	context {
					section
					name
					relativePath
				}
			}
		}
	}
`

export default Header
