import { forEach, filter, sortBy } from 'lodash'
import { getTemplateData } from '../../utils';

// Map pages to folder

export const getPagesByFolder = (headerSections, gatsbyPages) => {
    let sections = []

    forEach(headerSections, (section) => {
        const pagesBySection = filter(gatsbyPages, (o) => {
            if (o.context && o.context.section == section.folder) return 1;
        })

        let pages = []

        forEach(pagesBySection, (page, i) => {
            const data = getTemplateData(page.context.relativePath);
            pages.push({
                ...page,
                ...data
            })
        })

        sections.push({
            ...section,
            pages: sortBy(pages, 'menuOrder')
        })
    })

    return sections
}