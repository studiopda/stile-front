import React, { useEffect, useState } from 'react'
import styled, { css } from 'styled-components'
import { Link } from 'gatsby';
import OutsideClickHandler from 'react-outside-click-handler';
import usePortal from 'react-useportal'

import { media } from '../../styles/utils'
import { padding } from '../../styles/global'
import { heading2, heading4 } from '../../styles/type'
import { green } from '../../styles/colors';

const optionData = {
	initial: [
		{
			label: `I'm a teacher`,
			changeOptions: 'teacher',
		},
		{
			label: `I'm a student`,
			externalLink: 'https://stileapp.com/login'
		}
	],
	teacher: [
		{
			label: `My school does not use Stile yet`,
			link: '/set-up-trial'
		},
		{
			label: `My school already uses Stile`,
			externalLink: 'https://stileapp.com/login'
		}
	],
}

const TrialModal = (props) => {
	const { active, toggle } = props;
	const { Portal } = usePortal()
	const [options, setOptions] = useState(optionData.initial)

	useEffect(() => {
		setOptions(optionData.initial)
		document.body.style.overflow = props.active == true ? 'hidden' : 'auto';
	}, [active])

	const renderOptions = () => {
		const items = options.map((item, i) => {
			const { externalLink, link, changeOptions } = item;

			return (
				<Item 
					key={i}
					as={externalLink && 'a' || link && Link}
					href={externalLink}
					to={link}
					target={'_blank'}
					onClick={() => { 
						link && props.toggle()
						changeOptions && setOptions(optionData[changeOptions])
					}}
				>
					{item.label}
				</Item>
			)
		})

		return <Items>{items}</Items>
	}

    return (
		<>
			{active && (
				<Portal>
					<Wrapper
						active={active}
					>
						<Close
							onClick={toggle}
						/>
						<Content>
							<Heading>Set up a trial</Heading>
							{renderOptions()}
						</Content>
					</Wrapper>
				</Portal>
			)}
		</>
    )
}

// Shared

const Heading = styled.div``
const Subheading = styled.div``
const Description = styled.div``
const Items = styled.div``
const Item = styled.div``

// Wrapper

const Wrapper = styled.div`
    position: fixed;
	top: 0;
	left: 0;
	right: 0;
	bottom: 0;
	background: rgba(0, 0, 0, 0.25);
	z-index: 20;

	display: flex;
	flex-direction: column;
	justify-content: center;
	align-items: center;
	${padding};

	/* Active */

	transition: opacity 0.5s ease;
	opacity: 1;
	will-change: opacity;

	${props => {
		if (!props.active) return css`
			opacity: 0;
			pointer-events: none;
		`
	}}

	${media.phone`
		padding: 0;
	`}
`

// Content

const Content = styled.div`
	background: white;
    height: 280px;
	width: 400px;

	display: flex;
	flex-direction: column;
	align-items: center;
	padding: 40px;
	
	${Heading} {
		${heading2}
	}

	${Items} {
		margin-top: auto;
		display: flex;
		flex-direction: column;
		width: 100%;

		${Item} {
			display: flex;
			align-items: center;
			justify-content: center;
			background: ${green};
			border-radius: 4px;
			height: 48px;
			width: 100%;
			${heading4};
			cursor: pointer;

			&:not(:last-child) {
				margin-bottom: 24px;
			}
		}
	}

	${media.phone`
		height: 280px;
		width: 95vw;
	`}
`

// Close

const Close = styled.div`
	position: absolute;
	top: 24px;
	right: 24px;
	background-image: url(${require('../../assets/icons/modal-close.svg')});
	height: 40px;
	width: 40px;
	cursor: pointer;
`

export default TrialModal