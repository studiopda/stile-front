import React, { useState } from 'react'
import { navigate, Link, useStaticQuery } from 'gatsby'
import styled from 'styled-components'
import ProgressiveImage from 'react-progressive-image'
import moment from 'moment'

import { media } from '../../styles/utils'
import { container, bgImage, padding, hoverState, footerMaxWidth } from '../../styles/global'
import { darkGrey } from '../../styles/colors'
import { heading1, heading2, body } from '../../styles/type'
import { navSections } from '../Header/Header'

import { getGlobalData } from '../../../data/global'
import { getPagesByFolder } from '../Header/utils'

const Footer = (props) => {
	const data = getGlobalData('footer');
	const pageQuery = useStaticQuery(query).allSitePage.nodes
	const pages = getPagesByFolder(navSections, pageQuery);

	const renderContact = () => {
		const { phone, social } = data.contact;

		const items = social.map((item, i) => {
			return (
				<Item 
					as={'a'}
					href={item.link}
					target={'_blank'}
					key={i}
				>
					{item.name}
				</Item>
			)
		})

		return (
			<Contact>
				<Logo/>
				<Subheading>
					{phone}
				</Subheading>
				<Items>
					{items}
				</Items>
			</Contact>
		)
	}

	const renderPages = () => {
		if (!pages) return;

		const columns = pages.map((item, i) => {
			const pages = item.pages.map((item, i) => {
				return (
					<Item 
						key={i}
						as={Link}
						to={item.path}
					>
						{item.title}
					</Item>
				)
			})

			return (
				<Column 
					key={i}
				>	
					<Subheading>
						{item.name}
					</Subheading>

					<Items>
						{pages}
					</Items>
				</Column>
			)
		})

		return (
			<PageLinks>
				{columns}
			</PageLinks>
		)
	}

	const renderCredits = () => {
		const { terms, privacy } = data.credits;

		return (
			<Credits>
				<Credit
					as={'a'}
					href={terms}
				>
					Terms
				</Credit>
				
				<Credit
					as={'a'}
					href={terms}	
				>
					Security & Privacy
				</Credit>

				<Credit>
					© Stile Education {moment().format('YYYY')}
				</Credit>
			</Credits>
		)
	}
	
	return (
		<Wrapper>
			<Container>
				<Links>
					{renderContact()}
					{renderPages()}
				</Links>

				<Statement
					dangerouslySetInnerHTML={{__html: data.statement}}
				/>

				{renderCredits()}
			</Container>
		</Wrapper>	
	)
}

// Shared

const Heading = styled.div``
const Subheading = styled.div``
const Description = styled.div``
const Items = styled.div``
const Item = styled.div``

const BGImage = styled.div`
	background-image: url(${props => props.image});  
	${bgImage};
	transition: opacity 0.45s ease;
`

// Layout

const Wrapper = styled.div`
	display: flex;
	justify-content: center;
	width: 100%;
	background: ${darkGrey};

	${Heading},
	${Subheading},
	${Item} {
		color: white;
	}
`

const Container = styled.div`
	${container};
	max-width: ${footerMaxWidth}px;
	flex-direction: column;
	padding: 80px 0;
	${padding};

	${media.tablet`
		padding: 40px 0;
		${padding};
	`}
`

// Links

const Links = styled.div`
	display: flex;

	${media.tablet`
		flex-direction: column;
	`}
`

// Contact

const Logo = styled(BGImage)`
	background-image: url(${require('../../assets/images/header-logo.svg')});
	width: 41px;
	height: 16px;
`	

const Contact = styled.div`
	${Logo} {
		margin-bottom: 24px;
	}

	${Subheading} {
		margin-bottom: 22px;

		&, a {
			color: white;
		}
	}

	${Items} {
		display: flex;
		flex-direction: column;

		${Item} {
			${hoverState}
			text-decoration-line: underline;

			:not(:last-child) {
				margin-bottom: 22px;
			}
		}
	}
`

// Page Links

const PageLinks = styled.div`
	margin-left: auto;
	display: flex;

	${media.tablet`
		flex-direction: column;
		margin-top: 40px;
		margin-left: 0;
	`}
`

const Column = styled.div`
	${Subheading} {
		font-weight: 400;
		margin-bottom: 23px;
	}

	&:not(:last-child) {
		margin-right: 71px;
	}

	${Items} {
		display: flex;
		flex-direction: column;

		${Item} {
			${hoverState}

			&:not(:last-child) {
				margin-bottom: 22px;
			}
		}
	}

	${media.tablet`
		&:not(:last-child) {
			margin-bottom: 48px;
		}
	`}
`


// Statement 

const Statement = styled.div`
	${body}
	color: white;
	margin-top: 88px;

	${media.tablet`
		margin-top: 48px;
	`}
`

// Credits

const Credit = styled.div`
	font-size: 12px;
	line-height: 24px;
	color: white;
`

const Credits = styled.div`
	margin-top: 80px;
	display: flex;
	align-items: center;
	
	${media.tablet`
		margin-top: 40px;
	`}
	
	${Credit} {
		&:nth-child(1),
		&:nth-child(2) {
			text-decoration: underline;
			${hoverState}
		}

		&:nth-child(1) {
			margin-right: 57px;

			${media.tablet`
				margin-right: 37px;
			`}
		}

		&:nth-child(3) {
			margin-left: auto;
		}
	}
`

const query = graphql`
	query footerPages {
		allSitePage {
			nodes {
				path
			  	context {
					section
					name
					relativePath
				}
			}
		}
	}
`

export default Footer
