import React from 'react'
import { StaticQuery, graphql } from 'gatsby'
import { Helmet } from 'react-helmet'
import styled, { css } from 'styled-components'
import Transition from '../../transition';
import 'swiper/css/swiper.css'
import 'flatpickr/dist/themes/airbnb.css';

import { MetaTags, Header, Footer } from '../'
import { media } from '../../styles/utils';

const Layout = ({ children, meta, ...props }) => {
	return (
		<>
			<MetaTags
				{...meta}
			/>

			<Header
				{...props.headerProps}
			/>

			<Wrapper>
				<Transition>
					{children}
				</Transition>
			</Wrapper>

			<Footer/>
		</>
	)
}

const Wrapper = styled.div`
	width: 100%;
	min-height: 80vh;
	box-sizing: border-box;
	padding-top: 72px;
	overflow-x: hidden;
	z-index: 1;
	position: relative;

	${media.tablet`
		padding-top: 62px;
	`}
`

export default Layout
