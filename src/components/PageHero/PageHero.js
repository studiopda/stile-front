import React, { useState } from 'react'
import { navigate, Link } from 'gatsby'
import styled from 'styled-components'
import ProgressiveImage from 'react-progressive-image'

import { media } from '../../styles/utils'
import { container, bgImage, padding, hoverState } from '../../styles/global'
import { darkGrey } from '../../styles/colors'
import { heading1, heading2, body } from '../../styles/type'

const PageHero = ({data, ...props}) => {
	return (
		<Wrapper>
			<Container>
				{data.image && (
					<Image
						key={data.image}
						src={data.image}
					>
						{(src, loading) => {
							return (
								<BGImage 
									image={src} 
									style={{opacity: loading ? 0 : 1}}  
								/>
							)
						}}
					</Image> 
				)}

				<Info>
					<Heading
						as={'h1'}
						dangerouslySetInnerHTML={{__html: data.title}}
					/>
					<Description
						as={'h2'}
						dangerouslySetInnerHTML={{__html: data.description}}
					/>
				</Info>
			</Container>
		</Wrapper>	
	)
}

// Shared

const Heading = styled.div``
const Subheading = styled.div``
const Description = styled.div``

const Image = styled(ProgressiveImage)`
	overflow: hidden;
`

const BGImage = styled.div`
	background-image: url(${props => props.image});  
	${bgImage};
	transition: opacity 0.45s ease;
`

// Layout

const Wrapper = styled.div`
	display: flex;
	justify-content: center;
	width: 100%;
	margin-bottom: 88px;

	${media.phone`
		margin-bottom: 40px;
	`}
`

const Container = styled.div`
	${container}
	flex-direction: column;
	background: ${darkGrey};

	${BGImage} {
		height: 540px;
		width: 100%;

		${media.phone`
			height: 42vw;
		`}
	}
`

// Info 

const Info = styled.div`
	display: flex;
	flex-direction: column;
	padding: 80px 100px;

	${media.tablet`
		${padding}
	`}

	${media.phone`
		padding-top: 40px;
		padding-bottom: 40px;
	`}

	> * {
		color: white;
	}

	${Heading} {
		${heading1}
	}

	${Description} {
		${heading2}

		${media.phone`
			${body};
			margin-top: 8px;
		`}
	}
`

export default PageHero
