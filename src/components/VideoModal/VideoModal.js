import React, { Component, useEffect } from 'react'
import styled, { css } from 'styled-components'
import ProgressiveImage from 'react-progressive-image'
import { Helmet } from 'react-helmet'
import OutsideClickHandler from 'react-outside-click-handler';
import usePortal from 'react-useportal'

import { media, useBreakpoint, isClient } from '../../styles/utils'
import { padding, bgImage } from '../../styles/global'

const VideoModal = (props) => {
	const { Portal } = usePortal()
	const { active, toggle } = props;

	useEffect(() => {
		document.body.style.overflow = props.active == true ? 'hidden' : 'auto';
	}, [active])

    return (
		<>
			{active && (
				<Portal>
					<Wrapper
						active={active}
					>
						<Close
							onClick={toggle}
						/>
							<Content>
								{active && props.renderVideo()}
							</Content>
					</Wrapper>
				</Portal>
			)}
		</>
    )
}

// Shared

const Heading = styled.div``
const Subheading = styled.div``
const Description = styled.div``
const Info = styled.div``

const Image = styled(ProgressiveImage)`
    overflow: hidden;
`

const BGImage = styled.div`
    background-image: url(${props => props.image});
    ${bgImage};
    transition: opacity 0.45s ease;
`

// Wrapper

const Wrapper = styled.div`
    position: fixed;
	top: 0;
	left: 0;
	right: 0;
	bottom: 0;
	background: rgba(0, 0, 0, 0.25);
	z-index: 20;

	display: flex;
	justify-content: center;
	align-items: center;
	${padding};

	/* Active */

	transition: opacity 0.5s ease;
	opacity: 1;
	will-change: opacity;

	${props => {
		if (!props.active) return css`
			opacity: 0;
			pointer-events: none;
		`
	}}

	${media.phone`
		padding: 0;
	`}
`

// Content

const Content = styled.div`
	background: black;

    height: 735px;
	width: 100vw;
	max-height: 80vh;
	max-width: 1307px;

	display: flex;
	position: relative;
	overflow: hidden;
	box-shadow: -2px 8px 6px 0 rgba(0,0,0,0.07);
	border-radius: 5px;

	video {
		position: relative;
	}

	${media.phone`
		height: 280px;
		width: 95vw;
	`}
`

// Close

const Close = styled.div`
	position: absolute;
	top: 24px;
	right: 24px;
	background-image: url(${require('../../assets/icons/modal-close.svg')});
	height: 40px;
	width: 40px;
	cursor: pointer;
`

export default VideoModal