import React, {Component} from 'react'
import styled, { css } from 'styled-components'
import { rgba } from 'polished'

import { media } from '../../styles/utils'
import { bgIcon, bgImage } from '../../styles/global'
import { black, red } from '../../styles/colors'

class Checkbox extends Component {

    toggle = () => {
        const { onChange, value, defaultValue } = this.props;
        onChange(value !== null ? !value : !defaultValue, true);
    }

    render() {
        const { value, label, defaultValue } = this.props;
        return (
            <Wrapper>
                <Container>
                    <Box
                        active={value !== null ? value : defaultValue}
                        onClick={this.toggle}
                    />
                    <Label
                        active={value !== null ? value : defaultValue}
                        className={'label'}
                        onClick={this.toggle}
                    >   
                        {label}
                    </Label>
                </Container>
            </Wrapper>
        )
    }
}


export const Wrapper = styled.div`
    position: relative;
    width: 100%;
    margin-left: auto;
    user-select: none;
    margin-bottom: 6px;
`

const Container = styled.div`
    display: flex;
    align-items: flex-start;
`

const Label = styled.div`
    transform: translateY(1.5px);
    cursor: pointer;

    font-size: 16px;
    line-height: 24px;
`

const Box = styled.div`
    flex: 1 0 24px;
    max-width: 24px;
    width: 24px;
    height: 24px;
    border: 1px solid #CED0D6;
    border-radius: 4px;
    position: relative;
    margin-right: 16px;
    cursor: pointer;

    ${props => {
        if (props.active) return css`
            border: 2px solid #00CB50;

            &::after {
                content: '';
                position: absolute;
                top: 50%;
                left: 50%;
                transform: translate(-50%, -50%);
                background-image: url(${require('../../assets/icons/check.svg')});
                ${bgImage};
                width: 18px;
                height: 14px;
            }
        `
    }}
`

export default Checkbox;