import React from "react"

const defaults = {
    locale: 'AU',
    setLocaleContext: () => {},
}

export const LocaleContext = React.createContext({
    ...defaults,
});

class ContextWrapper extends React.PureComponent {
    constructor(props) {
        super(props);
    }

    setLocaleContext = (context) => {
        this.setState({
            ...this.state,
            ...context
        })
    }

    state = {
        ...defaults,
    }

	render() {
		const { children } = this.props
	
		return (
			<LocaleContext.Provider
                value={this.state}
            >
                {children}
            </LocaleContext.Provider>
		)
	}
}

export function withLocale(Component) {
    return function ThemeComponent(props) {
        return (
            <LocaleContext.Consumer>
                {context => <Component {...props} {...context} />}
            </LocaleContext.Consumer>
        )
    }
}

export default ContextWrapper