import React, { useState, useRef } from 'react'
import { Link, graphql } from 'gatsby'
import styled, { css, createGlobalStyle } from 'styled-components'
import ProgressiveImage from 'react-progressive-image'
import { motion } from 'framer-motion'
import Form from 'react-dynamic-form'
import { useSetState } from 'react-use'

import { Block, Layout, PageHero, Input } from '../components'
import { container, padding, bgImage, button } from '../styles/global'
import { black, red, darkGrey, green, greyText } from '../styles/colors'
import { heading1, body } from '../styles/type'
import { getTemplateData } from '../utils'
import { media, useBreakpoint } from '../styles/utils'
import { handleSignup } from '../services/trial'
import { trialFormSchema } from '../../data/forms/trial'
import { inputStyle } from '../components/Input/Input'

const Trial = ({pageContext, ...props}) => {
	const isPhone = useBreakpoint('phone');
    const data = getTemplateData('set-up-trial.js');
	const trialForm = useRef(null);
	
    const [formState, updateForm] = useSetState({
		submitting: false, 
		formComplete: false,
		error: false,
	})
    
    const renderForm = () => {
		if (formState.formComplete) return;

		return ( 
			<>
				<Heading>{data.heading}</Heading>

				<Content>	  
					<FormWrapper>
						<Form
							ref={trialForm}
							data={trialFormSchema}
							renderInput={<Input/>}
							styles={formStyles}
							renderSubmit={false}
							onSubmit={(fields, resetForm) => handleSignup(fields, resetForm, updateForm)}
						/>
						
						<Button
							theme={'outline-black'}
							onClick={() => {
								trialForm.current && trialForm.current.handleSubmit()
							}}
						>
							Set up a trial
						</Button>
						
						{formState.error && (
							<Error
								dangerouslySetInnerHTML={{__html: formState.error}}
							/>
						)}
					</FormWrapper>

					<Description
						dangerouslySetInnerHTML={{__html: data.text}}  
					/>
				</Content>
			</>
		)
	}

	const renderThankYou = () => {
		if (!formState.formComplete) return;

		return (
			<ThankYou
				dangerouslySetInnerHTML={{__html: data.formSuccessMessage }}
			/>
		)	
	}
	
	return (
		<Layout>  
			<DatePickerStyles/>
			<Wrapper>
				<Container>
					{renderForm()}
					{renderThankYou()}
				</Container>
			</Wrapper>
		</Layout>	
	)
}

// Shared

const Heading = styled.div``
const Description = styled.div``
const Button = styled.div``


// Layout

const Wrapper = styled.div`
	display: flex;
	justify-content: center;
	width: 100%;
	margin-top: 47px;

	${media.phone`
		margin-top: 40px;
	`}
`

const Container = styled.div`
	${container}
	flex-direction: column;
    padding-left: 90px; 
    padding-right: 90px;

	${Heading} {
        ${heading1};
        margin-bottom: 40px;
    }

	${media.phone`
		${padding};
        flex: 0 1 100%;
    `}
`

const Content = styled.div`
	display: flex;

	> ${Description} {
		flex: 0 1 433px;
		margin-left: 116px;

		font-size: 16px;
		line-height: 24px;
		color: ${darkGrey};
	}

	${media.phone`
		flex-direction: column;

		> ${Description} {
			flex: 1 0 auto;
			order: 1;
			margin-bottom: 30px;
			margin-left: 0;
		}
	`}
`	

// Form

const FormWrapper = styled.div`
	display: flex;
	width: 100%;
	position: relative;

	display: flex;
	flex-direction: column;
	width: 100%;
	flex: 0 1 525px;
    flex-direction: column;
	margin-bottom: 92px;

	${Button} {
		${button};
        max-width: 144px;

		${media.phone`
			margin-bottom: 40px;
		`}
	}

	${media.phone`
		order: 2;
	`}

`

const formStyles = css`
	flex-direction: column;
	align-items: flex-start;
	display: flex;	
	width: 100%;
	
	.dynamic-fields {
		width: 100%;
		display: flex;

	}

	.dynamic-field {
		margin-bottom: 24px;
		width: 100%;
		
		input {
			background: white;
		}
	}

	.error {
		margin-top: 16px;

		&, p, a {
			color: ${red} !important;
		}
	}

	/* Date / Time Picker */

	.flatpickr-input {
		${inputStyle}
		margin-left: 40px;
	}
`

export const DatePickerStyles = createGlobalStyle`
	.flatpickr-day	{
		&.selected,
		&.today.selected {
			background: ${green} !important;
			border-color: ${green} !important;
		}


		&.today {
			background: #CED0D6 !important;
			border: #CED0D6 !important;
		}
	}

	.flatpickr-next-month,
	.flatpickr-prev-month {
		&:hover {
			&, svg {
				color: ${green} !important;
				fill: ${green} !important;
			}
		}
	}
`

const ThankYou = styled.div`
    margin-top: 152px;
    max-width: 525px;
	${heading1};
`

const Error = styled.div`
	margin-top: 16px;

	&, p, a {
		color: ${red} !important;
	}
`



export default Trial