import React, { useState, useEffect } from 'react'
import { Link, graphql } from 'gatsby'
import styled, { css } from 'styled-components'
import { motion } from 'framer-motion'

import { Block, Layout, PageHero } from '../components'
import { container, padding, bgImage, button } from '../styles/global'
import { useMount, useWindowScroll } from 'react-use'
import { getTemplateData } from '../utils'
import { media, useBreakpoint, isClient } from '../styles/utils'

const SCROLL_THRESHOLD = 1;

const IndexPage = ({pageContext, ...props}) => {
	const isPhone = useBreakpoint('phone');
	const [collapsed, setCollapsed] = useState(isPhone ? false : true)
	const [modalActive, setModalActive] = useState(false)
	const {y} = useWindowScroll();
	const data = getTemplateData('home.js');

	useEffect(() => {
		if (y == 0 && collapsed) document.body.style.overflow = 'hidden'
 	}, [y, collapsed])

	const onWheel = (e) => {
		if (y < SCROLL_THRESHOLD && collapsed) {
			toggleCollapsed()
		}
	}

	const toggleModal = () => {
		setModalActive(!modalActive)
	}

	const toggleCollapsed = () => {
		if (isPhone) return;
		setCollapsed(!collapsed)

		if (!collapsed == false) {
			setTimeout(() => {
				document.body.style.overflow = 'auto'
			}, 1000);
		}
	}

	const renderDiscover = () => {
		return (
			<Discover
				variants={animatedDiscover}
				animate={collapsed ? 'hidden' : 'visible'}
			>
				<Container>
					<Button
						onClick={toggleCollapsed}
					>
						Discover More
					</Button>

					<Button
						onClick={toggleModal}
					>
						Watch Stile in action
					</Button>

					<Block
						layout={'media'}
						modalActive={modalActive}
						onModalChange={(state) => setModalActive(state)}
						{...data.watchStileVideo}
					/>
				</Container>
			</Discover>
		)
	}

	const wrapBlockAnimation = (block, comp, i, lastBlock) => {
		return (
			<AnimatedBlock
				layout={block.type}
				initial={isPhone ? 'visible' : 'hidden'}
				animate={collapsed ? 'hidden' : 'visible'}
				variants={getAnimatedBlockProps(block.type, i)}
			>
				{comp}
			</AnimatedBlock>
		)
	}

	const renderBlocks = () => {
		if (!data.blocks) return;

		return data.blocks.map((block, i) => {
			const excludeAnim = ['cta', 'page-links']

			let comp = (
				<Block
					layout={block.type}
					styles={block.type == 'media' && introVideo}
					lastBlock={i == (data.blocks.length - 1)}
					collapsed={collapsed}
					key={i}
					{...block}
				/>
			)

			// Inject discover buttons

			if (block.type == 'page-links') {
				comp = (
					<>
						{renderDiscover()}
						{comp}
					</>
				)
			}

			return excludeAnim.includes(block.type)
				? comp
				: wrapBlockAnimation(block, comp, i)
		})
	}

	return (
		<Layout
			headerProps={{
				onLogoClick: toggleCollapsed
			}}
		>
			<Wrapper
				onWheel={onWheel}
			>
				{renderBlocks()}
			</Wrapper>
		</Layout>
	)
}

// Shared

const Heading = styled.div``
const Subheading = styled.div``
const Description = styled.div``

// Layout

const Wrapper = styled.div`
	display: flex;
	flex-direction: column;
	align-items: center;
	width: 100%;
	margin-top: 120px;

	${media.phone`
		margin-top: 40px;
	`}

	/* Block overrides */

	.block-container {
		max-width: 1380px;
		align-items: flex-start;
	}

	.block_heading {
		margin-bottom: 20px;

		> * > * {
			max-width: none;
		}
	}

	.block_button {
		margin-top: -75px;

		${media.phone`
			margin-top: -32px;
			margin-bottom: 60px;
		`}
	}

	${media.phone`
		.block_media-comp {
			.block-container {
				transform: translateX(-17px);
			}
		}
	`}
`

const Container = styled.div`
	${container}
	${padding}
	max-width: 1380px;
`

// Intro video

const introVideo = css`
	max-width: 940px;

	.video-cover {
		padding-bottom: 56%;
	}
`

// Animated Blocks

const getAnimatedBlockProps = (type, i) => {
	const visibleBlocks = ['heading'];
	const visible = visibleBlocks.includes(type)

	return {
		hidden: {
			height: visible ? 'auto' : '0',
			opacity: visible ? 1 : 0,
			pointerEvents: visible ? 'all' : 'none',
			marginTop: i == 10 && '40px',
			marginBottom: visible && '-20px',
			transition: {
				duration: 1,
				easing: 'easeOut',
				opacity: {
					duration: 0.5,
				}
			}
		},
		visible: {
			height: 'auto',
			opacity: 1,
			pointerEvents: 'all',
			marginTop: 0,
			marginBottom: '20px',
			transition: {
				duration: 1.5,
				easing: 'easeInCubic',
				opacity: {
					duration: 2,
					delay: (0.2 * (i + 1))
				},
			}
		},
	}
}

// Discover

const animatedDiscover = {
	hidden: {
		height: 'auto',
		opacity: 1,
		marginTop: 40,
		marginBottom: 151,
		transition: {
			duration: 1,
		}
	},
	visible: {
		height: '0',
		marginBottom: 0,
		marginTop: 0,
		opacity: 0,
		transition: {
			duration: 1,
		}
	},
}

const Button = styled.div`
	${button}
	cursor: pointer;
`

const Discover = styled(motion.div)`
	display: flex;
	justify-content: center;
	width: 100%;
	margin-top: 40px;
	margin-bottom: 151px;

	${Button} {
		&:not(:last-child) {
			margin-right: 20px;
		}
	}

	/* Hide media block */

	.block_media {
		width: auto;
		height: 0;
	}
`

const AnimatedBlock = styled(motion.div)`
	width: 100%;

	/* Optimisations */

	${props => {
		if (props.layout !== 'media') return css`
			transform: translate3d(0,0,0);
		`
	}}
`

export default IndexPage