import React, { useState, useEffect, useRef } from 'react'
import { Link, graphql } from 'gatsby'
import styled, { css } from 'styled-components'
import ProgressiveImage from 'react-progressive-image'

import { Block, Layout, PageHero } from '../components'
import { container, padding, bgImage } from '../styles/global'
import { useMount } from 'react-use'
import { getTemplateData } from '../utils'
import { withLocale } from '../locale'

const DefaultPage = ({pageContext, ...props}) => {
	const data = getTemplateData(pageContext.relativePath);

	const renderHero = () => {
		if (!data.pageHero) return;

		return (
			<PageHero
				data={data.pageHero}
			/>
		)
	}

	const renderBlocks = () => {
		if (!data.blocks) return;
		
		return data.blocks.map((block, i) => {  
			const nextBlock = data.blocks[i + 1]; 

			return (
				<Block
					layout={block.type}
					lastBlock={i == (data.blocks.length - 1)}
					nextBlock={nextBlock}
                    key={i}
					{...block}
				/>
			)
		})
	}

	return (
		<Layout>
			<Wrapper>		
				{renderHero()}
				{renderBlocks()}
			</Wrapper>
		</Layout>	
	)
}

// Shared

const Heading = styled.div``
const Subheading = styled.div``
const Description = styled.div``

// Layout

const Wrapper = styled.div`
	display: flex;
	flex-direction: column;
	align-items: center;
	width: 100%;
`

const Container = styled.div`
	${container}
	${padding}

	display: flex;
	flex-direction: column;
`

// Blocks


export default withLocale(DefaultPage)