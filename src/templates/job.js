import React, { useState, useEffect, useRef } from 'react'
import { Link, graphql } from 'gatsby'
import styled, { css } from 'styled-components'
import ProgressiveImage from 'react-progressive-image'

import { Block, Layout } from '../components'
import { container, padding, innerContainer, buttonSecondary } from '../styles/global'
import { heading1 } from '../styles/type'
import { useMount } from 'react-use'
import { getJobData } from '../utils'
import { withLocale } from '../locale'

const Job = ({pageContext, ...props}) => {
	const data = getJobData(pageContext.relativePath);

	const renderBlocks = () => {
		if (!data.blocks) return;
		
		return data.blocks.map((block, i) => {  
			const nextBlock = data.blocks[i + 1]; 

			return (
				<Block
					layout={block.type}
					lastBlock={i == (data.blocks.length - 1)}
					nextBlock={nextBlock}
                    key={i}
					{...block}
				/>
			)
		})
	}

	const renderBack = () => {
		return (
			<Container>
				<InnerContainer>
					<Nav>
						<Button to={`/who-we-are/join-the-team/`}>
							← Back to all jobs
						</Button>
					</Nav>
				</InnerContainer>
			</Container>
		)
	}

	const renderTop = () => {
		let categories;

		if (data.categories.length > 0) {
			categories = data.categories.map((item, i) => {
				return (
					<Item key={i}>
						{item}
					</Item>
				)
			})
		}

		return (
			<Container>
				<InnerContainer>
					<Top>
						<Heading>{data.title}</Heading>

						<Items>
							{data.location && (
								<Item>{data.location}</Item>
							)}
							
							{categories}
							
							{data.workType && (
								<Item>{data.workType}</Item>
							)}
						</Items>
					</Top>
				</InnerContainer>
			</Container>
		)
	}

	return (
		<Layout>
			<Wrapper>
				{renderBack()}
				{renderTop()}
				{renderBlocks()}
			</Wrapper>
		</Layout>	
	)
}

// Shared

const Heading = styled.div``
const Subheading = styled.div``
const Description = styled.div``
const Button = styled(Link)``
const Items = styled.div``
const Item = styled.div``

// Layout

const Wrapper = styled.div`
	display: flex;
	flex-direction: column;
	align-items: center;
	width: 100%;
`

const Container = styled.div`
	${container}
	${padding}

	display: flex;
	flex-direction: column;
	align-items: center;
`

const InnerContainer = styled.div`
	${innerContainer};
`

// Nav

const Nav = styled.div`
	display: flex;
	flex-direction: column;
	align-items: flex-start;
	padding-top: 40px;
	padding-bottom: 60px;

	${Button} {
		${buttonSecondary}
	}
`

// Top

const Top = styled.div`
	margin-bottom: 32px;

	${Heading} {
		${heading1}
		transform: translate3d(0,0,0);
	}

	${Items} {
		display: flex;
		margin-top: 8px;

		${Item} {
			font-weight: 400;
			font-size: 14px;
			line-height: 24px;
			text-transform: uppercase;
			white-space: pre;

			&:not(:last-child)::after {
				content: ' · ';
			}
		}
	}
`

// Blocks


export default withLocale(Job)