import { css, createGlobalStyle } from 'styled-components';
import { media } from './utils';
import { darkGrey, green } from './colors';

export const maxWidth = 1280;
export const footerMaxWidth = 1175;

export const GlobalStyles = createGlobalStyle`
    html,
    body {
        margin: 0;
        padding: 0;
        height: 100%;
        width: 100%;
        background: white;
        font-family: 'Graphik', Helvetica, sans-serif;
    }
    
    a {
        text-decoration: none;
    }

    h1, h2, h3 {
        font-size: 1rem;
        font-weight: 300;
        margin: 0;
    }

    a, p, div {
        color: ${darkGrey};
        font-weight: 300;
    }

    b, strong {
        font-weight: 500;
    }

    * {
        -webkit-overflow-scrolling: touch;
        -webkit-font-smoothing: antialiased;
        box-sizing: border-box;
    }

    p:first-child {
		margin-top: 0;
	}
`;

export const container = css`
    display: flex;
	width: 100%;
    max-width: ${maxWidth}px;
`

export const hoverState = css`
    transition: 0.15s opacity ease;

    &:hover {
        opacity: 0.7;
    }
`

export const padding = css`
    padding-left: 50px; 
    padding-right: 50px;
    box-sizing: border-box;

    ${media.tablet`
        padding-left: 32px; 
        padding-right: 32px;
    `}

    ${media.phone`
        padding-left: 17px; 
        padding-right: 17px;
    `}
`

export const bgImage = css`
    background-size: cover;
    background-position: center center;
    background-repeat: no-repeat;
`

export const bgIcon = css`
    background-size: contain;
    background-position: center center;
    background-repeat: no-repeat;
`

export const innerContainer = css`
    max-width: 700px;
    width: 100%;
`

export const button = css`
    font-weight: 500;
	background: ${green};
	padding: 0 24px;
	height: 48px;
    min-width: 144px;
	display: flex;
	justify-content: center;
	align-items: center;
	border-radius: 4px;
`

export const buttonSecondary = css`
    ${button};
    background: ${darkGrey};
    color: white;
`
