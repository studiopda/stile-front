// Primary

export const green = '#00E65A'
export const green2 = '#00CB50'
export const black = '#4E4C49'
export const beige = '#FAF8F7'

// Secondary

export const orange = '#FF9817'
export const red = '#D13565;'
export const teal = '#35C3BD'
export const blue = '#0878BE'

// Other

export const darkGrey = '#393939'
export const grey = '#4F4D4A'
export const greyText = '#706D69'

// Possible

// #4E4C49
// #706D69
// #CED0D6
