import { css } from 'styled-components'
import { media } from './utils'

export const subheading = css`

`

export const heading1 = css`
    font-family: 'Boing';
	font-weight: 500;
	font-size: 50px;
	line-height: 64px;

    ${media.phone`
        font-size: 36px;
        line-height: 40px;
    `}
`

export const heading2 = css`
    font-family: 'Graphik';
    font-size: 32px;
    line-height: 40px;
`

export const heading3 = css`
    font-family: 'Graphik';
    font-size: 24px;
    line-height: 32px;
    font-weight: 500;
`

export const heading4 = css`
    font-family: 'Graphik';
    font-size: 16px;
    line-height: 24px;
    font-weight: 500;
`

export const body = css`
 	font-size: 24px;
    line-height: 32px;	

    ${media.phone`
        font-size: 16px;
        line-height: 24px;	
    `}
`

export const smallBody = css`
	font-size: 14px;
    line-height: 20px;
`

