import { Link } from 'gatsby'
import { forEach } from 'lodash'

// Scroll to ref

export const scrollToRef = (ref) => {
    if (!ref || !ref.current) return;

    try {
        window.scroll({
            top: ref.current.offsetTop,
            left: 0,
            behavior: 'smooth',
        });
    } catch (error) {
        window.scrollTo(ref.current.offsetTop, 0);
    }
}
 
export const scrollToTop = () => {
    try {
        window.scroll({
            top: 0,
            left: 0,    
            behavior: 'smooth',
        });
    } catch (error) {
        window.scrollTo(0, 0);
    }
}

export const getTemplateData = (relativePath) => {
    return require(`../data/pages/${relativePath}`).default
}

export const getJobData = (relativePath) => {
    return require(`../data/jobs/${relativePath}`).default
}

// Get Link props

export const getLinkProps = (link) => {
    return {
        as: link.external ? 'a' : Link,
        target: '_blank',
        href: link.url,
        to: link.url
    }
}

