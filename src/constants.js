export const JSON_URL = process.env.GATSBY_WORDPRESS_URL;
export const MAPBOX_KEY = process.env.GATSBY_MAPBOX_KEY;