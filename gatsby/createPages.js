require(`@babel/register`)

const path = require('path')
const Promise = require('bluebird')

module.exports = async (createPage, graphql) => {

    const result = await graphql(`
        {
            allFile(filter: {sourceInstanceName: {eq: "pages"}}) {
                nodes {
                    name
                    relativeDirectory
                    relativePath
                }
            }
        }
    `)

    const items = result.data.allFile.nodes;
    if (!items.length) return;

    items.forEach(item => {
        createPage({
            path: `${item.relativeDirectory}/${item.name}/`,
            component: path.resolve(`./src/templates/default-page.js`),
            context: {
                relativePath: item.relativePath,
                section: item.relativeDirectory || null,
                name: item.name
            }
        })
    });  
    
};  

