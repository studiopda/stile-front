import fonts from './src/styles/fonts.css'
import wrapWithProvider from "./wrap-with-provider"

export const wrapRootElement = wrapWithProvider